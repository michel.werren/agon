// We only need Europe/Zurich and UTC timezones
const replacementPlugin = new webpack.NormalModuleReplacementPlugin(
    /moment-timezone\/data\/packed\/latest\.json/,
    '../../../../../timezone/agon-timezone.json'
);
config.plugins.push(replacementPlugin);

// Only use de locale for Moment JS
config.plugins.push(new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /de/));
