// Minify JS for production

if (defined.PRODUCTION === true || defined.PRODUCTION === 'true') {
    const ClosurePlugin = require('closure-webpack-plugin');

    config.optimization = {
        minimize: true,
        nodeEnv: 'production',
        mangleWasmImports: true,
        removeAvailableModules: true,
        minimizer: [
            new ClosurePlugin({
                mode: 'STANDARD',
                platform: 'java'
            }, {
                languageOut: 'ECMASCRIPT5',
                formatting: 'SINGLE_QUOTES',
            })
        ],
    };
}