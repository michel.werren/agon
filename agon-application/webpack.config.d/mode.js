// Define environment dependent vars

let nodeEnv = 'development';

if (defined.PRODUCTION === true || defined.PRODUCTION === 'true') {
    nodeEnv = 'production';
}

const modePlugin = new webpack.DefinePlugin({
    'process.env': {
        'NODE_ENV': JSON.stringify(nodeEnv)
    },
    'process.env.NODE_ENV': JSON.stringify(nodeEnv),
    'serverPort': JSON.stringify(defined.SERVER_PORT),
    'production': defined.PRODUCTION === "true"
});
config.plugins.push(modePlugin);