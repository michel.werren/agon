// SASS to CSS compiler. In production mode the css will additionally extracted into a separate file.

// Those paths differs because during production build the path points to the minimized kotlin js
if (defined.PRODUCTION === true || defined.PRODUCTION === 'true') {
    config.entry.styles = '../../../../src/jsMain/kotlin/ch/sourcemotion/agon/ui/main.scss';
} else {
    config.entry.styles = '../../../../../src/jsMain/kotlin/ch/sourcemotion/agon/ui/main.scss';
}


if (defined.PRODUCTION === true || defined.PRODUCTION === 'true') {
    const ExtractTextPlugin = require("extract-text-webpack-plugin");
    const extractSass = new ExtractTextPlugin({
        filename: "[name].css",
        disable: false
    });

    const OptimizeCssnanoPlugin = require('@intervolga/optimize-cssnano-plugin');

    config.module.rules.push({
        test: /\.scss$/,
        use: extractSass.extract({
            use: [{
                loader: "css-loader"
            }, {
                loader: "sass-loader",
                options: {
                    includePaths: ['./node_modules']
                }
            }],
            // use style-loader in development
            fallback: "style-loader"
        })
    });

    config.plugins.push(extractSass);
    config.plugins.push(new OptimizeCssnanoPlugin({
        cssnanoOptions: {
            preset: ['default', {
                discardComments: {
                    removeAll: true,
                },
            }],
        },
    }));

} else {
    config.module.rules.push({
        test: /\.scss$/,
        use: [
            {loader: "style-loader"},
            {loader: "css-loader"},
            {
                loader: "sass-loader",
                options: {
                    includePaths: ['./node_modules']
                }
            }
        ]
    });
}


