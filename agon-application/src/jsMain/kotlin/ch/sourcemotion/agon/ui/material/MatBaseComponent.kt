package ch.sourcemotion.agon.ui.material

import react.RComponent
import react.RProps
import react.RState


abstract class MatBaseComponent<P : RProps, S : RState, C : MDCBase> : RComponent<P, S>() {
    protected var jsComponent: C? = null

    override fun componentWillUnmount() {
        jsComponent?.destroy()
    }
}