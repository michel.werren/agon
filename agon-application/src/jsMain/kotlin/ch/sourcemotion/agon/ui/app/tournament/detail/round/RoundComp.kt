package ch.sourcemotion.agon.ui.app.tournament.detail.round

import ch.sourcemotion.agon.dto.*
import ch.sourcemotion.agon.extension.isNeitherNullOrEmpty
import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.ui.app.tournament.detail.TournamentGraphComp
import ch.sourcemotion.agon.ui.app.tournament.detail.match.matchCard
import ch.sourcemotion.agon.ui.component.failurehandling.FailureHandlingCompState
import ch.sourcemotion.agon.ui.component.titled.titled
import ch.sourcemotion.agon.ui.extension.changeState
import ch.sourcemotion.agon.ui.extension.getPathParam
import ch.sourcemotion.agon.ui.extension.getPathParams
import ch.sourcemotion.agon.ui.extension.getReactHistory
import ch.sourcemotion.agon.ui.i18n.I18n
import ch.sourcemotion.agon.ui.material.loading.matLoadingModal
import ch.sourcemotion.agon.ui.request.*
import ch.sourcemotion.agon.ui.rest.RestClient
import ch.sourcemotion.agon.ui.rest.RestClientException
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import mu.KotlinLogging
import react.RBuilder
import react.RProps
import react.dom.div
import kotlin.contracts.ExperimentalContracts

interface RoundCompState : FailureHandlingCompState {
    var tournament: TournamentDetailDto?
    var stage: StageDto?
    var group: GroupDto?
    var round: RoundDto?
    var matches: List<MatchDto>?
}

@ExperimentalContracts
class RoundComp : TournamentGraphComp<RProps, RoundCompState>() {

    companion object {
        private val log = KotlinLogging.logger("RoundComp")
    }

    override fun RBuilder.render() {
        renderFailureDialog()
        val tournament = state.tournament
        val group = state.group
        val stage = state.stage
        val round = state.round
        val matches = state.matches

        if (tournament.isNotNull() && group.isNotNull() && stage.isNotNull() && round.isNotNull() && matches.isNotNull()) {
            titled {
                attrs {
                    title = "${I18n.default("Round")}: ${round.name}"
                    history = getReactHistory()
                }

                div(classes = "round-comp") {
                    renderGraphElement(I18n.default("TournamentName"), tournament.name)
                    renderGraphElement(I18n.default("StageName"), stage.name)
                    renderGraphElement(I18n.default("GroupName"), group.name)
                    renderGraphElement(I18n.default("RoundName"), round.name)

                    matches.sortedBy { it.number }.forEach { match ->
                        matchCard {
                            attrs {
                                this.match = match
                            }
                        }
                    }
                }
            }
        } else {
            matLoadingModal {
                attrs {
                    show = state.failureMessage.isNullOrBlank()
                    text = I18n.default("LoadMessage.LoadRound")
                    indeterminate = true
                }
            }
        }
    }

    override fun componentDidMount() {
        val tournamentId = getPathParam("tournamentId")
        val stageId = getPathParam("stageId")
        val groupId = getPathParam("groupId")
        val roundId = getPathParam("roundId")

        if (tournamentId.isNeitherNullOrEmpty() && stageId.isNeitherNullOrEmpty() && groupId.isNeitherNullOrEmpty() && roundId.isNeitherNullOrEmpty()) {
            GlobalScope.launch {
                val tournamentJob = async {
                    try {
                        RestClient.get(TournamentDetailRequest(tournamentId))
                    } catch (e: RestClientException) {
                        openFailureDialog(I18n.failure("Tournament", e.failure))
                        null
                    }
                }
                val stageJob = async {
                    try {
                        RestClient.get(StageRequest(tournamentId, stageId))
                    } catch (e: RestClientException) {
                        openFailureDialog(I18n.failure("Stage", e.failure))
                        null
                    }
                }
                val groupJob = async {
                    try {
                        RestClient.get(GroupRequest(tournamentId, groupId))
                    } catch (e: RestClientException) {
                        openFailureDialog(I18n.failure("Group", e.failure))
                        null
                    }
                }
                val roundJob = async {
                    try {
                        RestClient.get(RoundRequest(tournamentId, roundId))
                    } catch (e: RestClientException) {
                        openFailureDialog(I18n.failure("Round", e.failure))
                        null
                    }
                }
                val matchesJob = async {
                    try {
                        RestClient.get(MatchesRequest.forRound(tournamentId, roundId))
                    } catch (e: RestClientException) {
                        openFailureDialog(I18n.failure("Matches", e.failure))
                        null
                    }
                }

                val tournament = tournamentJob.await()
                val stage = stageJob.await()
                val group = groupJob.await()
                val round = roundJob.await()
                val matches = matchesJob.await()

                changeState {
                    this.tournament = tournament
                    this.stage = stage
                    this.group = group
                    this.round = round
                    this.matches = matches
                }
            }
        } else {
            log.error { "Missing at least one parameter. Parameters: ${getPathParams()}" }
        }
    }
}