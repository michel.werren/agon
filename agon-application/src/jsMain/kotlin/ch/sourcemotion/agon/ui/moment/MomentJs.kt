package ch.sourcemotion.agon.ui.moment

import kotlin.js.Date
import kotlin.math.ceil

const val TZ_EUROPE_ZURICH = "europe/zurich"

const val DATE_ONLY_FORMAT = "YYYY-MM-DD"

const val LONG_DATE_ONLY_FORMAT = "Do MMMM YYYY"

const val LONG_DATE_TIME_FORMAT = "Do MMMM YYYY HH:mm"


enum class MomentUnit(val unitName: String, val unitSingular: String, val shorthand: String) {
    MILLIS("milliseconds", "millisecond", "ms"),
    SECONDS("seconds", "second", "s"),
    MINUTES("minutes", "minute", "m"),
    HOURS("hours", "hour", "h"),
    DAYS("days", "day", "d"),
    WEEKS("weeks", "week", "w"),
    MONTHS("months", "month", "M"),
    QUARTERS("quarters", "quarter", "Q"),
    YEARS("years", "year", "y");
}

val moment: dynamic = kotlinext.js.require("moment-timezone")!!

fun momentChangeLanguage(lang: String, config: dynamic) = moment.locale(lang, config)

fun getMoment(): Moment = moment.utc()

fun getMomentTZ(): MomentTZ = moment.tz()

fun getMomentTZ(tz: String): MomentTZ = moment.tz(tz)

fun getMomentTZ(mom: Moment, tz: String): MomentTZ = moment.tz(mom, tz)

fun getMomentTZZurich(value: String): MomentTZ = moment.tz(value, TZ_EUROPE_ZURICH)

fun getMomentTZZurich(mom: Moment): MomentTZ = moment.tz(mom, TZ_EUROPE_ZURICH)

fun getMomentTZNowZurich(): MomentTZ = moment.tz(getMoment(), TZ_EUROPE_ZURICH)

fun getMoment(value: String): Moment = moment(value)

fun getMomentUtc(value: String): Moment = moment(value)
fun getMomentTZ(value: String, tz: String): MomentTZ = moment.tz(value, tz)

fun getMoment(date: Date): Moment = moment.utc(date)
fun getMomentTZ(date: Date, tz: String): MomentTZ = moment.tz(date, tz)

// Make use of date because of the lack of typing problem with direct usage of Kotlin Long type atm.
fun getMoment(millis: Number): Moment = getMoment(Date(millis))

fun getMomentTZ(millis: Number, tz: String): MomentTZ = getMomentTZ(Date(millis), tz)

fun cloneMoment(moment: Moment): Moment = moment(moment)

fun getDuration(unit: MomentUnit, value: Int): Duration = moment.duration(value, unit.unitName)

fun getDuration(millis: Number): Duration = moment.duration(millis)

fun setMomentLocale(locale: String) {
    moment.locale(locale)
}

/**
 * @return ceil of the given [Duration] as days
 */
fun getCeilDayDuration(duration: Duration): Int {
    return ceil(duration.asDays()).toInt()
}

val dayDuration: Duration = moment.duration(1, MomentUnit.DAYS.unitName)
val hourDuration: Duration = moment.duration(1, MomentUnit.HOURS.unitName)
val weekDuration: Duration = moment.duration(1, MomentUnit.WEEKS.unitName)
val monthDuration: Duration = moment.duration(1, MomentUnit.MONTHS.unitName)

interface Moment {
    companion object {
        fun add(moment: Moment, duration: Duration) = moment.add(duration)
        fun subtract(moment: Moment, duration: Duration) = moment.subtract(duration)

        fun endOf(moment: Moment, unit: MomentUnit) = moment.endOf(unit.unitSingular)
        fun startOf(moment: Moment, unit: MomentUnit) = moment.startOf(unit.unitSingular)

        fun format(moment: Moment, tz: String, format: String): String {
            return getMomentTZ(moment, tz).format(format)
        }

        fun isThisDay(day: Moment, mayWithin: Moment): Boolean {
            val dayStart = Moment.startOf(cloneMoment(day), MomentUnit.DAYS)
            val dayEnd = Moment.endOf(cloneMoment(day), MomentUnit.DAYS)
            return dayStart.isSameOrBefore(mayWithin) && dayEnd.isSameOrAfter(mayWithin)
        }

        fun sundayInFirstWeekInMonth(moment: Moment): Moment {
            val out = startOf(cloneMoment(moment), MomentUnit.MONTHS)
            return startOf(out, MomentUnit.WEEKS)
        }

        fun mondayInFirstWeekInMonth(moment: Moment): Moment {
            var sunday = sundayInFirstWeekInMonth(moment)
            // Avoid the trap when the 1. of the month is sunday, so we need a week earlier
            if (sunday.date() == 1) {
                sunday = subtract(sunday, weekDuration)
            }
            return add(sunday, dayDuration)
        }

        fun mondayThisWeek(moment: Moment): Moment {
            // First day in week, means Sunday
            val sunday = startOf(cloneMoment(moment), MomentUnit.WEEKS)
            // Since our week start is on Monday
            return add(sunday, dayDuration)
        }
    }

    @JsName("format")
    fun format(format: String? = null): String

    fun millisecond(): Int
    fun second(): Int
    fun minute(): Int
    fun hour(): Int
    @JsName("hours")
    fun hours(value: Int)

    fun date(): Int
    fun day(): Int
    fun weekday(): Int
    fun isoWeekday(): Int
    fun dayOfYear(): Int
    fun week(): Int
    fun isoWeek(): Int
    fun month(): Int
    @JsName("diff")
    fun diff(moment: Moment): Number

    @JsName("months")
    fun months(value: Int): Moment

    fun quarter(): Int
    fun year(): Int
    @JsName("years")
    fun years(value: Int): Moment

    @JsName("isoWeekYear")
    fun isoWeekYear(): Int

    @JsName("weeksInYear")
    fun weeksInYear(): Int

    @JsName("daysInMonth")
    fun daysInMonth(): Int

    @JsName("max")
    fun max(m1: Moment, m2: Moment): Moment

    @JsName("add")
    fun add(vararg value: dynamic): Moment

    @JsName("subtract")
    fun subtract(vararg value: dynamic): Moment

    @JsName("isBefore")
    fun isBefore(other: Moment): Boolean

    @JsName("isSame")
    fun isSame(other: Moment): Boolean

    @JsName("isSameOrBefore")
    fun isSameOrBefore(other: Moment): Boolean

    @JsName("isAfter")
    fun isAfter(other: Moment): Boolean

    @JsName("isSameOrAfter")
    fun isSameOrAfter(other: Moment): Boolean

    @JsName("isDST")
    fun isDST(): Boolean

    @JsName("isLeapYear")
    fun isLeapYear(): Boolean

    @JsName("endOf")
    fun endOf(unit: String): Moment

    @JsName("startOf")
    fun startOf(unit: String): Moment

    @JsName("toDate")
    fun toDate(): Date

    @JsName("valueOf")
    fun timestamp(): Long
}

/**
 * Moment with time zone
 */
interface MomentTZ : Moment {
    @JsName("names")
    fun names(): List<String>
}

interface Duration {
    @JsName("asDays")
    fun asDays(): Double

    @JsName("asHours")
    fun asHours(): Double

    @JsName("asMinutes")
    fun asMinutes(): Double
}
