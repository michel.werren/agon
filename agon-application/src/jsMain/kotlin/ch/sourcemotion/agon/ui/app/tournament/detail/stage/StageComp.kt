package ch.sourcemotion.agon.ui.app.tournament.detail.stage

import ch.sourcemotion.agon.dto.GroupDto
import ch.sourcemotion.agon.dto.StageDto
import ch.sourcemotion.agon.dto.TournamentDetailDto
import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.ui.app.tournament.detail.TournamentGraphComp
import ch.sourcemotion.agon.ui.component.description.DescriptionCardEntry
import ch.sourcemotion.agon.ui.component.description.descriptionCard
import ch.sourcemotion.agon.ui.component.failurehandling.FailureHandlingCompState
import ch.sourcemotion.agon.ui.component.titled.titled
import ch.sourcemotion.agon.ui.extension.changeState
import ch.sourcemotion.agon.ui.extension.getPathParam
import ch.sourcemotion.agon.ui.extension.getReactHistory
import ch.sourcemotion.agon.ui.extension.navigate
import ch.sourcemotion.agon.ui.i18n.I18n
import ch.sourcemotion.agon.ui.material.loading.matLoadingModal
import ch.sourcemotion.agon.ui.request.GroupsRequest
import ch.sourcemotion.agon.ui.request.StageRequest
import ch.sourcemotion.agon.ui.request.TournamentDetailRequest
import ch.sourcemotion.agon.ui.rest.RestClient
import ch.sourcemotion.agon.ui.rest.RestClientException
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import react.RBuilder
import react.RProps
import react.dom.div
import kotlin.contracts.ExperimentalContracts

interface StageCompState : FailureHandlingCompState {
    var tournament: TournamentDetailDto?
    var stage: StageDto?
    var groups: List<GroupDto>?
}

@ExperimentalContracts
class StageComp : TournamentGraphComp<RProps, StageCompState>() {
    override fun RBuilder.render() {
        val tournament = state.tournament
        val stage = state.stage
        val groups = state.groups
        if (tournament.isNotNull() && stage.isNotNull() && groups.isNotNull()) {
            titled {
                attrs {
                    title = "${I18n.default("Stage")}: ${stage.name}"
                    history = getReactHistory()
                }
                div(classes = "stage-comp") {
                    renderGraphElement(I18n.default("TournamentName"), tournament.name)
                    renderGraphElement(I18n.default("StageName"), stage.name)
                    div(classes = "stage-groups") {
                        groups.sortedBy { it.number }.forEach { group ->
                            descriptionCard {
                                attrs {
                                    entries = listOf(
                                        DescriptionCardEntry(I18n.default("GroupName"), group.name),
                                        DescriptionCardEntry(
                                            I18n.default("GroupSize"),
                                            group.settings?.size?.toString() ?: I18n.default("NoValue")
                                        ),
                                        DescriptionCardEntry(
                                            I18n.default("Closed"), if (group.closed.isTrue()) {
                                                I18n.default("Yes")
                                            } else {
                                                I18n.default("No")
                                            }
                                        )
                                    )
                                    clickHandler = {
                                        navigate("/groups/${tournament.id}/${stage.id}/${group.id}")
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            matLoadingModal {
                attrs {
                    show = state.failureMessage.isNullOrBlank()
                    text = I18n.default("LoadMessage.LoadStage")
                    indeterminate = true
                }
            }
        }
    }

    override fun componentDidMount() {
        GlobalScope.launch {
            val tournamentJob = async {
                getPathParam("tournamentId")?.let {
                    try {
                        RestClient.get(TournamentDetailRequest(it))
                    } catch (e: RestClientException) {
                        openFailureDialog(I18n.failure("Tournament", e.failure))
                        null
                    }
                }
            }

            val stageJob = async {
                getPathParam("tournamentId")?.let { tournamentId ->
                    getPathParam("stageId")?.let { stageId ->
                        try {
                            RestClient.get(StageRequest(tournamentId, stageId))
                        } catch (e: RestClientException) {
                            openFailureDialog(I18n.failure("Stage", e.failure))
                            null
                        }
                    }
                }
            }
            val groupsJob = async {
                getPathParam("tournamentId")?.let { tournamentId ->
                    getPathParam("stageId")?.let { stageId ->
                        try {
                            RestClient.get(GroupsRequest(tournamentId, stageId))
                        } catch (e: RestClientException) {
                            openFailureDialog(I18n.failure("Groups", e.failure))
                            null
                        }
                    }
                }
            }
            val tournament = tournamentJob.await()
            val stage = stageJob.await()
            val groups = groupsJob.await()

            changeState {
                this.groups = groups
                this.stage = stage
                this.tournament = tournament
            }
        }
    }
}