package ch.sourcemotion.agon.ui

import ch.sourcemotion.agon.ui.app.tournament.detail.TournamentDetailComp
import ch.sourcemotion.agon.ui.app.tournament.detail.group.GroupComp
import ch.sourcemotion.agon.ui.app.tournament.detail.round.RoundComp
import ch.sourcemotion.agon.ui.app.tournament.detail.stage.StageComp
import ch.sourcemotion.agon.ui.app.tournament.list.TournamentListComp
import ch.sourcemotion.agon.ui.i18n.I18n
import ch.sourcemotion.agon.ui.moment.setMomentLocale
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import mu.KotlinLogging
import mu.KotlinLoggingLevel
import mu.LOG_LEVEL
import react.dom.render
import react.router.dom.hashRouter
import react.router.dom.redirect
import react.router.dom.route
import react.router.dom.switch
import kotlin.browser.document
import kotlin.contracts.ExperimentalContracts

external var production: Boolean

private val log = KotlinLogging.logger("Main")

@ExperimentalContracts
fun main(args: Array<String>) {

    LOG_LEVEL = if (production) {
        KotlinLoggingLevel.INFO
    } else {
        KotlinLoggingLevel.TRACE
    }

    setMomentLocale("de")

    val container = document.getElementById("agon")!!

    GlobalScope.launch {
        I18n.start()

        render(container) {
            hashRouter {
                app {
                    switch {
                        route("/", exact = true) {
                            redirect(from ="/", to = "/tournaments")
                        }
                        route("/tournaments", component = TournamentListComp::class, exact = true)
                        route("/tournaments/:tournamentId", component = TournamentDetailComp::class, exact = true)
                        route("/stages/:tournamentId/:stageId", component = StageComp::class, exact = true)
                        route("/groups/:tournamentId/:stageId/:groupId", component = GroupComp::class, exact = true)
                        route("/rounds/:tournamentId/:stageId/:groupId/:roundId", component = RoundComp::class, exact = true)
                    }
                }
            }
        }
    }
}