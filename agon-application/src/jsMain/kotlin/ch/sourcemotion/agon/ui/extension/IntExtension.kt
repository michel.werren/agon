package ch.sourcemotion.agon.ui.extension

/**
 * @return This value if present or zero.
 */
fun Int?.orZero() : Int = this ?: 0