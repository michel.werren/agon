package ch.sourcemotion.agon.ui.component.failurehandling

import ch.sourcemotion.agon.extension.isNeitherNullOrEmpty
import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.ui.extension.ReactHistory
import ch.sourcemotion.agon.ui.extension.changeState
import ch.sourcemotion.agon.ui.extension.getReactHistory
import ch.sourcemotion.agon.ui.i18n.I18n
import ch.sourcemotion.agon.ui.material.dialog.matDialog
import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.div
import kotlin.contracts.ExperimentalContracts

interface FailureHandlingCompState : RState {
    var failureMessage: String?
    var history: ReactHistory?
}

@ExperimentalContracts
abstract class FailureHandlingComp<P : RProps, R : FailureHandlingCompState> : RComponent<P, R>() {
    fun RBuilder.renderFailureDialog() {
        val history = state.history
        matDialog {
            attrs {
                title = I18n.default("FailureMessage.Title")
                show = state.failureMessage.isNeitherNullOrEmpty()
                history?.let {
                    closedHandler = {
                        it.push("/tournaments")
                    }
                }
            }

            div(classes = "failure-handling-comp") {
                div(classes = "failure-detail-message") {
                    state.failureMessage?.let { +it }
                }

                div(classes = "failure-consequence-message") {
                    if (history.isNotNull()) {
                        +I18n.default("FailureMessage.BackNavigationConsequenceMessage")
                    } else {
                        +I18n.default("FailureMessage.LaterConsequenceMessage")
                    }
                }
            }
        }
    }

    protected fun openFailureDialog(message: String) {
        changeState {
            failureMessage = message
            this.history = getReactHistory()
        }
    }
}