package ch.sourcemotion.agon.ui.app.tournament.detail.match

import ch.sourcemotion.agon.dto.MatchDto
import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.ui.component.labeledtext.labeledText
import ch.sourcemotion.agon.ui.extension.toStringOrNoValue
import ch.sourcemotion.agon.ui.i18n.I18n
import ch.sourcemotion.agon.ui.material.MAT_CELL_DIMENSION
import ch.sourcemotion.agon.ui.material.card.matCard
import ch.sourcemotion.agon.ui.moment.LONG_DATE_TIME_FORMAT
import ch.sourcemotion.agon.ui.moment.TZ_EUROPE_ZURICH
import ch.sourcemotion.agon.ui.moment.getMomentTZ
import kotlinx.css.GridTemplateRows
import react.*
import react.dom.div
import react.dom.span
import styled.css
import styled.styledDiv
import kotlin.contracts.ExperimentalContracts

interface MatchCardCompProps : RProps {
    var match: MatchDto?
    var timezone: String?
}

@ExperimentalContracts
class MatchCardComp : RComponent<MatchCardCompProps, RState>() {
    override fun RBuilder.render() {

        val match = props.match
        val timezone = props.timezone ?: TZ_EUROPE_ZURICH

        if (match.isNotNull()) {
            matCard {
                attrs.noAction = true
                div(classes = "match-card-comp") {
                    match.opponents?.let { opponents ->

                        div(classes = "match-summary") {
                            +opponents.joinToString(" vs. ") {
                                it.participant?.name ?: I18n.default("NoValue")
                            }
                        }

                        opponents.firstOrNull { it.position == 1 }?.let { winner ->
                            val winnerName = winner.participant?.name.toStringOrNoValue()
                            div(classes = "match-winner") {
                                +"${I18n.default("Winner")} $winnerName"
                            }
                        }

                        div(classes = "match-fields") {
                            labeledText {
                                attrs {
                                    label = I18n.default("MatchType")
                                    text = I18n.default("MatchTypes.${match.type}")
                                }
                            }
                            labeledText {
                                attrs {
                                    label = I18n.default("Status")
                                    text = match.status?.let {
                                        I18n.default("StatusType.$it")
                                    } ?: I18n.default("NoValue")
                                }
                            }

                            match.playedAt?.let {
                                labeledText {
                                    attrs {
                                        label = I18n.default("PlayedAt")
                                        text = getMomentTZ(it, timezone).format(LONG_DATE_TIME_FORMAT)
                                    }
                                }
                            }

                            match.scheduledDatetime?.let {
                                labeledText {
                                    attrs {
                                        label = I18n.default("Scheduled")
                                        text = getMomentTZ(it, timezone).format(LONG_DATE_TIME_FORMAT)
                                    }
                                }
                            }
                        }

                        styledDiv {
                            css {
                                classes.add("match-opponents")
                                gridTemplateRows =
                                        GridTemplateRows(opponents.joinToString(" ") { MAT_CELL_DIMENSION.toString() })
                            }

                            opponents.forEach { opponent ->
                                div(classes = "match-opponent") {
                                    opponent.participant?.let { participant ->
                                        span(classes = "match-opponent-label") {
                                            val name = participant.name ?: I18n.default("NoValue")
                                            +name
                                        }
                                        labeledText {
                                            attrs {
                                                label = I18n.default("Score")
                                                text = "${opponent.score ?: I18n.default("NoValue")}"
                                            }
                                        }
                                        labeledText {
                                            attrs {
                                                label = I18n.default("Rank")
                                                text = "${opponent.rank ?: I18n.default("NoValue")}"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@ExperimentalContracts
fun RBuilder.matchCard(handler: RHandler<MatchCardCompProps>) = child(MatchCardComp::class, handler)