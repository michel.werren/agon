package ch.sourcemotion.agon.ui.material.snackbar

import ch.sourcemotion.agon.extension.isNeitherNullOrEmpty
import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.extension.isNull
import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.ui.material.MatBaseComponent
import kotlinx.html.ButtonType
import react.RBuilder
import react.RHandler
import react.RProps
import react.RState
import react.dom.button
import react.dom.div
import kotlin.contracts.ExperimentalContracts

interface MatSnackbarCompProps : RProps {
    var timeout: Long?
    var buttonText: String?
    var multiline: Boolean?
    var closedHandler: (() -> Unit)?
    var hideHandler: (() -> Unit)?
    var showHandler: (() -> Unit)?
}

/**
 * Snackback component to render an appearing bar with message on the bottom of the page.
 */
@ExperimentalContracts
class MatSnackbarComp : MatBaseComponent<MatSnackbarCompProps, RState, MDCSnackbar>() {
    override fun RBuilder.render() {
        div(classes = "mdc-snackbar") {
            ref {
                if (jsComponent.isNull() && it != null) {
                    jsComponent = MDCSnackbar(it).apply {
                        props.hideHandler?.let {
                            listen("MDCSnackbar:hide", it)
                        }
                        props.showHandler?.let {
                            listen("MDCSnackbar:show", it)
                        }
                    }
                }
            }

            attrs {
                attributes["aria-live"] = "assertive"
                attributes["aria-atomic"] = "true"
                attributes["aria-hidden"] = "true"
            }
            div(classes = "mdc-snackbar__text") { }
            div(classes = "mdc-snackbar__action-wrapper") {
                button(classes = "mdc-snackbar__action-button", type = ButtonType.button) {}
            }
        }
    }

    fun showMessage(message: String,
                    timeout: Long? = null,
                    multiline: Boolean? = null,
                    closedHandler: (() -> Unit)? = null,
                    buttonText: String? = null) {

        jsComponent?.let { jsComp ->

            val finalTimeout = timeout ?: props.timeout ?: 2750 * 2
            val finalClosedHandler = closedHandler ?: props.closedHandler
            val finalButtonText = buttonText ?: props.buttonText
            val finalMultiline = multiline ?: props.multiline

            val data = js("{}")
            if (finalClosedHandler.isNotNull() && finalButtonText.isNeitherNullOrEmpty()) {
                data["actionText"] = finalButtonText
                data["actionHandler"] = finalClosedHandler
            }
            data["timeout"] = "$finalTimeout"
            data["message"] = message
            data["multiline"] = finalMultiline.isTrue()
            jsComp.show(data)
        }
    }
}

@ExperimentalContracts
fun RBuilder.matSnackbar(handler: RHandler<MatSnackbarCompProps>) = child(MatSnackbarComp::class, handler)