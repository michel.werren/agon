package ch.sourcemotion.agon.ui.request

import ch.sourcemotion.agon.dto.GroupDto
import ch.sourcemotion.agon.TournamentPath
import ch.sourcemotion.agon.ui.rest.RestGetRequest


class GroupRequest(tournamentId: String, groupId: String): RestGetRequest<GroupDto>("${TournamentPath.ROOT_PATH}/$tournamentId/groups/$groupId", GroupDto.serializer())