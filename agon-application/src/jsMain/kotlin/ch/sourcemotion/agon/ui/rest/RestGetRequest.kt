package ch.sourcemotion.agon.ui.rest

import ch.sourcemotion.agon.extension.isNeitherNullOrEmpty
import kotlinx.serialization.KSerializer
import kotlin.contracts.ExperimentalContracts

/**
 * Base class to build requests in a save way.
 */
abstract class RestGetRequest<T> protected constructor(val path: String, val responseSerializer: KSerializer<T>, queryParams: Map<String, Any>? = null) {
    protected val queryParams : MutableMap<String, Any> = HashMap()

    init {
        queryParams?.let {
            this.queryParams.putAll(it)
        }
    }

    @ExperimentalContracts
    fun toQueryString() : String {
        val urlQueryParameters= queryParams.map { entry -> "${entry.key}=${entry.value}" }.joinToString("&")
        return if (urlQueryParameters.isNeitherNullOrEmpty()) {
            "$path?$urlQueryParameters"
        } else {
            path
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is RestGetRequest<*>) return false

        if (path != other.path) return false
        if (queryParams != other.queryParams) return false

        return true
    }

    override fun hashCode(): Int {
        var result = path.hashCode()
        result = 31 * result + queryParams.hashCode()
        return result
    }
}