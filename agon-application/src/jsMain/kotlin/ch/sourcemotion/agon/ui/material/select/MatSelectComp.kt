package ch.sourcemotion.agon.ui.material.select

import ch.sourcemotion.agon.extension.isNeitherNullOrEmpty
import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.ui.material.MatBaseComponent
import ch.sourcemotion.agon.ui.util.UUIDJS
import ch.sourcemotion.agon.ui.util.asElementId
import kotlinx.css.LinearDimension
import kotlinx.html.DIV
import kotlinx.html.InputType
import kotlinx.html.id
import mu.KotlinLogging
import react.*
import react.dom.*
import styled.css
import styled.styledDiv
import kotlin.browser.document
import kotlin.contracts.ExperimentalContracts


class MatSelectModel(val label: String, val value: String, var selected: Boolean = false)

interface MatSelectCompProps : RProps {
    var label: String?
    var outlined: Boolean?
    var disabled: Boolean?
    var model: List<MatSelectModel>?
    var deselectable: Boolean?
    var width: Int?
    var helperText: ReactElement?
    var changeHandler: ((index: Int, value: String) -> Unit)?
}

@ExperimentalContracts
class MatSelectComp : MatBaseComponent<MatSelectCompProps, RState, MDCSelect>() {

    companion object {
        private val log = KotlinLogging.logger("MatSelectComp")
    }

    private val selectDivId = UUIDJS.genV4().asElementId()

    override fun componentDidMount() {
        val divElement = document.querySelector("#$selectDivId")
        if (divElement.isNotNull()) {
            val mdcSelect = MDCSelect(divElement).apply {
                props.changeHandler?.let { handler ->
                    listen("MDCSelect:change") {
                        log.debug { "MDCSelect:change event" }
                        // When choice is deselectable, the index is increased
                        val realIndex = if (props.deselectable.isTrue()) {
                            selectedIndex - 1
                        } else {
                            selectedIndex
                        }
                        handler(realIndex, value)
                    }
                }
            }
            jsComponent = mdcSelect
        } else {
            log.error { "Unable to query material select root div by id" }
        }
    }

    override fun RBuilder.render() {

        log.debug { "Render Select" }

        props.model?.let { model ->
            model.firstOrNull { it.label.contains("10") }?.let {
                log.debug { "Found element ${it.value} / ${it.label}" }
            }
        }

        val width = if (props.width.isNotNull()) {
            LinearDimension("${props.width}px")
        } else {
            LinearDimension("100px")
        }

        styledDiv {
            css {
                classes.add(buildRootCss())
                this.width = width
            }

            attrs {
                id = selectDivId
            }

            input {
                attrs {
                    type = InputType.hidden
                    disabled = props.disabled.isTrue()
                }
            }

            i(classes = "mdc-select__dropdown-icon") {}

            div(classes = "mdc-select__selected-text") {}

            styledDiv {
                css {
                    classes.add("mdc-select__menu")
                    classes.add("mdc-menu mdc-menu-surface")
                    this.width = width
                }
                ul(classes = "mdc-list") {
                    if (props.deselectable.isTrue()) {
                        li(classes = buildDeselectItemCss()) {
                            attrs {
                                attributes["data-value"] = ""
                            }
                        }
                    }
                    if (props.model.isNeitherNullOrEmpty()) {
                        ArrayList(props.model!!).forEach { entry ->
                            li(classes = buildItemCss(entry)) {
                                attrs {
                                    if (entry.selected) {
                                        attributes["aria-selected"] = "true"
                                        log.debug { "'${entry.value}' / '${entry.label}' is selected" }
                                    }
                                    attributes["data-value"] = entry.value
                                }
                                +entry.label
                            }
                        }
                    }
                }
            }

            if (props.outlined.isTrue()) {
                renderOutlined()
            } else {
                renderRipple()
            }
        }
        props.helperText?.let {
            childList.add(it)
        }
    }

    private fun RDOMBuilder<DIV>.renderRipple() {
        span(classes = "mdc-floating-label") {
            props.label?.let { +it }
        }
        div(classes = "mdc-line-ripple") { }
    }

    private fun RDOMBuilder<DIV>.renderOutlined() {
        div(classes = "mdc-select--outlined") {
            div(classes = "mdc-notched-outline") {
                div(classes = "mdc-notched-outline__leading") { }
                div(classes = "mdc-notched-outline__notch") {
                    label(classes = "mdc-floating-label") {
                        props.label?.let { +it }
                    }
                }
                div(classes = "mdc-notched-outline__trailing") { }
            }
        }
    }

    private fun buildDeselectItemCss(): String {
        return if (hasSelected()) {
            "mdc-list-item mdc-list-item"
        } else {
            "mdc-list-item mdc-list-item--selected"
        }
    }

    private fun buildRootCss(): String {
        var css = "mdc-select"
        if (props.outlined.isTrue()) {
            css += " mdc-select--outlined"
        }
        if (props.disabled.isTrue()) {
            css += " mdc-select--disabled"
        }
        return css
    }

    private fun buildItemCss(model: MatSelectModel): String {
        var css = "mdc-list-item"
        if (model.selected) {
            css += " mdc-list-item--selected"
        }
        return css
    }

    private fun hasSelected() = props.model?.firstOrNull { it.selected }.isNotNull()
}

@ExperimentalContracts
fun RBuilder.matSelect(handler: RHandler<MatSelectCompProps>) = child(MatSelectComp::class, handler)