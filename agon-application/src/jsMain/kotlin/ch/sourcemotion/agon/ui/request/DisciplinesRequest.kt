package ch.sourcemotion.agon.ui.request

import ch.sourcemotion.agon.dto.DisciplineDto
import ch.sourcemotion.agon.DisciplinePath
import ch.sourcemotion.agon.ui.rest.RestGetRequest
import kotlinx.serialization.list


class DisciplinesRequest : RestGetRequest<List<DisciplineDto>>(DisciplinePath.GET_DISCIPLINES, DisciplineDto.serializer().list)