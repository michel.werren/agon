package ch.sourcemotion.agon.ui.material.icon

import ch.sourcemotion.agon.extension.isNeitherNullOrEmpty
import ch.sourcemotion.agon.extension.isNotNull
import kotlinx.html.js.onClickFunction
import react.*
import react.dom.i
import kotlin.contracts.ExperimentalContracts

interface MatIconCompProps : RProps {
    var icon: MatIcon?
    var clickHandler: (() -> Unit)?
    var classes: String?
}

/**
 * Component to render a material icon
 */
@ExperimentalContracts
class MatIconComp : RComponent<MatIconCompProps, RState>() {
    override fun RBuilder.render() {
        props.icon?.let { icon ->
            i(classes = getRootCss()) {
                props.clickHandler?.let { handler ->
                    attrs {
                        onClickFunction = {
                            handler()
                        }
                    }
                }
                +icon.getIconName()
            }
        }
    }

    private fun getRootCss(): String {
        var css = "material-icons"
        if (props.clickHandler.isNotNull()) {
            css += " mat-icon-comp-as-button"
        }
        if (props.classes.isNeitherNullOrEmpty()) {
            css += " ${props.classes}"
        }
        return css
    }
}

@ExperimentalContracts
fun RBuilder.matIcon(handler: RHandler<MatIconCompProps>) = child(MatIconComp::class, handler)