package ch.sourcemotion.agon.ui.material.checkbox

import ch.sourcemotion.agon.extension.isFalse
import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.extension.isNull
import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.ui.component.fixLabel
import ch.sourcemotion.agon.ui.material.MatBaseComponent
import ch.sourcemotion.agon.ui.material.form.MDCFormField
import ch.sourcemotion.agon.ui.util.UUIDJS
import ch.sourcemotion.agon.ui.util.asElementId
import kotlinx.html.InputType
import kotlinx.html.id
import kotlinx.html.js.onChangeFunction
import react.RBuilder
import react.RHandler
import react.RProps
import react.RState
import react.dom.div
import react.dom.input
import react.dom.svg
import kotlin.contracts.ExperimentalContracts

enum class MatCheckBoxState {
    UNCHECKED,
    CHECKED,
    INTERMEDIATE;

    companion object {
        fun forMDCCheckbox(checkbox: MDCCheckBox): MatCheckBoxState {
            return when {
                checkbox.checked -> CHECKED
                checkbox.indeterminate -> INTERMEDIATE
                else -> UNCHECKED
            }
        }
    }
}

interface MatCheckBoxCompProps : RProps {
    var label: String?
    var state: MatCheckBoxState?
    var disabled: Boolean?
    var changeHandler: ((MatCheckBoxState) -> Unit)?
}

@ExperimentalContracts
class MatCheckBoxComp : MatBaseComponent<MatCheckBoxCompProps, RState, MDCCheckBox>() {

    private val checkboxId = UUIDJS.genV4().asElementId()

    private var formFieldJs: MDCFormField? = null
    /**
     * Flag to determine if the MDC checkbox is defined as input on form field.
     */
    private var formFieldInputAssigned = false

    override fun RBuilder.render() {
        div(classes = "mdc-form-field") {
            ref {
                if (formFieldJs.isNull() && it != null) {
                    formFieldJs = MDCFormField(it)
                }
                // In the case the MDC checkbox was instantiated first
                if (jsComponent.isNotNull() && formFieldInputAssigned.isFalse()) {
                    formFieldJs!!.input = jsComponent!!
                    formFieldInputAssigned = true
                }
            }

            div(classes = buildCheckboxCss()) {
                ref {
                    if (jsComponent.isNull() && it != null) {
                        jsComponent = MDCCheckBox(it).apply {
                            applyProps(props)
                        }
                    }
                    // In the case the MDC form field was instantiated first
                    if (formFieldJs.isNotNull() && formFieldInputAssigned.isFalse()) {
                        formFieldJs!!.input = jsComponent!!
                        formFieldInputAssigned = true
                    }
                }

                input(classes = "mdc-checkbox__native-control") {
                    attrs {
                        type = InputType.checkBox
                        disabled = isDisabled()
                        id = checkboxId
                        props.changeHandler?.let { handler ->
                            onChangeFunction = {
                                jsComponent?.let { checkbox ->
                                    val state = MatCheckBoxState.forMDCCheckbox(checkbox)
                                    handler(state)
                                }
                            }
                        }
                    }
                }

                div(classes = "mdc-checkbox__background") {
                    svg(classes = "mdc-checkbox__checkmark") {
                        attrs {
                            attributes["viewBox"] = "0 0 24 24"
                        }

                        path(classes = "mdc-checkbox__checkmark-path") {
                            attrs {
                                fill = "none"
                                d = "M1.73,12.91 8.1,19.28 22.79,4.59"
                            }
                        }
                    }
                    div(classes = "mdc-checkbox__mixedmark") { }
                }
            }
            fixLabel {
                attrs {
                    htmlFor = checkboxId
                }
                props.label?.let { +it }
            }
        }
    }

    override fun componentWillUpdate(nextProps: MatCheckBoxCompProps, nextState: RState) {
        jsComponent?.applyProps(nextProps)
    }

    private fun MDCCheckBox.applyProps(props: MatCheckBoxCompProps) {
        val finalState = if (props.state.isNotNull()) {
            props.state!!
        } else {
            MatCheckBoxState.UNCHECKED
        }
        when (finalState) {
            MatCheckBoxState.CHECKED -> {
                checked = true
                indeterminate = false
            }
            MatCheckBoxState.INTERMEDIATE -> {
                checked = false
                indeterminate = true
            }
            MatCheckBoxState.UNCHECKED -> {
                checked = false
                indeterminate = false
            }
        }
    }

    override fun componentWillUnmount() {
        formFieldJs?.destroy()
        super.componentWillUnmount()
    }

    private fun buildCheckboxCss(): String {
        var css = "mdc-checkbox"
        if (isDisabled()) {
            css += " mdc-checkbox--disabled"
        }

        return css
    }

    private fun isDisabled() = props.disabled.isTrue()
}

@ExperimentalContracts
fun RBuilder.matCheckbox(handler: RHandler<MatCheckBoxCompProps>) = child(MatCheckBoxComp::class, handler)