package ch.sourcemotion.agon.ui

import ch.sourcemotion.agon.ui.material.elevation.matElevation
import react.*
import react.dom.div
import kotlin.contracts.ExperimentalContracts

/**
 * Application component. Parent of any navigation and therefore page on the application.
 */
@ExperimentalContracts
class App : RComponent<RProps, RState>() {
    override fun RBuilder.render() {
        div(classes = "agon-app") {
            matElevation {
                attrs {
                    level = 1
                }
                props.children()
            }
        }
    }
}

@ExperimentalContracts
fun RBuilder.app(handler: RHandler<RProps>) = child(App::class, handler)