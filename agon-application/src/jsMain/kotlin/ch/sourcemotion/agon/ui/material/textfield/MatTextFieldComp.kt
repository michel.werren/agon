package ch.sourcemotion.agon.ui.material.textfield

import ch.sourcemotion.agon.extension.isFalse
import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.extension.isNull
import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.ui.component.fixLabel
import ch.sourcemotion.agon.ui.extension.changeState
import ch.sourcemotion.agon.ui.material.MatBaseComponent
import ch.sourcemotion.agon.ui.material.icon.MatIcon
import ch.sourcemotion.agon.ui.util.UUIDJS
import ch.sourcemotion.agon.ui.util.asElementId
import kotlinx.html.DIV
import kotlinx.html.InputType
import kotlinx.html.id
import kotlinx.html.js.onChangeFunction
import kotlinx.html.js.onKeyDownFunction
import org.w3c.dom.HTMLInputElement
import react.*
import react.dom.RDOMBuilder
import react.dom.div
import react.dom.input
import kotlin.contracts.ExperimentalContracts

enum class Key(val code: Int) {
    RETURN(13)
}

data class KeyPressed(val key: Key, val keyDownHandler: (() -> Unit))

interface MatTextFieldCompProps : RProps {
    var initialText: String?
    var hintText: String?
    var clear: Boolean?
    var outlined: Boolean?
    var trailingIcon: MatIcon?
    var leadingIcon: MatIcon?
    var focusOnIconClick: Boolean?
    var required: Boolean?
    var readOnly: Boolean?
    var helperText: ReactElement?
    var changeHandler: ((String) -> Unit)?
    var keyPressed: KeyPressed?
}

interface MatTextFieldCompState : RState {
    /**
     * We need an initialized state here because React only controls inputs with non null values. So we need to set at least an
     * empty [String] on the very first rendering
     */
    var initialized: Boolean?
}

/**
 * Material text field component.
 */
@ExperimentalContracts
class MatTextFieldComp : MatBaseComponent<MatTextFieldCompProps, MatTextFieldCompState, MDCTextField>() {

    private val inputId = UUIDJS.genV4().asElementId()
    private var inputElement: HTMLInputElement? = null

    override fun RBuilder.render() {

        div(classes = buildTextFieldCss()) {
            ref {
                if (jsComponent.isNull() && it != null) {
                    jsComponent = MDCTextField(it)
                    jsComponent!!.value = props.initialText ?: ""
                }
            }
            props.leadingIcon?.let { icon ->
                matTextFieldIcon {
                    attrs.icon = icon
                }
            }
            input(classes = "mdc-text-field__input") {
                ref {
                    if (inputElement.isNull()) {
                        inputElement = it
                    }
                }
                attrs {
                    type = InputType.text
                    id = inputId
                    required = props.required.isTrue()
                    readonly = props.readOnly.isTrue()

                    // Ensure component is controlled by React
                    when {
                        state.initialized.isFalse() -> value = ""
                        props.clear.isTrue() -> {
                            jsComponent?.let {
                                it.value = ""
                            }
                            value = ""
                        }
                        else -> jsComponent?.let {
                            value = it.value
                        }
                    }

                    props.changeHandler?.let { handler ->
                        onChangeFunction = {
                            jsComponent?.let { ref ->
                                handler(ref.value)
                            }
                        }
                    }

                    props.keyPressed?.let {
                        onKeyDownFunction = {event ->
                            if (event.asDynamic().keyCode == it.key.code) {
                                it.keyDownHandler()
                            }
                        }
                    }
                }
            }
            if (props.outlined.isTrue()) {
                renderOutlined()
            } else {
                fixLabel(classes = "mdc-floating-label") {
                    attrs {
                        htmlFor = inputId
                    }
                    props.hintText?.let { +it }
                }
                props.trailingIcon?.let { icon ->
                    matTextFieldIcon {
                        attrs.icon = icon
                    }
                }
                div(classes = "mdc-line-ripple") { }
            }
        }
        props.helperText?.let {
            childList.add(it)
        }
    }

    override fun componentDidMount() {
        changeState { initialized = true }
    }

    override fun componentWillUpdate(nextProps: MatTextFieldCompProps, nextState: MatTextFieldCompState) {
        if (nextProps.initialText != props.initialText) {
            nextProps.initialText?.let { initialText ->
                jsComponent!!.value = initialText
            }
        }
    }

    private fun RDOMBuilder<DIV>.renderOutlined() {
        props.trailingIcon?.let { icon ->
            matTextFieldIcon {
                attrs {
                    this.icon = icon
                    if (props.focusOnIconClick.isTrue()) {
                        clickHandler = {
                            jsComponent?.focus()
                        }
                    }
                }
            }
        }
        div(classes = "mdc-notched-outline") {
            div(classes = "mdc-notched-outline__leading") {}
            div(classes = "mdc-notched-outline__notch") {
                fixLabel(classes = "mdc-floating-label") {
                    attrs {
                        htmlFor = inputId
                    }
                    props.hintText?.let { +it }
                }
            }
            div(classes = "mdc-notched-outline__trailing") {}
        }
    }

    private fun buildTextFieldCss(): String {
        var css = "mdc-text-field mat-text-field-comp"
        if (props.outlined.isTrue()) {
            css += " mdc-text-field--outlined"
        }
        if (props.leadingIcon.isNotNull()) {
            css += " mdc-text-field--with-leading-icon"
        }
        if (props.trailingIcon.isNotNull()) {
            css += " mdc-text-field--with-trailing-icon"
        }
        return css
    }

    fun getTextField() = inputElement
}

@ExperimentalContracts
fun RBuilder.matTextField(handler: RHandler<MatTextFieldCompProps>) = child(MatTextFieldComp::class, handler)