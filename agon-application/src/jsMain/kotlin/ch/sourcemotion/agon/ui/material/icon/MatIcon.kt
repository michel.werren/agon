package ch.sourcemotion.agon.ui.material.icon

interface MatIcon {
    fun getIconName(): String
}

/**
 * Collection of used material icons
 */
enum class MatIcons(private val iconName: String) : MatIcon {
    ADD("add"),
    BACKSPACE("keyboard_backspace"),
    CALENDAR("calendar_today"),
    SORT_ASCENDING("arrow_downward"),
    SORT_DESCENDING("arrow_upward"),
    SORT_NONE("swap_vert"),
    PAGINATION_PREVIOUS("keyboard_arrow_left"),
    PAGINATION_NEXT("keyboard_arrow_right"),
    NAVIGATION("navigation"),
    CLEAR("clear"),
    DETAILS("details"),
    REFRESH("refresh"),
    SEARCH("search");

    override fun getIconName() = iconName
}