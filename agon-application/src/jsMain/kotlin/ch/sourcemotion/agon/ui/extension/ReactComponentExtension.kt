package ch.sourcemotion.agon.ui.extension

import kotlinext.js.clone
import react.Component
import react.RComponent
import react.RProps
import react.RState

external interface ReactHistory {
    fun goBack()
    fun push(path: String)
}

/**
 * Extension for lesser verbose React component state changes.
 */
fun <P : RProps, S : RState> Component<P, S>.changeState(callback: S.() -> Unit) {
    setState(clone(state).apply(callback))
}

fun RComponent<*, *>.navigate(path: String) {
    this.props.asDynamic().history.push(path)
}

fun RComponent<*, *>.navigateBack() {
    this.props.asDynamic().history.goBack
}

fun RComponent<*, *>.getReactHistory(): ReactHistory {
    return this.props.asDynamic().history
}

/**
 * Extracts a path param from the current location like /device/:id. Then the value of the param name "id" will be extracted.
 *
 * @return Path param value
 */
fun RComponent<*, *>.getPathParam(paramName: String): String? {
    return this.props.asDynamic().match.params[paramName]
}

fun RComponent<*, *>.getPathParams(): List<String>? {
    return this.props.asDynamic().match.params
}
