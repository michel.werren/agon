package ch.sourcemotion.agon.ui.material.button

import ch.sourcemotion.agon.extension.isNull
import ch.sourcemotion.agon.extension.isNullOrTrue
import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.ui.material.MatBaseComponent
import kotlinx.html.js.onClickFunction
import react.RBuilder
import react.RHandler
import react.RProps
import react.RState
import react.dom.button
import kotlin.contracts.ExperimentalContracts

interface MatButtonProps : RProps {
    var label: String?
    var disabled: Boolean?
    var raised: Boolean?
    var outlined: Boolean?
    var unelevated: Boolean?
    var clickHandler: (() -> Unit)?
}

@ExperimentalContracts
class MatButtonComp : MatBaseComponent<MatButtonProps, RState, MDCRipple>() {

    override fun RBuilder.render() {
        button(classes = buildCssClasses()) {
            ref {
                if (jsComponent.isNull() && it != null) {
                    jsComponent = MDCRipple(it)
                }
            }
            props.label?.let { +it }
            attrs {
                disabled = props.disabled.isTrue()
                props.clickHandler?.let {
                    onClickFunction = {
                        it()
                    }
                }
            }
        }
    }

    private fun buildCssClasses(): String {
        var css = "mdc-button"
        if (props.raised.isNullOrTrue()) {
            css += " mdc-button--raised"
        }
        if (props.outlined.isTrue()) {
            css += " mdc-button--outlined"
        }
        if (props.unelevated.isTrue()) {
            css += " mdc-button--unelevated"
        }
        return css
    }
}

@ExperimentalContracts
fun RBuilder.matButton(handler: RHandler<MatButtonProps>) = child(MatButtonComp::class, handler)