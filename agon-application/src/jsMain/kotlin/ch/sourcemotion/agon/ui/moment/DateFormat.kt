package ch.sourcemotion.agon.ui.moment


enum class DateFormat(val momentFormat: String, val faltPickrFormat: String) {
    DATE_ONLY("DD-MM-YYYY", "Y-m-d")
}