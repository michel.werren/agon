package ch.sourcemotion.agon.ui.request

import ch.sourcemotion.agon.dto.TournamentDetailDto
import ch.sourcemotion.agon.TournamentPath
import ch.sourcemotion.agon.ui.rest.RestGetRequest

/**
 * Request to get the detail of a tournament.
 */
class TournamentDetailRequest(id: String) : RestGetRequest<TournamentDetailDto>("${TournamentPath.ROOT_PATH}/$id", TournamentDetailDto.serializer())