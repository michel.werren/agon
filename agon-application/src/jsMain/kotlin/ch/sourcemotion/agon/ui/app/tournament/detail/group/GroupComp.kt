package ch.sourcemotion.agon.ui.app.tournament.detail.group

import ch.sourcemotion.agon.dto.GroupDto
import ch.sourcemotion.agon.dto.RoundDto
import ch.sourcemotion.agon.dto.StageDto
import ch.sourcemotion.agon.dto.TournamentDetailDto
import ch.sourcemotion.agon.extension.isNeitherNullOrEmpty
import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.extension.or
import ch.sourcemotion.agon.ui.app.tournament.detail.TournamentGraphComp
import ch.sourcemotion.agon.ui.component.description.DescriptionCardEntry
import ch.sourcemotion.agon.ui.component.description.descriptionCard
import ch.sourcemotion.agon.ui.component.failurehandling.FailureHandlingCompState
import ch.sourcemotion.agon.ui.component.titled.titled
import ch.sourcemotion.agon.ui.extension.changeState
import ch.sourcemotion.agon.ui.extension.getPathParam
import ch.sourcemotion.agon.ui.extension.getReactHistory
import ch.sourcemotion.agon.ui.extension.navigate
import ch.sourcemotion.agon.ui.i18n.I18n
import ch.sourcemotion.agon.ui.material.loading.matLoadingModal
import ch.sourcemotion.agon.ui.request.GroupRequest
import ch.sourcemotion.agon.ui.request.RoundsOfGroupsRequest
import ch.sourcemotion.agon.ui.request.StageRequest
import ch.sourcemotion.agon.ui.request.TournamentDetailRequest
import ch.sourcemotion.agon.ui.rest.RestClient
import ch.sourcemotion.agon.ui.rest.RestClientException
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import mu.KotlinLogging
import react.RBuilder
import react.RProps
import react.dom.div
import kotlin.contracts.ExperimentalContracts

interface GroupCompState : FailureHandlingCompState {
    var tournament: TournamentDetailDto?
    var stage: StageDto?
    var group: GroupDto?
    var rounds: List<RoundDto>?
}

/**
 * Routing target component to show graph to a group and the rounds within this group.
 */
@ExperimentalContracts
class GroupComp : TournamentGraphComp<RProps, GroupCompState>() {

    companion object {
        private val log = KotlinLogging.logger("GroupComp")
    }

    override fun RBuilder.render() {
        renderFailureDialog()

        val tournament = state.tournament
        val group = state.group
        val stage = state.stage
        val rounds = state.rounds

        if (tournament.isNotNull() && group.isNotNull() && stage.isNotNull() && rounds.isNotNull()) {
            titled {
                attrs {
                    title = "${I18n.default("Group")}: ${group.name}"
                    history = getReactHistory()
                }

                div(classes = "group-comp") {
                    renderGraphElement(I18n.default("TournamentName"), tournament.name)
                    renderGraphElement(I18n.default("StageName"), stage.name)
                    renderGraphElement(I18n.default("GroupName"), group.name)
                    rounds.sortedBy { it.number }.forEach { round ->
                        descriptionCard {
                            attrs {
                                entries = listOf(
                                    DescriptionCardEntry(I18n.default("RoundName"), round.name),
                                    DescriptionCardEntry(
                                        I18n.default("RoundSize"),
                                        round.settings?.size?.toString() ?: I18n.noValue()
                                    ),
                                    DescriptionCardEntry(
                                        I18n.default("Closed"), if (round.closed.isTrue()) {
                                            I18n.default("Yes")
                                        } else {
                                            I18n.default("No")
                                        }
                                    )
                                )
                                clickHandler = {
                                    navigate("/rounds/${tournament.id}/${stage.id}/${group.id}/${round.id}")
                                }
                            }
                        }
                    }
                }
            }
        } else {
            matLoadingModal {
                attrs {
                    show = state.failureMessage.isNullOrBlank()
                    text = I18n.default("LoadMessage.LoadGroup")
                    indeterminate = true
                }
            }
        }
    }

    override fun componentDidMount() {
        val tournamentId = getPathParam("tournamentId").or {
            log.error { "Tournament id is missing" }
            null
        }
        val stageId = getPathParam("stageId").or {
            log.error { "Stage id is missing" }
            null
        }
        val groupId = getPathParam("groupId").or {
            log.error { "Group id is missing" }
            null
        }

        if (tournamentId.isNeitherNullOrEmpty() && stageId.isNeitherNullOrEmpty() && groupId.isNeitherNullOrEmpty()) {
            GlobalScope.launch {
                val tournamentJob = async {
                    getPathParam("tournamentId")?.let {
                        try {
                            RestClient.get(TournamentDetailRequest(it))
                        } catch (e: RestClientException) {
                            openFailureDialog(I18n.failure("Tournament", e.failure))
                            null
                        }
                    }
                }

                val stageJob = async {
                    getPathParam("tournamentId")?.let { tournamentId ->
                        getPathParam("stageId")?.let { stageId ->
                            try {
                                RestClient.get(StageRequest(tournamentId, stageId))
                            } catch (e: RestClientException) {
                            openFailureDialog(I18n.failure("Stage", e.failure))
                                null
                            }
                        }
                    }
                }
                val groupJob = async {
                    getPathParam("tournamentId")?.let { tournamentId ->
                        getPathParam("groupId")?.let { groupId ->
                            try {
                                RestClient.get(GroupRequest(tournamentId, groupId))
                            } catch (e: RestClientException) {
                                openFailureDialog(I18n.failure("Group", e.failure))
                                null
                            }
                        }
                    }
                }
                val roundsJob = async {
                    getPathParam("tournamentId")?.let { tournamentId ->
                        getPathParam("groupId")?.let { groupId ->
                            try {
                                RestClient.get(RoundsOfGroupsRequest(tournamentId, groupId))
                            } catch (e: RestClientException) {
                                openFailureDialog(I18n.failure("Rounds", e.failure))
                                null
                            }
                        }
                    }
                }
                val tournament = tournamentJob.await()
                val stage = stageJob.await()
                val group = groupJob.await()
                val rounds = roundsJob.await()

                changeState {
                    this.tournament = tournament
                    this.stage = stage
                    this.group = group
                    this.rounds = rounds
                }
            }
        }
    }
}