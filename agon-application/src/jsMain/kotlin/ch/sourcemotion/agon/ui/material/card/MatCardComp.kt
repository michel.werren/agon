package ch.sourcemotion.agon.ui.material.card

import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.extension.isNull
import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.ui.material.icon.MatIcon
import kotlinx.css.LinearDimension
import kotlinx.html.js.onClickFunction
import react.*
import react.dom.button
import react.dom.div
import styled.css
import styled.styledDiv
import kotlin.contracts.ExperimentalContracts


class MatCardTextButtons {
    val buttons = ArrayList<MatCardTextButton>()

    companion object {
        fun textButtons(block: MatCardTextButtons.() -> Unit): MatCardTextButtons {
            val textButtons = MatCardTextButtons()
            block(textButtons)
            return textButtons
        }
    }

    fun button(block: MatCardTextButton.() -> Unit) {
        buttons.add(MatCardTextButton.textButton(block))
    }
}

class MatCardTextButton {
    var label: String? = null
    var handler: (() -> Unit)? = null

    companion object {
        fun textButton(block: MatCardTextButton.() -> Unit): MatCardTextButton {
            val buttonDescription = MatCardTextButton()
            block(buttonDescription)
            return buttonDescription
        }
    }
}

class MatCardIconButtons {

    val buttons = ArrayList<MatCardIconButton>()
    companion object {

        fun iconButtons(block: MatCardIconButtons.() -> Unit): MatCardIconButtons {
            val iconButtons = MatCardIconButtons()
            block(iconButtons)
            return iconButtons
        }
    }
    fun button(block: MatCardIconButton.() -> Unit) {
        buttons.add(MatCardIconButton.iconButton(block))
    }
}

class MatCardIconButton {
    var icon: MatIcon? = null
    var handler: (() -> Unit)? = null

    companion object {
        fun iconButton(block: MatCardIconButton.() -> Unit): MatCardIconButton {
            val buttonDescription = MatCardIconButton()
            block(buttonDescription)
            return buttonDescription
        }
    }
}

@ExperimentalContracts
interface MatCardCompProps : RProps {
    var mediaElement: ReactElement?
    var textButtons: MatCardTextButtons?
    var iconButtons: MatCardIconButtons?
    var classes: String?
    var height: String?
    var width: String?
    var noAction: Boolean?
}

@ExperimentalContracts
class MatCardComp : RComponent<MatCardCompProps, RState>() {

    override fun RBuilder.render() {
        styledDiv {
            css {
                classes.add(getRootClasses())

                props.width?.let {
                    width = LinearDimension(it)
                }
                props.height?.let {
                    height = LinearDimension(it)
                }
            }

            props.mediaElement?.let { mediaElement ->
                div(classes = "mdc-card__media mdc-card__media--square") {
                    div(classes = "mdc-card__media-content") {
                        child(mediaElement)
                    }
                }
            }

            if (props.mediaElement.isNull()) {
                if (props.noAction.isTrue()) {
                    props.children()
                } else {
                    div(classes = "mdc-card__primary-action") {
                        props.children()
                    }
                }
            }

            if (hasActions()) {
                div(classes = "mdc-card__actions") {
                    props.textButtons?.let { textButtons ->
                        textButtons.buttons.forEach { but ->
                            button(classes = "mdc-button mdc-card__action mdc-card__action--button") {
                                attrs {
                                    but.handler?.let { handler ->
                                        onClickFunction = {
                                            handler()
                                        }
                                    }
                                }
                                but.label?.let { +it }
                            }
                        }
                    }
                    div(classes = "mdc-card__action-icons") {
                        props.iconButtons?.let { textButtons ->
                            textButtons.buttons.forEach { but ->
                                button(classes = "material-icons mdc-icon-button mdc-card__action mdc-card__action--icon") {
                                    attrs {
                                        but.handler?.let { handler ->
                                            onClickFunction = {
                                                handler()
                                            }
                                        }
                                    }
                                    but.icon?.let { +it.getIconName() }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun getRootClasses(): String {
        var css = "mdc-card"
        props.classes?.let { classes ->
            css += " $classes"
        }
        return css
    }

    private fun hasActions() = props.textButtons.isNotNull() || props.iconButtons.isNotNull()
}

@ExperimentalContracts
fun RBuilder.matCard(handler: RHandler<MatCardCompProps>) = child(MatCardComp::class, handler)