package ch.sourcemotion.agon.ui.material.tabbar

import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.extension.isNull
import ch.sourcemotion.agon.ui.material.MatBaseComponent
import ch.sourcemotion.agon.ui.material.icon.MatIcon
import kotlinx.html.role
import kotlinx.html.tabIndex
import mu.KotlinLogging
import react.RBuilder
import react.RHandler
import react.RProps
import react.RState
import react.dom.button
import react.dom.div
import react.dom.span
import kotlin.contracts.ExperimentalContracts

/**
 * Descrption of a single tab
 */
class TabDescription {
    var label: String? = null
    var icon: MatIcon? = null
    var activatedHandler: ((Int) -> Unit)? = null
}

/**
 * Summary of all tabs to render
 */
class TabsDescription private constructor(val tabs: MutableList<TabDescription> = ArrayList()) {
    companion object {
        fun tabs(block: TabsDescription.() -> Unit): TabsDescription {
            val tabsDescription = TabsDescription()
            block(tabsDescription)
            return tabsDescription
        }
    }

    fun tab(block: TabDescription.() -> Unit) {
        val description = TabDescription()
        block(description)
        tabs.add(description)
    }
}

interface MatTabBarCompProps : RProps {
    var description: TabsDescription?
    var activeIndex: Int?
}

/**
 * Tab bar component. Optional with leading icons
 */
@ExperimentalContracts
class MatTabBarComp : MatBaseComponent<MatTabBarCompProps, RState, MDCTabBar>() {

    companion object {
        private val log = KotlinLogging.logger("MatTabBarComp")
    }

    override fun RBuilder.render() {
        div(classes = "mdc-tab-bar") {
            ref {
                if (jsComponent.isNull() && it != null) {
                    jsComponent = MDCTabBar(it).apply {
                        listen("MDCTabBar:activated") { event ->
                            onTabActivation(event.detail.index)
                        }
                    }
                }
            }
            attrs.role = "tablist"

            props.description?.let { tabsDescription ->
                val tabs = tabsDescription.tabs
                div(classes = "mdc-tab-scroller") {
                    div(classes = "mdc-tab-scroller__scroll-area") {
                        div(classes = "mdc-tab-scroller__scroll-content") {
                            tabs.forEachIndexed { index, tab ->
                                button(classes = "mdc-tab mdc-tab--active") {
                                    val activeTab = index == props.activeIndex ?: 0
                                    attrs {
                                        role = "tab"
                                        if (activeTab) {
                                            attributes["aria-selected"] = "true"
                                            tabIndex = "0"
                                        } else {
                                            attributes["aria-selected"] = "false"
                                            tabIndex = "-1"
                                        }
                                    }
                                    span(classes = "mdc-tab__content") {
                                        if (tab.icon.isNotNull()) {
                                            span(classes = "mdc-tab__icon material-icons") {
                                                +tab.icon!!.getIconName()
                                            }
                                        }
                                        span(classes = "mdc-tab__text-label") {
                                            tab.label?.let { +it }
                                        }
                                    }
                                    span(classes = buildIndicatorCss(activeTab)) {
                                        span(classes = "mdc-tab-indicator__content mdc-tab-indicator__content--underline") {

                                        }
                                    }
                                    span(classes = "mdc-tab__ripple") { }
                                }
                            }
                        }
                    }
                }
            }
        }
        val indexToActivate = props.activeIndex
        if (indexToActivate.isNotNull()) {
            jsComponent?.activateTab(indexToActivate)
            jsComponent?.scrollIntoView(indexToActivate)
        }
    }

    private fun buildIndicatorCss(activeTab: Boolean): String {
        var css = "mdc-tab-indicator"
        if (activeTab) {
            css += " mdc-tab-indicator--active"
        }
        return css
    }

    override fun componentDidMount() {
        jsComponent?.activateTab(0)
        jsComponent?.scrollIntoView(0)
    }

    private fun onTabActivation(index: Int) {
        props.description?.let { tabs ->
            try {
                tabs.tabs[index].activatedHandler?.invoke(index)
            } catch (e: Exception) {
                log.error { "No tab description for index $index" }
            }
        }
    }
}

@ExperimentalContracts
fun RBuilder.matTabBar(handler: RHandler<MatTabBarCompProps>) = child(MatTabBarComp::class, handler)