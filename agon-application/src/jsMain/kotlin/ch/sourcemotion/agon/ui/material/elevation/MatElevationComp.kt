package ch.sourcemotion.agon.ui.material.elevation

import ch.sourcemotion.agon.extension.isNull
import react.*
import react.dom.div
import kotlin.contracts.ExperimentalContracts

interface MatElevationCompProps : RProps {
    var level: Int?
    var cssClasses: String?
}

/**
 * Renders a visual elevation of its content.
 */
@ExperimentalContracts
class MatElevationComp : RComponent<MatElevationCompProps, RState>() {

    override fun RBuilder.render() {
        div(classes = buildElevationCss()) {
            props.children()
        }
    }

    private fun buildElevationCss(): String {
        val finalLevel = if (props.level.isNull()) {
            0
        } else {
            props.level
        }
        return "mdc-elevation-transition mdc-elevation--z$finalLevel ${props.cssClasses}"
    }
}


@ExperimentalContracts
fun RBuilder.matElevation(handler: RHandler<MatElevationCompProps>) = child(MatElevationComp::class, handler)