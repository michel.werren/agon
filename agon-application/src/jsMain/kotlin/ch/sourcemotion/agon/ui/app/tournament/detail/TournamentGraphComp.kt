package ch.sourcemotion.agon.ui.app.tournament.detail

import ch.sourcemotion.agon.ui.component.description.DescriptionCardEntry
import ch.sourcemotion.agon.ui.component.description.descriptionCard
import ch.sourcemotion.agon.ui.component.failurehandling.FailureHandlingComp
import ch.sourcemotion.agon.ui.component.failurehandling.FailureHandlingCompState
import ch.sourcemotion.agon.ui.material.icon.MatIcons
import ch.sourcemotion.agon.ui.material.icon.matIcon
import kotlinx.html.DIV
import react.RProps
import react.dom.RDOMBuilder
import react.dom.div
import kotlin.contracts.ExperimentalContracts


@ExperimentalContracts
abstract class TournamentGraphComp<P : RProps, S : FailureHandlingCompState> : FailureHandlingComp<P, S>() {
    protected fun RDOMBuilder<DIV>.renderGraphElement(elementLabel: String, elementText: String?) {
        descriptionCard {
            attrs {
                entries = listOf(DescriptionCardEntry(elementLabel, elementText))
            }
        }

        div(classes = "graph-connect-arrow") {
            matIcon {
                attrs.icon = MatIcons.NAVIGATION
            }
        }
    }
}