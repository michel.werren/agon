package ch.sourcemotion.agon.ui.app.tournament.detail.ranking

import ch.sourcemotion.agon.dto.GroupDto
import ch.sourcemotion.agon.dto.RankingItemDto
import ch.sourcemotion.agon.extension.isNeitherNullOrEmpty
import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.ui.component.labeledtext.labeledText
import ch.sourcemotion.agon.ui.extension.toStringOrNoValue
import ch.sourcemotion.agon.ui.i18n.I18n
import ch.sourcemotion.agon.ui.material.card.matCard
import ch.sourcemotion.agon.ui.material.dialog.DialogAction
import ch.sourcemotion.agon.ui.material.dialog.matDialog
import react.*
import react.dom.div
import react.dom.span
import kotlin.contracts.ExperimentalContracts

interface RankingDialogCompProps : RProps {
    var stageName: String?
    var rankings: List<RankingItemDto>?
    var groups: List<GroupDto>?
    var closedHandler: (() -> Unit)?
}

@ExperimentalContracts
class RankingDialogComp : RComponent<RankingDialogCompProps, RState>() {
    override fun RBuilder.render() {

        val groups = props.groups
        val rankings = props.rankings
        val stageName = props.stageName
        if (groups.isNotNull() && rankings.isNotNull() && stageName.isNeitherNullOrEmpty()) {
            matDialog {
                attrs {
                    show = true
                    title = I18n.default("Ranking", mapOf("stage_name" to stageName))
                    actions = listOf(DialogAction(I18n.default("Close")) {
                        props.closedHandler?.invoke()
                    })
                }

                div(classes = "ranking-dialog-comp") {
                    rankings.sortedBy { it.rank }.forEach { ranking ->

                        val group = groups.firstOrNull { it.id == ranking.groupId }

                        matCard {
                            attrs.noAction = true

                            div(classes = "ranking") {
                                div(classes = "ranking-head") {
                                    span(classes = "participant-head") {
                                        +(ranking.participant?.name ?: I18n.default("NoParticipantName"))
                                    }
                                    div(classes = "rank-head") {
                                       labeledText {
                                           attrs {
                                               label = I18n.default("Rank")
                                               text = ranking.rank.toStringOrNoValue()
                                           }
                                       }
                                    }
                                }
                                div(classes = "ranking-body") {
                                    labeledText {
                                        attrs {
                                            label = I18n.default("GroupName")
                                            text = group?.name.toStringOrNoValue()
                                            classes = "ranking-body-item"
                                        }
                                    }
                                    labeledText {
                                        attrs {
                                            label = I18n.default("RankingDetail.Points")
                                            text = ranking.points.toStringOrNoValue()
                                            classes = "ranking-body-item"
                                        }
                                    }
                                    labeledText {
                                        attrs {
                                            label = I18n.default("RankingDetail.Played")
                                            text = ranking.properties?.played.toStringOrNoValue()
                                            classes = "ranking-body-item"
                                        }
                                    }
                                    labeledText {
                                        attrs {
                                            label = I18n.default("RankingDetail.Wins")
                                            text = ranking.properties?.wins.toStringOrNoValue()
                                            classes = "ranking-body-item"
                                        }
                                    }
                                    labeledText {
                                        attrs {
                                            label = I18n.default("RankingDetail.Draws")
                                            text = ranking.properties?.draws.toStringOrNoValue()
                                            classes = "ranking-body-item"
                                        }
                                    }
                                    labeledText {
                                        attrs {
                                            label = I18n.default("RankingDetail.Looses")
                                            text = ranking.properties?.losses.toStringOrNoValue()
                                            classes = "ranking-body-item"
                                        }
                                    }
                                    labeledText {
                                        attrs {
                                            label = I18n.default("RankingDetail.ScoreFor")
                                            text = ranking.properties?.scoreFor.toStringOrNoValue()
                                            classes = "ranking-body-item"
                                        }
                                    }
                                    labeledText {
                                        attrs {
                                            label = I18n.default("RankingDetail.ScoreAgainst")
                                            text = ranking.properties?.scoreAgainst.toStringOrNoValue()
                                            classes = "ranking-body-item"
                                        }
                                    }
                                    labeledText {
                                        attrs {
                                            label = I18n.default("RankingDetail.ScoreDifference")
                                            text = ranking.properties?.scoreDifference.toStringOrNoValue()
                                            classes = "ranking-body-item"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@ExperimentalContracts
fun RBuilder.rankingDialog(handler: RHandler<RankingDialogCompProps>) = child(RankingDialogComp::class, handler)