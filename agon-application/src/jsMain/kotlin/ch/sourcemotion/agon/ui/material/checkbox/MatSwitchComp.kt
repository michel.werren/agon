package ch.sourcemotion.agon.ui.material.checkbox

import ch.sourcemotion.agon.extension.isNull
import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.ui.component.fixLabel
import ch.sourcemotion.agon.ui.material.MatBaseComponent
import ch.sourcemotion.agon.ui.util.UUIDJS
import ch.sourcemotion.agon.ui.util.asElementId
import kotlinx.html.InputType
import kotlinx.html.id
import kotlinx.html.js.onChangeFunction
import kotlinx.html.role
import react.RBuilder
import react.RHandler
import react.RProps
import react.RState
import react.dom.div
import react.dom.input
import kotlin.contracts.ExperimentalContracts

interface MatSwitchCompProps : RProps {
    var label: String?
    var checked: Boolean?
    var disabled: Boolean?
    var changeHandler: ((Boolean) -> Unit)?
}

/**
 * Switch component
 */
@ExperimentalContracts
class MatSwitchComp : MatBaseComponent<MatSwitchCompProps, RState, MDCSwitch>() {

    private val switchId = UUIDJS.genV4().asElementId()

    override fun RBuilder.render() {

        div(classes = buildRootCss()) {
            ref {
                if (jsComponent.isNull() && it != null) {
                    jsComponent = MDCSwitch(it).apply {
                        applyProps(props)
                    }
                }
            }
            div(classes = "mdc-switch__track") {}
            div(classes = "mdc-switch__thumb-underlay") {
                div(classes = "mdc-switch__thumb") {
                    input(classes = "mdc-switch__native-control", type = InputType.checkBox) {
                        attrs {
                            id = switchId
                            role = "switch"
                            disabled = isDisabled()
                            props.changeHandler?.let { handler ->
                                onChangeFunction = {
                                    jsComponent?.let { switch ->
                                        handler(switch.checked)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        fixLabel {
            attrs {
                htmlFor = switchId
            }
            props.label?.let { +it }
        }
    }

    override fun componentWillUpdate(nextProps: MatSwitchCompProps, nextState: RState) {
        jsComponent?.applyProps(nextProps)
    }

    private fun MDCSwitch.applyProps(switchCompProps: MatSwitchCompProps) {
        disabled = switchCompProps.disabled.isTrue()
        checked = switchCompProps.checked.isTrue()
    }

    private fun buildRootCss(): String {
        var css = "mdc-switch"
        if (isDisabled()) {
            css += " mdc-switch--disabled"
        }
        return css
    }

    private fun isDisabled() = props.disabled.isTrue()
}

@ExperimentalContracts
fun RBuilder.matSwitch(handler: RHandler<MatSwitchCompProps>) = child(MatSwitchComp::class, handler)