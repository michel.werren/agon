package ch.sourcemotion.agon.ui.material.button

import ch.sourcemotion.agon.extension.isNeitherNullOrEmpty
import ch.sourcemotion.agon.extension.isNull
import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.ui.material.MatBaseComponent
import ch.sourcemotion.agon.ui.material.icon.MatIcon
import kotlinx.html.js.onClickFunction
import react.RBuilder
import react.RHandler
import react.RProps
import react.RState
import react.dom.button
import react.dom.span
import kotlin.contracts.ExperimentalContracts

interface MatFabProps : RProps {
    var label: String?
    var icon: MatIcon?
    var mini: Boolean
    var primaryColor: Boolean?
    var classes: String?
    var clickHandler: (() -> Unit)?
}

/**
 * Material fab button. It's by design not possible to disable. Suggested is to hide it in this case.
 */
@ExperimentalContracts
class MatFabComp : MatBaseComponent<MatFabProps, RState, MDCRipple>() {

    override fun RBuilder.render() {
        if (props.icon.isNull()) {
            throw IllegalStateException("Fab button needs an icon")
        }

        val icon = props.icon!!

        button(classes = "${buildRootCss()} ${props.classes}") {
            ref {
                if (jsComponent.isNull() && it != null) {
                    jsComponent = MDCRipple(it)
                }
            }

            attrs {
                props.clickHandler?.let {
                    onClickFunction = {
                        it()
                    }
                }
            }

            span(classes = "mdc-fab__icon material-icons") {
                +icon.getIconName()
            }

            if (isExtended()) {
                renderExtended()
            } else {
                attrs {
                    attributes["aria-label"] = icon.getIconName()
                }
            }
        }
    }

    private fun RBuilder.renderExtended() {
        span(classes = "mdc-fab__label") {
            props.label?.let { +it }
        }
    }

    private fun buildRootCss(): String {
        var css = "mdc-fab"
        if (isMini()) {
            css += " mdc-fab--mini"
        }
        if (isExtended()) {
            css += " mdc-fab--extended"
        }
        if (isPrimary()) {
            css += " primary-fab"
        }
        return css
    }

    private fun isMini() = props.mini.isTrue()

    private fun isPrimary() = props.primaryColor.isTrue()

    private fun isExtended() = props.label.isNeitherNullOrEmpty()
}

@ExperimentalContracts
fun RBuilder.matFab(handler: RHandler<MatFabProps>) = child(MatFabComp::class, handler)