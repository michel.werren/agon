package ch.sourcemotion.agon.ui.component.description

import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.extension.isNull
import ch.sourcemotion.agon.ui.component.labeledtext.labeledText
import ch.sourcemotion.agon.ui.material.card.matCard
import kotlinx.html.js.onClickFunction
import react.*
import react.dom.div
import kotlin.contracts.ExperimentalContracts

data class DescriptionCardEntry(val label: String, val text: String?)

interface DescriptionCardCompProps : RProps {
    var entries: List<DescriptionCardEntry>?
    var clickHandler: (() -> Unit)?
}


@ExperimentalContracts
class DescriptionCardComp : RComponent<DescriptionCardCompProps, RState>(){
    override fun RBuilder.render() {
        props.entries?.let { entries ->
            matCard {
                val clickHandler = props.clickHandler
                if (clickHandler.isNull()) {
                    attrs.noAction = true
                }

                div(classes = "description-card-comp") {
                    if (clickHandler.isNotNull()) {
                        attrs.onClickFunction = {
                            clickHandler()
                        }
                    }
                    entries.forEach { entry ->
                        labeledText {
                            attrs {
                                label = entry.label
                                text = entry.text
                            }
                        }
                    }
                }
            }
        }
    }
}

@ExperimentalContracts
fun RBuilder.descriptionCard(handler: RHandler<DescriptionCardCompProps>) = child(DescriptionCardComp::class, handler)