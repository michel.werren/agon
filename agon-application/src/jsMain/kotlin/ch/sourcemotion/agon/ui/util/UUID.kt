package ch.sourcemotion.agon.ui.util

/**
 * Definitions for uuidjs functionality
 */
@JsModule("uuidjs")
external class UUIDJS {
    companion object {
        fun genV4(): UUIDJS
        fun parse(uuid: String): UUIDJS
    }

    var hexString: String
    var hexNoDelim: String
    var version: Int
}