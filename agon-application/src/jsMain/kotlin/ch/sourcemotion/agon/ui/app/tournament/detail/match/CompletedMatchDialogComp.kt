package ch.sourcemotion.agon.ui.app.tournament.detail.match

import ch.sourcemotion.agon.dto.MatchDto
import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.extension.isNull
import ch.sourcemotion.agon.ui.dto.toMomentUtc
import ch.sourcemotion.agon.ui.extension.toStringOrNoValue
import ch.sourcemotion.agon.ui.i18n.I18n
import ch.sourcemotion.agon.ui.material.dialog.DialogAction
import ch.sourcemotion.agon.ui.material.dialog.matDialog
import react.*
import react.dom.div
import kotlin.contracts.ExperimentalContracts

interface CompletedMatchDialogCompProps : RProps {
    var stageName: String?
    var timezone: String?
    var matches: List<MatchDto>?
    var closedHandler: (() -> Unit)?
}

@ExperimentalContracts
class CompletedMatchDialogComp : RComponent<CompletedMatchDialogCompProps, RState>() {
    override fun RBuilder.render() {
        val matches = props.matches
        if (matches.isNotNull()) {
            val sortedMatches = matches.sortedWith(Comparator { a, b ->
                val aMoment = a.playedAt.toMomentUtc()
                val bMoment = b.playedAt.toMomentUtc()
                when {
                    aMoment.isNull() && bMoment.isNull() -> 0
                    aMoment.isNull() -> -1
                    bMoment.isNull() -> 1
                    aMoment!!.isSame(bMoment!!) -> 0
                    aMoment.isBefore(bMoment) -> -1
                    aMoment.isAfter(bMoment) -> 1
                    else -> 0
                }
            })

            matDialog {
                attrs {
                    show = true
                    title = "${I18n.default("CompletedMatches")}: ${props.stageName.toStringOrNoValue()}"
                    actions = listOf(DialogAction(I18n.default("Close")) {
                        props.closedHandler?.invoke()
                    })
                }
                div(classes = "completed-match-dialog") {
                    sortedMatches.forEach { match ->
                        matchCard {
                            attrs {
                                timezone = props.timezone
                                this.match = match
                            }
                        }
                    }
                }
            }
        }
    }
}

@ExperimentalContracts
fun RBuilder.completedMatches(handler: RHandler<CompletedMatchDialogCompProps>) = child(CompletedMatchDialogComp::class, handler)