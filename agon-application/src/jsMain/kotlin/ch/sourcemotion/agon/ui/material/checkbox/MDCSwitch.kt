@file:JsModule("@material/switch")

package ch.sourcemotion.agon.ui.material.checkbox

import ch.sourcemotion.agon.ui.material.MDCBase

@JsName("MDCSwitch")
external class MDCSwitch(element: dynamic) : MDCBase {
    var checked: Boolean
    var disabled: Boolean
}