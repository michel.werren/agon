package ch.sourcemotion.agon.ui.app.tournament.detail

import ch.sourcemotion.agon.dto.ParticipantDto
import ch.sourcemotion.agon.dto.StageDto
import ch.sourcemotion.agon.dto.TournamentDetailDto
import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.extension.isNull
import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.ui.app.tournament.detail.stage.stages
import ch.sourcemotion.agon.ui.component.failurehandling.FailureHandlingComp
import ch.sourcemotion.agon.ui.component.failurehandling.FailureHandlingCompState
import ch.sourcemotion.agon.ui.component.labeledtext.labeledText
import ch.sourcemotion.agon.ui.component.titled.titled
import ch.sourcemotion.agon.ui.dto.toMoment
import ch.sourcemotion.agon.ui.extension.changeState
import ch.sourcemotion.agon.ui.extension.getPathParam
import ch.sourcemotion.agon.ui.extension.getReactHistory
import ch.sourcemotion.agon.ui.i18n.I18n
import ch.sourcemotion.agon.ui.material.MAT_CELL_DIMENSION
import ch.sourcemotion.agon.ui.material.card.matCard
import ch.sourcemotion.agon.ui.material.loading.matLoading
import ch.sourcemotion.agon.ui.material.loading.matLoadingModal
import ch.sourcemotion.agon.ui.material.tabbar.TabsDescription.Companion.tabs
import ch.sourcemotion.agon.ui.material.tabbar.matTabBar
import ch.sourcemotion.agon.ui.moment.LONG_DATE_ONLY_FORMAT
import ch.sourcemotion.agon.ui.request.ParticipantsRequest
import ch.sourcemotion.agon.ui.request.StagesRequest
import ch.sourcemotion.agon.ui.request.TournamentDetailRequest
import ch.sourcemotion.agon.ui.rest.RestClient
import ch.sourcemotion.agon.ui.rest.RestClientException
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.css.GridTemplateRows
import kotlinx.html.DIV
import mu.KotlinLogging
import react.RBuilder
import react.RProps
import react.dom.RDOMBuilder
import react.dom.div
import react.dom.img
import styled.css
import styled.styledDiv
import kotlin.contracts.ExperimentalContracts

private const val DETAIL_ICON_ROW_DIMENSION = MAT_CELL_DIMENSION * 8
private const val DETAIL_ROW_HEIGHT = MAT_CELL_DIMENSION * 3

enum class TournamentDetailTab(val index: Int) {
    DETAIL(0),
    PARTICIPANTS(1),
    RESULTS(2)
}

interface TournamentDetailCompState : FailureHandlingCompState {
    var activeTab: TournamentDetailTab
    var tournamentDetail: TournamentDetailDto?
    var participants: List<ParticipantDto>?
    var stages: List<StageDto>?
    var clearFilter: Boolean?
}

@ExperimentalContracts
class TournamentDetailComp : FailureHandlingComp<RProps, TournamentDetailCompState>() {

    companion object {
        private val log = KotlinLogging.logger("TournamentDetailComp")
    }

    init {
        state.activeTab = TournamentDetailTab.DETAIL
    }

    override fun RBuilder.render() {
        renderFailureDialog()
        val tournamentDetail = state.tournamentDetail
        if (tournamentDetail.isNotNull()) {
            titled {
                attrs {
                    title = tournamentDetail.name
                    history = getReactHistory()
                }

                div(classes = "tournament-detail") {
                    div(classes = "tournament-detail-tabs") {
                        matTabBar {
                            attrs {
                                description = tabs {
                                    tab {
                                        label = I18n.default("Detail")
                                        activatedHandler = {
                                            log.debug { "Switch to Detail tab" }
                                            switchToDetailTab()
                                        }
                                    }
                                    tab {
                                        label = I18n.default("Participants")
                                        activatedHandler = {
                                            log.debug { "Switch to Participants tab" }
                                            switchToParticipantsTab()
                                        }
                                    }
                                    tab {
                                        label = I18n.default("Results")
                                        activatedHandler = {
                                            log.debug { "Switch to Results tab" }
                                            switchToResultsTab()
                                        }
                                    }
                                }
                                activeIndex = state.activeTab.index
                            }
                        }
                    }
                    when (state.activeTab) {
                        TournamentDetailTab.DETAIL -> {
                            renderDetailTab(tournamentDetail)
                        }
                        TournamentDetailTab.PARTICIPANTS -> {
                            val participants = state.participants
                            if (participants.isNotNull()) {
                                if (participants.isNotEmpty()) {
                                    renderParticipantsTab(participants)
                                } else {
                                    renderNoData()
                                }
                            } else {
                                renderLocalLoadingIndicator(I18n.default("LoadMessage.LoadParticipants"))
                            }
                        }
                        TournamentDetailTab.RESULTS -> {
                            val stages = state.stages
                            if (stages.isNotNull()) {
                                if (stages.isNotEmpty()) {
                                    renderStagesTab(tournamentDetail, stages)
                                } else {
                                    renderNoData()
                                }
                            } else {
                                renderLocalLoadingIndicator(I18n.default("LoadMessage.LoadStages"))
                            }
                        }
                    }
                }
            }
        } else {
            matLoadingModal {
                attrs {
                    show = state.failureMessage.isNullOrBlank()
                    text = I18n.default("LoadMessage.LoadTournament")
                    indeterminate = true
                }
            }
        }
    }

    private fun RBuilder.renderNoData() {
        div(classes = "no-data") {
            +I18n.default("NoData")
        }
    }


    private fun RBuilder.renderDetailTab(tournamentDetail: TournamentDetailDto) {
        styledDiv {
            css {
                classes.add("tournament-detail-detail")
                classes.add("tournament-detail-tab")

                var rowTemplates = ""
                tournamentDetail.logo?.let {
                    rowTemplates += "${DETAIL_ICON_ROW_DIMENSION}px"
                }
                repeat(11) {
                    rowTemplates += " ${DETAIL_ROW_HEIGHT}px"
                }

                gridTemplateRows = GridTemplateRows(rowTemplates)
            }

            tournamentDetail.logo?.let { logo ->
                img(classes = "tournament-detail-detail-logo", src = logo.logoSmall ?: logo.logoMedium) {}
            }

            labeledText {
                attrs {
                    label = I18n.default("FullName")
                    text = tournamentDetail.fullName ?: I18n.default("NoValue")
                    classes = "tournament-detail-detail-entry"
                }
            }
            labeledText {
                attrs {
                    label = I18n.default("Organizer")
                    text = tournamentDetail.organization ?: I18n.default("NoValue")
                    classes = "tournament-detail-detail-entry"
                }
            }
            labeledText {
                attrs {
                    label = I18n.default("Location")
                    text = tournamentDetail.location ?: I18n.default("NoValue")
                    classes = "tournament-detail-detail-entry"
                }
            }
            labeledText {
                attrs {
                    label = I18n.default("Discipline")
                    text = I18n.default("Platforms.${tournamentDetail.discipline}")
                    classes = "tournament-detail-detail-entry"
                }
            }
            labeledText {
                attrs {
                    label = I18n.default("ParticipantType")
                    text = I18n.default("ParticipantTypes.${tournamentDetail.participantType}")
                    classes = "tournament-detail-detail-entry"
                }
            }
            labeledText {
                attrs {
                    label = I18n.default("StartDate")
                    text = tournamentDetail.scheduledDateStart.toMoment()?.format(LONG_DATE_ONLY_FORMAT) ?:
                            I18n.default("NoValue")
                    classes = "tournament-detail-detail-entry"
                }
            }
            labeledText {
                attrs {
                    label = I18n.default("EndDate")
                    text = tournamentDetail.scheduledDateEnd.toMoment()?.format(LONG_DATE_ONLY_FORMAT) ?:
                            I18n.default("NoValue")
                    classes = "tournament-detail-detail-entry"
                }
            }
            labeledText {
                attrs {
                    label = I18n.default("WebSite")
                    text = tournamentDetail.website
                    classes = "tournament-detail-detail-entry"
                    url = true
                }
            }
            labeledText {
                attrs {
                    label = I18n.default("Platform")
                    text = tournamentDetail.platforms?.joinToString(",") { I18n.default("Platforms.$it") } ?:
                            I18n.default("NoValue")
                    classes = "tournament-detail-detail-entry"
                }
            }
            labeledText {
                attrs {
                    label = I18n.default("MaxParticipantCount")
                    text = tournamentDetail.size.toString()
                    classes = "tournament-detail-detail-entry"
                }
            }
            labeledText {
                attrs {
                    label = I18n.default("Status")
                    text = tournamentDetail.status?.let { state -> I18n.default("StatusType.$state") } ?:
                            I18n.default("NoValue")
                    classes = "tournament-detail-detail-entry"
                }
            }
        }
    }


    private fun RBuilder.renderParticipantsTab(participants: List<ParticipantDto>) {
        div(classes = "tournament-detail-participant tournament-detail-tab") {
            participants.forEach { participant ->
                matCard {
                    attrs {
                        noAction = true
                    }
                    labeledText {
                        attrs {
                            label = I18n.default("Name")
                            text = participant.name
                            classes = "tournament-participant"
                        }
                    }
                }
            }
        }
    }

    private fun RBuilder.renderStagesTab(tournamentDetail: TournamentDetailDto, stages: List<StageDto>) {
        stages {
            attrs {
                tournament = tournamentDetail
                this.stages = stages
                history = props.asDynamic().history
            }
        }
    }

    private fun RDOMBuilder<DIV>.renderLocalLoadingIndicator(text: String) {
        div(classes = "tournament-detail-loading-placeholder") {
            matLoading {
                attrs {
                    this.text = text
                    indeterminate = true
                }
            }
        }
    }

    override fun componentDidUpdate(prevProps: RProps, prevState: TournamentDetailCompState, snapshot: Any) {
        if (state.clearFilter.isTrue()) {
            changeState { clearFilter = false }
        }
    }

    override fun componentDidMount() {
        GlobalScope.launch {
            val tournamentDetail = loadTournamentDetail()
            changeState {
                this.tournamentDetail = tournamentDetail
            }
        }
    }

    /**
     * Switches to tournament detail tab and reload data
     */
    private fun switchToDetailTab() {
        if (state.activeTab != TournamentDetailTab.DETAIL) {
            changeState {
                activeTab = TournamentDetailTab.DETAIL
            }
            if (state.tournamentDetail.isNull()) {
                GlobalScope.launch {
                    val tournamentDetail = loadTournamentDetail()
                    changeState {
                        this.tournamentDetail = tournamentDetail
                    }
                }
            }
        }
    }


    /**
     * Switches to tournament detail tab and reload data
     */
    private fun switchToParticipantsTab() {
        if (state.activeTab != TournamentDetailTab.PARTICIPANTS) {
            changeState {
                activeTab = TournamentDetailTab.PARTICIPANTS
            }
            if (state.participants.isNullOrEmpty()) {
                getPathParam("tournamentId")?.let { tournamentId ->
                    GlobalScope.launch {
                        try {
                            val participants = RestClient.get(ParticipantsRequest(tournamentId))
                            changeState {
                                this.participants = participants
                            }
                        } catch (e: RestClientException) {
                            openFailureDialog(I18n.failure("Participants", e.failure))
                        }
                    }
                }
            }
        }
    }

    /**
     * Switches to stages tag an load stages when they not already was loaded
     */
    private fun switchToResultsTab() {
        if (state.activeTab != TournamentDetailTab.RESULTS) {
            changeState {
                activeTab = TournamentDetailTab.RESULTS
            }
            if (state.stages.isNullOrEmpty()) {
                getPathParam("tournamentId")?.let { tournamentId ->
                    GlobalScope.launch {
                        try {
                            val stages = RestClient.get(StagesRequest(tournamentId))
                            changeState {
                                this.stages = stages
                                activeTab = TournamentDetailTab.RESULTS
                            }
                        } catch (e: RestClientException) {
                            openFailureDialog(I18n.failure("Stages", e.failure))
                        }
                    }
                }
            }
        }
    }

    private suspend fun loadTournamentDetail(): TournamentDetailDto? {
        return getPathParam("tournamentId")?.let { id ->
            return try {
                RestClient.get(TournamentDetailRequest(id))
            } catch (e: RestClientException) {
                openFailureDialog(I18n.failure("Tournament", e.failure))
                null
            }
        }
    }
}