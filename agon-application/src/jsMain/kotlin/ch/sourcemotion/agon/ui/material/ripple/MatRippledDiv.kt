package ch.sourcemotion.agon.ui.material.ripple

import ch.sourcemotion.agon.extension.isNull
import ch.sourcemotion.agon.extension.or
import ch.sourcemotion.agon.ui.material.MatBaseComponent
import ch.sourcemotion.agon.ui.material.button.MDCRipple
import ch.sourcemotion.agon.ui.util.UUIDJS
import ch.sourcemotion.agon.ui.util.asElementId
import kotlinx.css.LinearDimension
import kotlinx.html.id
import react.RBuilder
import react.RHandler
import react.RProps
import react.RState
import styled.css
import styled.styledDiv
import kotlin.browser.window
import kotlin.contracts.ExperimentalContracts

interface MatRippledDivProps : RProps {
    var classes: String?
    var height: String?
    var width: String?
}

@ExperimentalContracts
class MatRippledDiv : MatBaseComponent<MatRippledDivProps, RState, MDCRipple>() {

    private val divId = UUIDJS.genV4().asElementId()

    override fun RBuilder.render() {
        styledDiv {
            attrs {
                id = divId
            }
            css {
                props.classes?.let {
                    classes.add(props.classes.or(""))
                }
                props.width?.let {
                    width = LinearDimension(it)
                }
                props.height?.let {
                    height = LinearDimension(it)
                }
            }
            ref {
                if (jsComponent.isNull()) {
                    jsComponent = MDCRipple(window.document.querySelector("#$divId"))
                }
            }
            props.children()
        }
    }
}

@ExperimentalContracts
fun RBuilder.matRippledDiv(handler: RHandler<MatRippledDivProps>) = child(MatRippledDiv::class, handler)