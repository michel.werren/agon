@file:JsModule("@material/dialog")

package ch.sourcemotion.agon.ui.material.dialog

import ch.sourcemotion.agon.ui.material.MDCBase

external interface DialogEvent {
    var detail: DialogEventDetail
}

external interface DialogEventDetail {
    var action: String
}



@JsName("MDCDialog")
external class MDCDialog(element: dynamic) : MDCBase {
    fun listen(event: String, handler: (event: DialogEvent) -> Unit)
    fun open()
    val isOpen : Boolean
}