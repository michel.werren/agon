package ch.sourcemotion.agon.ui.material

/**
 * Base for all material component javascript components
 */
abstract external class MDCBase {
    fun destroy()
}