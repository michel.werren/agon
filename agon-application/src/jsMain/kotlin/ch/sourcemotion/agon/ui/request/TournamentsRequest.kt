package ch.sourcemotion.agon.ui.request

import ch.sourcemotion.agon.dto.TournamentDto
import ch.sourcemotion.agon.TournamentPath
import ch.sourcemotion.agon.ui.rest.RestGetRequest
import kotlinx.serialization.list

/**
 * Request to get all tournaments
 */
class TournamentsRequest : RestGetRequest<List<TournamentDto>>(TournamentPath.ROOT_PATH, TournamentDto.serializer().list)