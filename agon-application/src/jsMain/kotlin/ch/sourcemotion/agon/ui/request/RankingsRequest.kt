package ch.sourcemotion.agon.ui.request

import ch.sourcemotion.agon.dto.RankingItemDto
import ch.sourcemotion.agon.TournamentPath
import ch.sourcemotion.agon.ui.rest.RestGetRequest
import kotlinx.serialization.list


class RankingsRequest(tournamentId: String, stageId: String) : RestGetRequest<List<RankingItemDto>>("${TournamentPath.ROOT_PATH}/$tournamentId/stages/$stageId/ranking-items", RankingItemDto.serializer().list)