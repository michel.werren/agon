package ch.sourcemotion.agon.ui.material.datetimepicker

import org.w3c.dom.HTMLInputElement
import kotlin.js.Date


val flatpickrRef = kotlinext.js.require("flatpickr")
val german = kotlinext.js.require("flatpickr/dist/l10n/de.js").default.de

fun globalFlatPickrConfiguration() {
    flatpickrRef.localize(german)
    flatpickrRef.l10ns.default.firstDayOfWeek = 1
}

external interface Flatpickr {
    fun destroy()
    fun clear()
    fun close()
    fun redraw()
    val input: HTMLInputElement
    var config: dynamic
    fun set(property: String, value: dynamic)
    fun setDate(date: Date, fireEvent: Boolean, format: String? = definedExternally)
}