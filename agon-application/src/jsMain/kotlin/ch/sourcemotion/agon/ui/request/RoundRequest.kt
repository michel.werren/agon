package ch.sourcemotion.agon.ui.request

import ch.sourcemotion.agon.dto.RoundDto
import ch.sourcemotion.agon.TournamentPath
import ch.sourcemotion.agon.ui.rest.RestGetRequest


class RoundRequest(tournamentId: String, roundId: String) : RestGetRequest<RoundDto>("${TournamentPath.ROOT_PATH}/$tournamentId/rounds/$roundId", RoundDto.serializer())