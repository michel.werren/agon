package ch.sourcemotion.agon.ui.material.button

import ch.sourcemotion.agon.extension.isNeitherNullOrEmpty
import ch.sourcemotion.agon.extension.isNull
import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.ui.material.MatBaseComponent
import ch.sourcemotion.agon.ui.material.icon.MatIcon
import kotlinx.html.js.onClickFunction
import react.RBuilder
import react.RHandler
import react.RProps
import react.RState
import react.dom.button
import kotlin.contracts.ExperimentalContracts

interface MatIconButtonProps : RProps {
    var icon: MatIcon?
    var disabled: Boolean?
    var classes: String?
    var clickHandler: (() -> Unit)?
}

@ExperimentalContracts
class MatIconButtonComp : MatBaseComponent<MatIconButtonProps, RState, MDCRipple>() {

    override fun RBuilder.render() {
        button(classes = buildRootCss()) {
            ref {
                if (jsComponent.isNull() && it != null) {
                    val mdcRipple = MDCRipple(it)
                    mdcRipple.unbounded = true
                    jsComponent = mdcRipple
                }
            }
            props.icon?.let { +it.getIconName() }
            attrs {
                disabled = props.disabled.isTrue()
                props.clickHandler?.let {
                    onClickFunction = {
                        it()
                    }
                }
            }
        }
    }

    private fun buildRootCss(): String {
        var css = "mdc-icon-button material-icons"
        if (props.classes.isNeitherNullOrEmpty()) {
            css += " ${props.classes}"
        }
        return css
    }
}

@ExperimentalContracts
fun RBuilder.matIconButton(handler: RHandler<MatIconButtonProps>) = child(MatIconButtonComp::class, handler)