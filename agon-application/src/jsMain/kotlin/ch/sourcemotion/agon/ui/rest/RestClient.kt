package ch.sourcemotion.agon.ui.rest

import ch.sourcemotion.agon.AgonHeader
import ch.sourcemotion.agon.extension.isNeitherNullOrEmpty
import kotlinx.coroutines.await
import kotlinx.serialization.json.JSON
import mu.KotlinLogging
import org.w3c.fetch.Headers
import org.w3c.fetch.NO_CACHE
import org.w3c.fetch.RequestCache
import org.w3c.fetch.RequestInit
import kotlin.browser.window
import kotlin.contracts.ExperimentalContracts

external var serverPort: String?


@ExperimentalContracts
object RestClient {

    @PublishedApi
    internal val log = KotlinLogging.logger("RestClient")

    @PublishedApi
    internal val baseUrl = buildBaseUrl()

    init {
        log.info { "Rest http client created. Will connect to $baseUrl" }
    }

    private fun buildBaseUrl(): String {
        var url = "${window.location.protocol}//${window.location.hostname}"

        if (serverPort.isNeitherNullOrEmpty()) {
            url += ":$serverPort"
        }
        return url
    }


    @Suppress("UNCHECKED_CAST")
    suspend inline fun <reified T : Any> get(request: RestGetRequest<T>): T {
        val headers = Headers()
        headers.append(AgonHeader.AGON_REST_CLIENT_NAME, AgonHeader.AGON_REST_CLIENT_VALUE)
        val url = "$baseUrl${request.toQueryString()}"

        // Do request
        val response = window.fetch(url, object : RequestInit {
            override var method: String? = "GET"
            override var headers = headers
            override var cache: RequestCache? = RequestCache.NO_CACHE
        }).await()

        // Process response
        if (response.ok) {
            return JSON.nonstrict.parse(request.responseSerializer, response.text().await())
        } else {
            log.error { "Request $url to backend has failed with status ${response.status} / ${response.statusText}" }
            val failure = when(response.status) {
                404.toShort() -> RestClientFailure.NOT_FOUND
                else -> RestClientFailure.SERVER_ERROR
            }
            throw RestClientException(failure)
        }
    }
}