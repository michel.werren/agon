package ch.sourcemotion.agon.ui.request

import ch.sourcemotion.agon.dto.MatchDto
import ch.sourcemotion.agon.TournamentPath
import ch.sourcemotion.agon.ui.rest.RestGetRequest
import kotlinx.serialization.list


class MatchesRequest private constructor(tournamentId: String, filter: Map<String, Any>)
    : RestGetRequest<List<MatchDto>>("${TournamentPath.ROOT_PATH}/$tournamentId/matches", MatchDto.serializer().list, filter) {
    companion object {
        fun forRound(tournamentId: String, roundId: String) : MatchesRequest {
            return MatchesRequest(tournamentId, mapOf("round_ids" to listOf(roundId).joinToString(",")))
        }

        fun forStage(tournamentId: String, stageId: String, onlyCompleted: Boolean) : MatchesRequest {
            val queryParams = mutableMapOf<String, Any>("stage_ids" to listOf(stageId).joinToString(","))
            if (onlyCompleted) {
                queryParams["statuses"] = "completed"
            }
            return MatchesRequest(tournamentId, queryParams)
        }
    }
}