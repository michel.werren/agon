package ch.sourcemotion.agon.ui.app.tournament.detail.stage

import ch.sourcemotion.agon.dto.*
import ch.sourcemotion.agon.extension.isFalse
import ch.sourcemotion.agon.extension.isNeitherNullOrEmpty
import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.ui.app.tournament.detail.match.completedMatches
import ch.sourcemotion.agon.ui.app.tournament.detail.ranking.rankingDialog
import ch.sourcemotion.agon.ui.component.failurehandling.FailureHandlingComp
import ch.sourcemotion.agon.ui.component.failurehandling.FailureHandlingCompState
import ch.sourcemotion.agon.ui.component.labeledtext.labeledText
import ch.sourcemotion.agon.ui.extension.changeState
import ch.sourcemotion.agon.ui.extension.toStringOrNoValue
import ch.sourcemotion.agon.ui.i18n.I18n
import ch.sourcemotion.agon.ui.material.card.MatCardTextButtons.Companion.textButtons
import ch.sourcemotion.agon.ui.material.card.matCard
import ch.sourcemotion.agon.ui.material.loading.matLoadingModal
import ch.sourcemotion.agon.ui.request.GroupsRequest
import ch.sourcemotion.agon.ui.request.MatchesRequest
import ch.sourcemotion.agon.ui.request.RankingsRequest
import ch.sourcemotion.agon.ui.rest.RestClient
import ch.sourcemotion.agon.ui.rest.RestClientException
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.html.js.onClickFunction
import mu.KotlinLogging
import react.RBuilder
import react.RHandler
import react.RProps
import react.dom.div
import styled.css
import styled.styledDiv
import kotlin.contracts.ExperimentalContracts

data class RankingInfo(val stageName: String, val rankings: List<RankingItemDto>, val groups: List<GroupDto>)

data class CompletedMatchesInfo(val stageName: String, val matches: List<MatchDto>)

interface StagesCompProps : RProps {
    var tournament: TournamentDetailDto?
    var stages: List<StageDto>?
    var history: dynamic
}

interface StagesCompState : FailureHandlingCompState {
    var rankingInfo: RankingInfo?
    var completedMatchesInfo: CompletedMatchesInfo?
    var showLoadingModal: Boolean?
}

@ExperimentalContracts
class StagesComp : FailureHandlingComp<StagesCompProps, StagesCompState>() {

    companion object {
        private val log = KotlinLogging.logger("StagesComp")
    }

    override fun RBuilder.render() {
        renderFailureDialog()
        val tournament = props.tournament
        val stages = props.stages

        if (tournament.isNotNull() && stages.isNotNull()) {
            val tournamentId = tournament.id
            styledDiv {
                css {
                    classes.add("stages-comp")
                }

                stages.forEach { stage ->
                    matCard {
                        attrs.textButtons = textButtons {
                            button {
                                label = I18n.default("RankingOrMatchOverview")
                                handler = {
                                    changeState {
                                        showLoadingModal = true
                                        completedMatchesInfo = null
                                        rankingInfo = null
                                    }

                                    GlobalScope.launch {
                                        val rankingsPresent = loadAndShowRankings(tournamentId, stage.id, stage.name
                                                ?: I18n.noValue())
                                        if (rankingsPresent.isFalse()) {
                                            log.info { "No ranking information available, so try to load finished matches" }
                                            loadAndShowCompletedMatches(tournamentId, stage.id, stage.name
                                                    ?: I18n.noValue())
                                        }
                                    }
                                }
                            }
                        }

                        div(classes = "stage") {
                            attrs {
                                onClickFunction = {
                                    if (props.history != null) {
                                        props.history.push("/stages/$tournamentId/${stage.id}")
                                    }
                                }
                            }

                            div(classes = "stage-title") {
                                +"${I18n.default("Stage")}: ${stage.name.toStringOrNoValue()}"
                            }

                            div(classes = "stage-description") {
                                labeledText {
                                    attrs {
                                        label = I18n.default("StagesType")
                                        text = stage.type?.let { I18n.default("StagesTypes.${stage.type}") } ?: I18n.default("NoValue")
                                    }
                                }
                                labeledText {
                                    attrs {
                                        label = I18n.default("StagesNumber")
                                        text = stage.number.toString()
                                    }
                                }
                                labeledText {
                                    attrs {
                                        label = I18n.default("Closed")
                                        text = if (stage.closed.isTrue()) {
                                            I18n.default("Yes")
                                        } else {
                                            I18n.default("No")
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Render corresponding dialog when stage details should be shown
            val rankingInfo = state.rankingInfo
            val completedMatchesInfo = state.completedMatchesInfo

            if (rankingInfo.isNotNull()) {
                rankingDialog {
                    attrs {
                        rankings = rankingInfo.rankings
                        groups = rankingInfo.groups
                        stageName = rankingInfo.stageName
                        closedHandler = {
                            changeState {
                                this.rankingInfo = null
                            }
                        }
                    }
                }
            } else if (completedMatchesInfo.isNotNull()) {
                completedMatches {
                    attrs {
                        matches = completedMatchesInfo.matches
                        stageName = completedMatchesInfo.stageName
                        timezone = tournament.timezone
                    }
                }
            }

            matLoadingModal {
                attrs {
                    show = state.showLoadingModal.isTrue() && state.failureMessage.isNullOrBlank()
                    text = I18n.default("LoadMessage.LoadResults")
                    indeterminate = true
                }
            }
        }
    }


    private suspend fun loadAndShowRankings(tournamentId: String, stageId: String, stageName: String): Boolean {
        val rankings = try {
            RestClient.get(RankingsRequest(tournamentId, stageId))
        } catch (e: RestClientException) {
            openFailureDialog(I18n.failure("Groups", e.failure))
            null
        }

        if (rankings.isNeitherNullOrEmpty()) {
            return try {
                val groups = RestClient.get(GroupsRequest(tournamentId, stageId))
                changeState {
                    rankingInfo = RankingInfo(stageName, rankings, groups)
                    showLoadingModal = false
                }
                true
            } catch (e: RestClientException) {
                openFailureDialog(I18n.failure("Groups", e.failure))
                false
            }
        }
        return false
    }

    private suspend fun loadAndShowCompletedMatches(tournamentId: String, stageId: String, stageName: String) {
        try {
            val completedMatches = RestClient.get(MatchesRequest.forStage(tournamentId, stageId, true))
            changeState {
                this.completedMatchesInfo = CompletedMatchesInfo(stageName, completedMatches)
                showLoadingModal = false
            }
        } catch (e: RestClientException) {
            openFailureDialog(I18n.failure("Matches", e.failure))
        }
    }
}

@ExperimentalContracts
fun RBuilder.stages(handler: RHandler<StagesCompProps>) = child(StagesComp::class, handler)