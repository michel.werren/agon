package ch.sourcemotion.agon.ui.request

import ch.sourcemotion.agon.dto.StageDto
import ch.sourcemotion.agon.TournamentPath
import ch.sourcemotion.agon.ui.rest.RestGetRequest


class StageRequest(tournamentId: String, stageId: String)
    : RestGetRequest<StageDto>("${TournamentPath.ROOT_PATH}/$tournamentId/stages/$stageId", StageDto.serializer())