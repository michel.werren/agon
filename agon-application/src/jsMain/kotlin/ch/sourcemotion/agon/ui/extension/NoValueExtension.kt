package ch.sourcemotion.agon.ui.extension

import ch.sourcemotion.agon.ui.i18n.I18n


fun Any?.toStringOrNoValue() = this?.toString() ?: I18n.noValue()