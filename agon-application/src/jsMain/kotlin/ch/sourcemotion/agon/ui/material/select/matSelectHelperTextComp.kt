package ch.sourcemotion.agon.ui.material.select

import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.ui.material.MatBaseComponent
import ch.sourcemotion.agon.ui.util.UUIDJS
import ch.sourcemotion.agon.ui.util.asElementId
import kotlinx.html.id
import react.RBuilder
import react.RHandler
import react.RProps
import react.RState
import react.dom.p
import kotlin.contracts.ExperimentalContracts

interface MatSelectHelperTextCompProps : RProps {
    var text: String?
    var validationError: Boolean?
    var validationWarning: Boolean?
    var showPermanent: Boolean?
}

/**
 * Helper text component for [MatSelectComp]
 */
@ExperimentalContracts
class MatSelectHelperTextComp : MatBaseComponent<MatSelectHelperTextCompProps, RState, MDCSelectHelperText>() {

    private val textId = UUIDJS.genV4().asElementId()

    override fun RBuilder.render() {
        p(classes = buildCssClasses()) {
            ref {
                jsComponent = MDCSelectHelperText(it)
            }
            attrs {
                id = textId
                attributes["aria-hidden"] = "true"
            }
            props.text?.let { +it }
        }
    }

    private fun buildCssClasses(): String {
        var css = "mdc-select-helper-text"
        if (props.showPermanent.isTrue()) {
            css += " mdc-select-helper-text--persistent"
        }
        if (props.validationError.isTrue()) {
            css += " mdc-select-helper-text--validation-msg mat-select-error-validation-msg"
        }
        if (props.validationWarning.isTrue()) {
            css += " mdc-select-helper-text--validation-msg mat-select-warn-validation-msg"
        }

        return css
    }
}

@ExperimentalContracts
fun RBuilder.matSelectHelperText(handler: RHandler<MatSelectHelperTextCompProps>) =
        child(MatSelectHelperTextComp::class, handler)