package ch.sourcemotion.agon.ui.rest

enum class RestClientFailure {
    NOT_FOUND,
    SERVER_ERROR
}

class RestClientException(val failure: RestClientFailure) : Exception()