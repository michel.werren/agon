package ch.sourcemotion.agon.ui.util


/**
 * Extension to generate a html element id based on a UUID with a leading char to ensure there is no leading digit.
 */
fun UUIDJS.asElementId(): String = "a$hexNoDelim"