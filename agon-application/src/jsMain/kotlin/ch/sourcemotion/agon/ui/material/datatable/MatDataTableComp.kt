package ch.sourcemotion.agon.ui.material.datatable

import ch.sourcemotion.agon.extension.*
import ch.sourcemotion.agon.ui.extension.changeState
import ch.sourcemotion.agon.ui.material.button.matIconButton
import ch.sourcemotion.agon.ui.material.checkbox.MatCheckBoxState
import ch.sourcemotion.agon.ui.material.checkbox.matCheckbox
import ch.sourcemotion.agon.ui.material.datatable.model.*
import ch.sourcemotion.agon.ui.material.elevation.matElevation
import ch.sourcemotion.agon.ui.material.icon.MatIcon
import ch.sourcemotion.agon.ui.material.icon.MatIcons
import ch.sourcemotion.agon.ui.material.icon.matIcon
import ch.sourcemotion.agon.ui.material.select.MatSelectModel
import ch.sourcemotion.agon.ui.material.select.matSelect
import kotlinx.css.ColumnGap
import kotlinx.css.GridTemplateColumns
import kotlinx.css.LinearDimension
import kotlinx.html.DIV
import kotlinx.html.js.onClickFunction
import react.*
import react.dom.RDOMBuilder
import react.dom.div
import react.dom.span
import styled.css
import styled.styledDiv
import styled.styledSpan
import kotlin.contracts.ExperimentalContracts
import kotlin.math.ceil

interface MatDataTablePaginationL10n {
    fun getElementsPerSiteLabel(): String
    fun getCurrentPageDescription(overallItemCount: Int, currentStartRow: Int, currentEndRow: Int): String
}

data class SortState(val columnIdx: Int, val direction: SortDirection)

data class PaginationState(val itemsPerPage: Int = 10, val currentPageIdx: Int = 0, val maxPageIndex: Int) {
    fun withPreviousPageIdx() = copy(currentPageIdx = currentPageIdx - 1)
    fun withNextPageIdx() = copy(currentPageIdx = currentPageIdx + 1)
    fun withItemsPerPage(items: Int) = copy(itemsPerPage = items)
}

@ExperimentalContracts
interface MatDataTableCompProps : RProps {
    var tableDescription: MatDataTableDescription
    var tableData: MatDataTableData
    var l10n: MatDataTablePaginationL10n?
}

interface MatDataTableCompState : RState {
    var lastSortState: SortState?
    var seletedRows: Set<Int>
    /**
     * Copy of the original table data. So we can mutate its order etc.
     */
    var rowsToRender: List<List<String?>>?
    var paginationState: PaginationState?
}

/**
 * Material data table.
 */
@ExperimentalContracts
class MatDataTableComp : RComponent<MatDataTableCompProps, MatDataTableCompState>() {

    private val delegatingL10n: MatDataTablePaginationL10n = object : MatDataTablePaginationL10n {
        override fun getElementsPerSiteLabel(): String {
            return props.l10n?.getElementsPerSiteLabel().or("Elements per page")
        }

        override fun getCurrentPageDescription(overallItemCount: Int, currentStartRow: Int, currentEndRow: Int): String {
            return props.l10n?.getCurrentPageDescription(overallItemCount, currentStartRow, currentEndRow)
                    .or("$currentStartRow - $currentEndRow of $overallItemCount")
        }
    }

    init {
        state.seletedRows = HashSet()
    }

    override fun RBuilder.render() {
        val tableDescription = props.tableDescription
        val tableData = props.tableData

        // Dont render without configuration and data
        if (tableDescription.isNull() || tableData.isNull()) {
            throw IllegalStateException("No table data or description. Abort rendering")
        }

        div(classes = "mat-datatable") {
            matElevation {
                attrs {
                    level = tableDescription.elevation
                }
                div(classes = "mat-datatable-content") {
                    renderHead(tableDescription, tableData)
                    state.rowsToRender?.let { rowsToRender ->
                        rowsToRender.forEachIndexed { index, columnCells ->
                            renderBodyRow(tableDescription, rowsToRender, index, columnCells, tableData.rows[index].id)
                        }
                    }
                    val paginationState = state.paginationState
                    if (tableDescription.pagination && paginationState.isNotNull()) {
                        renderPagination(tableData, tableDescription, paginationState)
                    }
                }
            }
        }
    }

    override fun componentDidMount() {
        val tableData = props.tableData
        val tableDescription = props.tableDescription

        val originalDataRows = tableData.getSimpleRowsValues()
        changeState {
            if (tableDescription.pagination.isTrue()) {
                val maxPageIndex = calculateMaxPageIndex(originalDataRows.size)
                paginationState = PaginationState(maxPageIndex = maxPageIndex)
                rowsToRender = getItemsCurrentPage(
                        originalDataRows,
                        paginationState!!.currentPageIdx,
                        paginationState!!.itemsPerPage
                )
            } else {
                rowsToRender = originalDataRows
            }
        }
    }

    private fun RDOMBuilder<DIV>.renderHead(tableDesc: MatDataTableDescription, tableData: MatDataTableData) {
        val headerData = tableData.header

        styledDiv {
            val colums = tableDesc.columnDescription.colums
            css {
                classes.add("mat-datatable-header-row")
                classes.add("mat-datatable-row")

                // Leading column, either with checkbox or empty
                val gridTemplateWidths = buildRowGridCssWidth(tableDesc.columnDescription)
                gridTemplateColumns = GridTemplateColumns(gridTemplateWidths)
                columnGap = ColumnGap("${tableDesc.columnSpace}px")
            }

            // Render leading column
            div(classes = "mat-datatable-leading-columnValue") {}

            colums.forEachIndexed { columnIndex, column ->

                val cellValue = headerData.getValue(columnIndex)

                div(classes = "mat-datatable-cell") {
                    column.sort?.let { sort ->
                        matIcon {
                            attrs {
                                classes = "mat-datatable-head-sorted-cell-icon"
                                clickHandler = {
                                    val nextSortDirection = getNextSortDirection(columnIndex)

                                    val sorted =
                                            sortForColumn(tableData.getSimpleRowsValues(), columnIndex, nextSortDirection, sort)
                                    val paginationState = state.paginationState

                                    val finalRows = if (tableDesc.pagination && paginationState.isNotNull()) {
                                        getItemsCurrentPage(sorted, paginationState.currentPageIdx, paginationState.itemsPerPage)
                                    } else {
                                        sorted
                                    }
                                    changeState {
                                        rowsToRender = finalRows
                                        lastSortState = SortState(columnIndex, nextSortDirection)
                                    }
                                }
                                icon = getHeaderSortIcon(columnIndex)
                            }
                        }
                    }
                    styledSpan {
                        css {
                            if (column.sort.isNotNull()) {
                                marginLeft = LinearDimension("3px")
                            }
                        }
                        cellValue?.let {
                            +it
                        }
                    }
                }
            }

            div(classes = "mat-datatable-trailing-columnValue") {}
        }
    }

    private fun RDOMBuilder<DIV>.renderBodyRow(tableDesc: MatDataTableDescription, allRows: List<List<String?>>,
                                               rowIdx: Int, columnCells: List<String?>, rowId: String?) {

        val lastRow = allRows.size - 1 == rowIdx

        styledDiv {
            css {
                classes.add("mat-datatable-row")
                classes.add("mat-datatable-data-row")
                if (lastRow.isTrue()) {
                    classes.add("mat-datatable-row-last")
                }
                if (tableDesc.hasSelection()) {
                    classes.add("mat-datatable-row-selectable")
                }

                // Leading column, either with checkbox or empty
                val gridTemplateWidths = buildRowGridCssWidth(tableDesc.columnDescription)
                gridTemplateColumns = GridTemplateColumns(gridTemplateWidths)
                columnGap = ColumnGap("${tableDesc.columnSpace}px")
            }

            attrs {
                onClickFunction = {
                    tableDesc.selectedHandler?.let { handler ->
                        toggleSelectedState(rowId, rowIdx, handler)
                    }
                }
            }

            div(classes = "mat-datatable-leading-columnValue") {
                if (tableDesc.renderSelectionBoxes()) {
                    matCheckbox {
                        attrs {
                            state = if (this@MatDataTableComp.state.seletedRows.contains(rowIdx)) {
                                MatCheckBoxState.CHECKED
                            } else {
                                MatCheckBoxState.UNCHECKED
                            }
                        }
                    }
                }
            }

            columnCells.forEach { entry ->
                div(classes = "mat-datatable-cell") {
                    span(classes = "mat-datatable-cell-text") {
                        entry?.let {
                            +it
                        }
                    }
                }
            }

            div(classes = "mat-datatable-trailing-columnValue") {}
        }
    }

    private fun RBuilder.renderPagination(tableData: MatDataTableData, tableDesc: MatDataTableDescription,
                                          paginationState: PaginationState) {
        div(classes = "mat-datatable-pagination") {
            div(classes = "mat-datatable-pagination-item-per-page") {
                matSelect {
                    attrs {
                        label = delegatingL10n.getElementsPerSiteLabel()
                        deselectable = false
                        width = 150
                        model = listOf(
                                MatSelectModel("10", "10", selected = "10" == paginationState.itemsPerPage.toString()),
                                MatSelectModel("20", "20", selected = "20" == paginationState.itemsPerPage.toString()),
                                MatSelectModel("30", "30", selected = "30" == paginationState.itemsPerPage.toString())
                        )
                        changeHandler = { _, value ->
                            state.paginationState?.let {
                                val newPaginationState = it.withItemsPerPage(value.toInt())
                                changeStateForPageChange(tableDesc, tableData, PaginationState(newPaginationState.itemsPerPage, maxPageIndex = calculateMaxPageIndex(tableData.rows.size, newPaginationState.itemsPerPage)))
                            }
                        }
                    }
                }
            }
            div(classes = "mat-datatable-pagination-page-label") {
                props.tableData.rows.let { rows ->
                    state.paginationState?.let {
                        val currentStartRow = getCurrentPageStartIndex(it.currentPageIdx, it.itemsPerPage) + 1
                        val currentEndRow = getCurrentPageEndIndex(it.currentPageIdx, it.itemsPerPage, rows.size)
                        +delegatingL10n.getCurrentPageDescription(tableData.rows.size, currentStartRow, currentEndRow)
                    }
                }
            }
            div(classes = "mat-datatable-pagination-previous-icon") {
                matIconButton {
                    attrs {
                        icon = MatIcons.PAGINATION_PREVIOUS
                        disabled = paginationState.currentPageIdx == 0
                        clickHandler = {
                            changeStateForPageChange(tableDesc, tableData, paginationState.withPreviousPageIdx())
                        }
                    }
                }
            }
            div(classes = "mat-datatable-pagination-next-icon") {
                matIconButton {
                    attrs {
                        icon = MatIcons.PAGINATION_NEXT
                        disabled = paginationState.currentPageIdx >= paginationState.maxPageIndex
                        clickHandler = {
                            changeStateForPageChange(tableDesc, tableData, paginationState.withNextPageIdx())
                        }
                    }
                }
            }
            div(classes = "mat-datatable-trailing-columnValue") {}
        }
    }


    private fun changeStateForPageChange(tableDesc: MatDataTableDescription, tableData: MatDataTableData,
                                         paginationState: PaginationState) {
        val lastSortState = state.lastSortState
        val sortedRows = if (lastSortState.isNotNull()) {
            val sort = tableDesc.columnDescription.colums[lastSortState.columnIdx].sort
            sortForColumn(tableData.getSimpleRowsValues(), lastSortState.columnIdx, lastSortState.direction, sort!!)
        } else {
            tableData.getSimpleRowsValues()
        }
        val paginatedRows = getItemsCurrentPage(sortedRows, paginationState.currentPageIdx, paginationState.itemsPerPage)
        changeState {
            this.paginationState = paginationState
            rowsToRender = paginatedRows
        }
    }

    private fun sortForColumn(allRows: List<List<String?>>, columnIndex: Int, direction: SortDirection,
                              sort: ColumnSort): List<List<String?>> {
        return if (direction == SortDirection.NONE) {
            allRows
        } else {
            sort(columnIndex, allRows.newArrayListCopy(), direction)
        }
    }

    /**
     * When new properties are present, ensure new table data will be applied.
     */
    override fun componentWillReceiveProps(nextProps: MatDataTableCompProps) {
        if (nextProps.tableData != props.tableData || nextProps.tableDescription != props.tableDescription) {

            val tableData = nextProps.tableData
            val tableDesc = nextProps.tableDescription
            val newPaginationState = PaginationState(maxPageIndex = calculateMaxPageIndex(tableData.rows.size))

            val lastSortState = state.lastSortState
            val sortedRows = if (lastSortState.isNotNull()) {
                val sort = tableDesc.columnDescription.colums[lastSortState.columnIdx].sort
                sortForColumn(tableData.getSimpleRowsValues(), lastSortState.columnIdx, lastSortState.direction, sort!!)
            } else {
                tableData.getSimpleRowsValues()
            }
            val paginatedRows = getItemsCurrentPage(sortedRows, newPaginationState.currentPageIdx, newPaginationState.itemsPerPage)
            changeState {
                paginationState = newPaginationState
                rowsToRender = paginatedRows
            }
        }
    }

    private fun toggleSelectedState(rowId: String?, rowIdx: Int, selectionHandler: SelectionHandler) {
        changeState {
            seletedRows = if (seletedRows.contains(rowIdx)) {
                selectionHandler(rowId, rowIdx, false)
                HashSet(seletedRows).minusElement(rowIdx)
            } else {
                selectionHandler(rowId, rowIdx, true)
                HashSet(seletedRows).plus(rowIdx)
            }
        }
    }

    private fun buildRowGridCssWidth(columnDescription: MatDataTableColumnDescription): String {
        return "60px ".plus(columnDescription.colums.joinToString(" ") { column ->
            if (column.width.isNotNull()) {
                "${column.width}px"
            } else {
                "auto"
            }
        }).plus(" 60px")
    }

    private fun getHeaderSortIcon(columnIdx: Int): MatIcon {
        val lastSortState = state.lastSortState
        return if (lastSortState.isNotNull() && lastSortState.columnIdx == columnIdx) {
            when (lastSortState.direction) {
                SortDirection.DESC -> MatIcons.SORT_DESCENDING
                SortDirection.ASC -> MatIcons.SORT_ASCENDING
                SortDirection.NONE -> MatIcons.SORT_NONE
            }
        } else {
            MatIcons.SORT_NONE
        }
    }

    private fun getNextSortDirection(columnIdx: Int): SortDirection {
        val lastSortState = state.lastSortState
        return if (lastSortState.isNotNull() && lastSortState.columnIdx == columnIdx) {
            when (lastSortState.direction) {
                SortDirection.NONE -> SortDirection.ASC
                SortDirection.ASC -> SortDirection.DESC
                SortDirection.DESC -> SortDirection.NONE
            }
        } else {
            SortDirection.ASC
        }
    }

    /**
     * Builds a sublist of the given rows.
     */
    private fun getItemsCurrentPage(allRows: List<List<String?>>, currentPageIdx: Int,
                                    itemsPerPage: Int): List<List<String?>> {
        val rowCount = allRows.size

        val startIndex = getCurrentPageStartIndex(currentPageIdx, itemsPerPage)
        val endIndex = getCurrentPageEndIndex(currentPageIdx, itemsPerPage, rowCount)

        return allRows.subList(startIndex, endIndex)
    }

    private fun getCurrentPageEndIndex(currentPageIdx: Int, itemsPerPage: Int, allRowCount: Int): Int {
        val commonEnd = (currentPageIdx + 1) * itemsPerPage
        return if (commonEnd < allRowCount) {
            commonEnd
        } else {
            allRowCount
        }
    }

    private fun getCurrentPageStartIndex(currentPageIdx: Int, itemsPerPage: Int) = currentPageIdx * itemsPerPage

    /**
     * @return Max of pages against the current overall count of items and the configured count of items per page.
     */
    private fun calculateMaxPageIndex(allItemsCount: Int, itemsPerPage: Int = 10) = ceil(allItemsCount.div(itemsPerPage.toDouble())).toInt() - 1
}

@ExperimentalContracts
fun RBuilder.matDataTable(handler: RHandler<MatDataTableCompProps>) = child(MatDataTableComp::class, handler)