package ch.sourcemotion.agon.ui.component

import kotlinx.html.CommonAttributeGroupFacadeFlowInteractivePhrasingContent
import kotlinx.html.HTMLTag
import kotlinx.html.TagConsumer
import kotlinx.html.attributes.StringAttribute
import kotlinx.html.attributesMapOf
import react.RBuilder
import react.ReactElement
import react.dom.RDOMBuilder
import react.dom.tag

open class FixedLabel(initialAttributes: Map<String, String>, override val consumer: TagConsumer<*>) : HTMLTag("label", consumer, initialAttributes, null, true, false), CommonAttributeGroupFacadeFlowInteractivePhrasingContent {
    var form: String
        get() = StringAttribute().get(this, "form")
        set(newValue) {
            StringAttribute().set(this, "form", newValue)
        }

    var htmlFor: String
        get() = StringAttribute().get(this, "htmlFor")
        set(newValue) {
            StringAttribute().set(this, "htmlFor", newValue)
        }
}

inline fun RBuilder.fixLabel(classes: String? = null, block: RDOMBuilder<FixedLabel>.() -> Unit): ReactElement = tag(block) { FixedLabel(attributesMapOf("class", classes), it) }