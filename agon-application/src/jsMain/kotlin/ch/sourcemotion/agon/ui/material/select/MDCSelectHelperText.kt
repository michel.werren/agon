@file:JsModule("@material/select/helper-text")

package ch.sourcemotion.agon.ui.material.select

import ch.sourcemotion.agon.ui.material.MDCBase

@JsName("MDCSelectHelperText")
external class MDCSelectHelperText(element: dynamic) : MDCBase