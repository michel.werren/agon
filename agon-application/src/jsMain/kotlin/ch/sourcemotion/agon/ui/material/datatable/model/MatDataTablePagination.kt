package ch.sourcemotion.agon.ui.material.datatable.model

/**
 * Pagination for many rows.
 */
class MatDataTablePagination {
    var itemsPerPage = 10
    var changeable = true
}