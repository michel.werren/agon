package ch.sourcemotion.agon.ui.request

import ch.sourcemotion.agon.dto.RoundDto
import ch.sourcemotion.agon.TournamentPath
import ch.sourcemotion.agon.ui.rest.RestGetRequest
import kotlinx.serialization.list


/**
 * Request to get rounds of a group
 */
class RoundsOfGroupsRequest(tournamentId: String, groupIds: List<String>) : RestGetRequest<List<RoundDto>>("${TournamentPath.ROOT_PATH}/$tournamentId/rounds",
        RoundDto.serializer().list, mapOf("group_ids" to groupIds.joinToString(","))) {
    constructor(tournamentId: String, groupId: String) : this(tournamentId, listOf(groupId))
}