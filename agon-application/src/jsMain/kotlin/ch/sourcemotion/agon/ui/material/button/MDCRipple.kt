@file:JsModule("@material/ripple")

package ch.sourcemotion.agon.ui.material.button

import ch.sourcemotion.agon.ui.material.MDCBase

@JsName("MDCRipple")
external class MDCRipple(element: dynamic) : MDCBase {
    var unbounded: Boolean
}