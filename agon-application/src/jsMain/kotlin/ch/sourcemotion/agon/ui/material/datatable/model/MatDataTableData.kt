package ch.sourcemotion.agon.ui.material.datatable.model


/**
 * The table data.
 */
class MatDataTableData {

    val header = MatDataTableHeaderData()

    val rows = ArrayList<MatDataTableBodyRowData>()

    companion object {
        fun data(block: MatDataTableData.() -> Unit): MatDataTableData {
            val tableData = MatDataTableData()
            block(tableData)
            return tableData
        }
    }

    fun row(block: MatDataTableBodyRowData.() -> Unit) {
        val rowData = MatDataTableBodyRowData()
        block(rowData)
        rows.add(rowData)
    }

    fun header(block: MatDataTableHeaderData.() -> Unit) {
        block(header)
    }


    /**
     * @return Simple row / column structure with only the column values.
     */
    fun getSimpleRowsValues() = rows.map { row -> row.columValues }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is MatDataTableData) return false

        if (header != other.header) return false
        if (rows != other.rows) return false

        return true
    }

    override fun hashCode(): Int {
        var result = header.hashCode()
        result = 31 * result + rows.hashCode()
        return result
    }
}

/**
 * Table body row data. Optionally each row can obtain its concrete id.
 */
class MatDataTableBodyRowData : MatDataTableHeaderData() {
    /**
     * Id the row can be identified with. Helpful to identify an entity e.g.
     */
    var id: String = ""

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is MatDataTableBodyRowData) return false
        if (!super.equals(other)) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + id.hashCode()
        return result
    }
}

/**
 * Table header data
 */
open class MatDataTableHeaderData {
    val columValues = ArrayList<String?>()

    fun columnValue(value: String?) {
        columValues.add(value)
    }

    fun getValue(index: Int): String? {
        return try {
            columValues[index]
        } catch (e: Exception) {
            null
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is MatDataTableHeaderData) return false

        if (columValues != other.columValues) return false

        return true
    }

    override fun hashCode(): Int {
        return columValues.hashCode()
    }
}