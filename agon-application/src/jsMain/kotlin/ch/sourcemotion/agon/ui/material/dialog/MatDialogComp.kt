package ch.sourcemotion.agon.ui.material.dialog

import ch.sourcemotion.agon.extension.isFalse
import ch.sourcemotion.agon.extension.isNull
import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.ui.material.MatBaseComponent
import ch.sourcemotion.agon.ui.util.UUIDJS
import ch.sourcemotion.agon.ui.util.asElementId
import kotlinx.html.ButtonType
import kotlinx.html.id
import kotlinx.html.role
import org.w3c.dom.HTMLDivElement
import react.RBuilder
import react.RHandler
import react.RProps
import react.RState
import react.dom.button
import react.dom.div
import react.dom.footer
import react.dom.h2
import kotlin.contracts.ExperimentalContracts

data class DialogAction(val label: String, val actionValue: String = UUIDJS.genV4().hexNoDelim, val handler: () -> Unit)

interface MatDialogCompProps : RProps {
    var title: String?
    var scrollable: Boolean?
    var show: Boolean?
    var actions: List<DialogAction>?
    var closedHandler: (() -> Unit)?
}

private const val DIALOG_OPENING_EVENT = "MDCDialog:opening"
private const val DIALOG_OPENED_EVENT = "MDCDialog:opened"
private const val DIALOG_CLOSING_EVENT = "MDCDialog:closing"
private const val DIALOG_CLOSED_EVENT = "MDCDialog:closed"

@ExperimentalContracts
class MatDialogComp : MatBaseComponent<MatDialogCompProps, RState, MDCDialog>() {

    private val dialogTitleId = UUIDJS.genV4().asElementId()
    private val dialogContentId = UUIDJS.genV4().asElementId()
    private var contentElement: HTMLDivElement? = null

    override fun RBuilder.render() {
        div(classes = getRootCss()) {
            ref {
                if (jsComponent.isNull() && it != null) {
                    jsComponent = MDCDialog(it).apply {
                        listen(DIALOG_OPENED_EVENT, this@MatDialogComp::onDialogOpened)
                        listen(DIALOG_CLOSING_EVENT, this@MatDialogComp::onDialogClosing)
                        listen(DIALOG_CLOSED_EVENT, this@MatDialogComp::onDialogClosed)
                        if (props.show.isTrue()) {
                            open()
                        }
                    }
                }
            }

            attrs {
                role = "alertdialog"
                attributes["aria-modal"] = "true"
                attributes["aria-labelledby"] = dialogTitleId
                attributes["aria-describedby"] = dialogContentId
            }

            div(classes = "mdc-dialog__container") {
                div(classes = "mdc-dialog__surface") {
                    h2(classes = "mdc-dialog__title") {
                        attrs.id = dialogTitleId
                        props.title?.let { +it }
                    }
                    div(classes = "mdc-dialog__content") {
                        ref {
                            if (contentElement.isNull() && it != null) {
                                contentElement = it
                            }
                        }
                        attrs.id = dialogContentId
                        props.children()
                    }
                    props.actions?.let { actions ->
                        footer(classes = "mdc-dialog__actions") {
                            actions.forEach { action ->
                                button(classes = "mdc-button mdc-dialog__button") {
                                    attrs {
                                        type = ButtonType.button
                                        attributes["data-mdc-dialog-action"] = action.actionValue
                                    }
                                    +action.label
                                }
                            }
                        }
                    }
                }
            }
            div(classes = "mdc-dialog__scrim") { }
        }

        jsComponent?.let {
            if (props.show.isTrue() && it.isOpen.isFalse()) {
                it.open()
            }
        }
    }

    @Suppress("UNUSED_PARAMETER")
    private fun onDialogOpened(dialogEvent: DialogEvent) {
        contentElement?.setAttribute("aria-hidden", "true")
    }

    @Suppress("UNUSED_PARAMETER")
    private fun onDialogClosing(dialogEvent: DialogEvent) {
        contentElement?.removeAttribute("aria-hidden")
    }

    private fun onDialogClosed(event: DialogEvent) {
        props.actions?.let { actions ->
            actions.firstOrNull { it.actionValue == event.detail.action }?.let {
                it.handler()
            }
        }

        props.closedHandler?.invoke()
    }

    private fun getRootCss(): String {
        var css = "mdc-dialog"
        if (props.scrollable.isTrue()) {
            css += " mdc-dialog--scrollable"
        }
        return css
    }
}

@ExperimentalContracts
fun RBuilder.matDialog(handler: RHandler<MatDialogCompProps>) = child(MatDialogComp::class, handler)