package ch.sourcemotion.agon.ui.mediaquery

import ch.sourcemotion.agon.ui.extension.orZero
import org.w3c.dom.events.Event
import kotlin.browser.document
import kotlin.browser.window

private const val MOBILE_MAX = 767
private const val TABLET_MIN = 768
private const val TABLET_MAX = 1023
private const val DESKTOP_MIN = 1024

typealias MediaQueryListener = (MediaClass) -> Unit

enum class MediaClass {
    MOBILE,
    TABLET,
    DESKTOP
}

/**
 * Media query abstraction to access media query event from Kotlin
 */
class MediaQuery {

    private val listeners: MutableList<MediaQueryListener> = ArrayList()

    private val mobileQuery = window.matchMedia("(max-width: ${MOBILE_MAX}px)")
    private val tabletQuery = window.matchMedia("(min-width: ${TABLET_MIN}px) and (max-width: ${TABLET_MAX}px)")
    private val desktopQuery = window.matchMedia("(min-width: ${DESKTOP_MIN}px)")

    init {
        mobileQuery.addListener(this::onMobile)
        tabletQuery.addListener(this::onTablet)
        desktopQuery.addListener(this::onDesktop)
    }

    private fun onMobile(e: Event) {
        if (e.asDynamic().matches) {
            listeners.forEach { it(MediaClass.MOBILE) }
        }
    }

    private fun onTablet(e: Event) {
        if (e.asDynamic().matches) {
            listeners.forEach { it(MediaClass.TABLET) }
        }
    }

    private fun onDesktop(e: Event) {
        if (e.asDynamic().matches) {
            listeners.forEach { it(MediaClass.DESKTOP) }
        }
    }

    fun addListener(listener: MediaQueryListener) = listeners.add(listener)
    fun removeListener(listener: MediaQueryListener) = listeners.remove(listener)

    fun getCurrentMediaClass(): MediaClass {
        val width = maxOf(document.documentElement?.clientWidth.orZero(), window.innerWidth)
        return when {
            width <= MOBILE_MAX -> MediaClass.MOBILE
            width in TABLET_MIN..TABLET_MAX -> MediaClass.TABLET
            else -> MediaClass.DESKTOP
        }
    }

    fun dismiss() {
        mobileQuery.removeListener(this::onMobile)
        tabletQuery.removeListener(this::onTablet)
        desktopQuery.removeListener(this::onDesktop)
    }
}

