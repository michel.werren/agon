@file:JsModule("@material/form-field")

package ch.sourcemotion.agon.ui.material.form

import ch.sourcemotion.agon.ui.material.MDCBase

@JsName("MDCFormField")
external class MDCFormField(element: dynamic) : MDCBase {
    var input: MDCBase
}