@file:JsModule("@material/checkbox")

package ch.sourcemotion.agon.ui.material.checkbox

import ch.sourcemotion.agon.ui.material.MDCBase

@JsName("MDCCheckbox")
external class MDCCheckBox(element: dynamic) : MDCBase {
    var checked: Boolean
    var indeterminate: Boolean
    var value: Boolean
}