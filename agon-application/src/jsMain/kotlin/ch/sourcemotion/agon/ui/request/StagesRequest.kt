package ch.sourcemotion.agon.ui.request

import ch.sourcemotion.agon.dto.StageDto
import ch.sourcemotion.agon.TournamentPath
import ch.sourcemotion.agon.ui.rest.RestGetRequest
import kotlinx.serialization.list


class StagesRequest(tournamentId: String) : RestGetRequest<List<StageDto>>("${TournamentPath.ROOT_PATH}/$tournamentId/stages", StageDto.serializer().list)