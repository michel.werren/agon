package ch.sourcemotion.agon.ui.request

import ch.sourcemotion.agon.dto.RoundDto
import ch.sourcemotion.agon.TournamentPath
import ch.sourcemotion.agon.ui.rest.RestGetRequest
import kotlinx.serialization.list


class RoundsOfStageRequest(tournamentId: String, stageId: String) : RestGetRequest<List<RoundDto>>("${TournamentPath.ROOT_PATH}/$tournamentId/rounds",
        RoundDto.serializer().list, mapOf("stage_ids" to stageId))