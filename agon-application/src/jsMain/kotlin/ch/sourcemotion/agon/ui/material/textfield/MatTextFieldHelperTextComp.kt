package ch.sourcemotion.agon.ui.material.textfield

import ch.sourcemotion.agon.extension.isNull
import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.ui.material.MatBaseComponent
import ch.sourcemotion.agon.ui.material.select.MatSelectHelperTextComp
import ch.sourcemotion.agon.ui.material.select.MatSelectHelperTextCompProps
import ch.sourcemotion.agon.ui.util.UUIDJS
import ch.sourcemotion.agon.ui.util.asElementId
import kotlinx.html.id
import react.RBuilder
import react.RHandler
import react.RProps
import react.RState
import react.dom.p
import kotlin.contracts.ExperimentalContracts

interface MatTextFieldHelperTextCompProps : RProps {
    var text: String?
    var validationError: Boolean?
    var validationWarning: Boolean?
    var showPermanent: Boolean?
}

/**
 * Helper text component for [ch.sourcemotion.agon.ui.material.MatTextFieldComp]
 */
@ExperimentalContracts
class MatTextFieldHelperTextComp : MatBaseComponent<MatTextFieldHelperTextCompProps, RState, MDCTextFieldHelperText>() {

    private val textId = UUIDJS.genV4().asElementId()

    override fun RBuilder.render() {
        p(classes = buildCssClasses()) {
            ref {
                if (jsComponent.isNull() && it != null) {
                    jsComponent = MDCTextFieldHelperText(it)
                }
            }
            attrs {
                id = textId
                attributes["aria-hidden"] = "true"
            }
            props.text?.let { +it }
        }
    }

    private fun buildCssClasses(): String {
        var css = "mdc-text-field-helper-text"
        if (props.showPermanent.isTrue()) {
            css += " mdc-text-field-helper-text--persistent"
        }
        if (props.validationError.isTrue()) {
            css += " mdc-text-field-helper-text--validation-msg mat-text-field-error-validation-msg"
        }
        if (props.validationWarning.isTrue()) {
            css += " mdc-text-field-helper-text--validation-msg mat-text-field-warn-validation-msg"
        }

        return css
    }
}

@ExperimentalContracts
fun RBuilder.matTextFieldHelperText(handler: RHandler<MatSelectHelperTextCompProps>) =
        child(MatSelectHelperTextComp::class, handler)