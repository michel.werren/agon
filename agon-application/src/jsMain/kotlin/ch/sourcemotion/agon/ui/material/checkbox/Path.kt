package ch.sourcemotion.agon.ui.material.checkbox

import kotlinx.html.CommonAttributeGroupFacadeFlowInteractivePhrasingContent
import kotlinx.html.HTMLTag
import kotlinx.html.TagConsumer
import kotlinx.html.attributes.StringAttribute
import kotlinx.html.attributesMapOf
import react.RBuilder
import react.ReactElement
import react.dom.RDOMBuilder
import react.dom.tag


/**
 * SVG path element
 */
class Path(initialAttributes: Map<String, String>, override val consumer: TagConsumer<*>) : HTMLTag("path", consumer, initialAttributes, null, true, false), CommonAttributeGroupFacadeFlowInteractivePhrasingContent {
    var fill: String
        get() = StringAttribute().get(this, "fill")
        set(newValue) {
            StringAttribute().set(this, "fill", newValue)
        }

    var d: String
        get() = StringAttribute().get(this, "d")
        set(newValue) {
            StringAttribute().set(this, "d", newValue)
        }
}

inline fun RBuilder.path(classes: String? = null, block: RDOMBuilder<Path>.() -> Unit): ReactElement = tag(block) {
    Path(
        attributesMapOf("class", classes),
        it
    )
}