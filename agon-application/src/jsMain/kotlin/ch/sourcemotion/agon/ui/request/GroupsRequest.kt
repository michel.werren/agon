package ch.sourcemotion.agon.ui.request

import ch.sourcemotion.agon.dto.GroupDto
import ch.sourcemotion.agon.TournamentPath
import ch.sourcemotion.agon.ui.rest.RestGetRequest
import kotlinx.serialization.list

/**
 * Request to get groups of a stage
 */
class GroupsRequest(tournamentId: String, stageId: String) : RestGetRequest<List<GroupDto>>("${TournamentPath.ROOT_PATH}/$tournamentId/groups",
        GroupDto.serializer().list, mapOf("stage_ids" to stageId))