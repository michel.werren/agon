package ch.sourcemotion.agon.ui.request

import ch.sourcemotion.agon.dto.ParticipantDto
import ch.sourcemotion.agon.TournamentPath
import ch.sourcemotion.agon.ui.rest.RestGetRequest
import kotlinx.serialization.list


/**
 * Request to get a participant list of a tournament
 */
class ParticipantsRequest(tournamentId: String) : RestGetRequest<List<ParticipantDto>>("${TournamentPath.ROOT_PATH}/$tournamentId/participants", ParticipantDto.serializer().list)