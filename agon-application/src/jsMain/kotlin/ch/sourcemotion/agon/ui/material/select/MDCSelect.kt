@file:JsModule("@material/select")

package ch.sourcemotion.agon.ui.material.select

import ch.sourcemotion.agon.ui.material.MDCBase

@JsName("MDCSelect")
external class MDCSelect(element: dynamic) : MDCBase {
    val selectedIndex: Int
    val value: String

    fun listen(event: String, listener: () -> Unit)

    fun setValid(isValid: Boolean)
}