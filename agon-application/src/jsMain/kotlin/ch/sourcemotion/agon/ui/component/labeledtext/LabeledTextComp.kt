package ch.sourcemotion.agon.ui.component.labeledtext

import ch.sourcemotion.agon.extension.isTrue
import react.*
import react.dom.a
import react.dom.div
import react.dom.span
import kotlin.contracts.ExperimentalContracts

interface LabeledTextCompProps : RProps {
    var label: String?
    var text: String?
    var classes: String?
    var url: Boolean
}

/**
 * Text output component to render a label and its text.
 */
@ExperimentalContracts
class LabeledTextComp : RComponent<LabeledTextCompProps, RState>() {
    override fun RBuilder.render() {
        div(classes = getRootCss()) {
            span(classes = "labeled-text-label") {
                props.label?.let { +"$it:" }
            }

            if (props.url.isTrue()) {
                props.text?.let { url ->
                    a(classes = "labeled-text-text labeled-text-url", href = url, target = "_blank") { +url }
                }
            } else {
                span(classes = "labeled-text-text") {
                    props.text?.let { +it }
                }
            }
        }
    }

    private fun getRootCss(): String {
        var css = "labeled-text"
        props.classes?.let {
            css += " $it"
        }
        return css
    }
}

@ExperimentalContracts
fun RBuilder.labeledText(handler: RHandler<LabeledTextCompProps>) = child(LabeledTextComp::class, handler)