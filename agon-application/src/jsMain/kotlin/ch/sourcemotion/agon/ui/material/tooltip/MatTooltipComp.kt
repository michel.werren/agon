package ch.sourcemotion.agon.ui.material.tooltip

import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.ui.extension.changeState
import kotlinx.css.LinearDimension
import mu.KotlinLogging
import react.*
import react.dom.findDOMNode
import styled.css
import styled.styledDiv
import kotlin.contracts.ExperimentalContracts

interface MatTooltipCompProps : RProps {
    var text: String?
}

interface MatTooltipCompState : RState {
    var topPosition: Int?
    var leftPosition: Int?
    var active: Boolean?
}

/**
 * Material tool tip
 */
@ExperimentalContracts
class MatTooltipComp : RComponent<MatTooltipCompProps, MatTooltipCompState>() {

    companion object {
        private val log = KotlinLogging.logger("MatTooltipComp")
    }

    @ExperimentalContracts
    override fun componentDidMount() {
        val thisDomNode = findDOMNode(this)
        if (thisDomNode.isNotNull()) {
            val tooltipParent = thisDomNode.parentElement
            if (tooltipParent.isNotNull()) {
                val parentBottomCoordinate = tooltipParent.clientTop + tooltipParent.clientHeight
                // Middle on the x axis of the parent component
                val parentHorizontalMiddleCoordinate = tooltipParent.clientLeft + (tooltipParent.clientWidth / 2)
                val thisWidth = thisDomNode.clientWidth

                log.debug { "Tooltip width $thisWidth" }

                changeState {
                    topPosition = parentBottomCoordinate + 10
                    leftPosition = parentHorizontalMiddleCoordinate - (thisWidth / 2)
                    log.debug { "Tooltip position $leftPosition / $topPosition" }
                }

                tooltipParent.addEventListener("mouseover", callback =  {
                    log.debug { "Tooltip activated" }
                    changeState {
                        active = true
                    }
                })
                tooltipParent.addEventListener("mouseout", callback =  {
                    log.debug { "Tooltip deactivated" }
                    changeState {
                        active = false
                    }
                })
            } else {
                log.warn { "Unable to get tool tip's parent element" }
            }
        } else {
            log.warn { "Unable to find tool tip's dom node" }
        }
    }

    override fun RBuilder.render() {
        styledDiv {
            css {
                state.topPosition?.let {
                    top = LinearDimension("${it}px")
                }
                state.leftPosition?.let {
                    left = LinearDimension("${it}px")
                }
                classes.add("mat-tooltip")
                if (state.active.isTrue()) {
                    classes.add("mat-tooltip-active")
                }
            }

            props.text?.let { +it }
        }
    }
}

@ExperimentalContracts
fun RBuilder.matTooltip(handler: RHandler<MatTooltipCompProps>) = child(MatTooltipComp::class, handler)