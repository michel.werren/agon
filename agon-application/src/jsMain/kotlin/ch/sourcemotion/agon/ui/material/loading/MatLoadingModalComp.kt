package ch.sourcemotion.agon.ui.material.loading

import ch.sourcemotion.agon.extension.isTrue
import react.RBuilder
import react.RComponent
import react.RHandler
import react.RState
import react.dom.div
import kotlin.contracts.ExperimentalContracts

interface MatLoadingModalCompProps : MatLoadingCompProps {
    var show: Boolean?
}

@ExperimentalContracts
class MatLoadingModalComp : RComponent<MatLoadingModalCompProps, RState>() {
    override fun RBuilder.render() {
        if (props.show.isTrue()) {
            div(classes = "mat-loading-modal-comp") {
                matLoading {
                    attrs {
                        text = props.text
                        indeterminate = props.indeterminate
                        reversed = props.reversed
                    }
                }
            }
        }
    }
}

@ExperimentalContracts
fun RBuilder.matLoadingModal(handler: RHandler<MatLoadingModalCompProps>) = child(MatLoadingModalComp::class, handler)