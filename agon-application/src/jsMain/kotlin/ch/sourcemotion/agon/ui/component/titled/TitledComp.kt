package ch.sourcemotion.agon.ui.component.titled

import ch.sourcemotion.agon.ui.material.button.matFab
import ch.sourcemotion.agon.ui.material.icon.MatIcons
import react.*
import react.dom.div
import kotlin.contracts.ExperimentalContracts

interface TitledCompProps : RProps {
    var title: String?
    var history: dynamic
}

@ExperimentalContracts
class TitledComp : RComponent<TitledCompProps, RState>() {
    override fun RBuilder.render() {
        div(classes = "titled-comp") {
            div(classes = "titled-comp-title") {
                props.title?.let { +it }
            }

            if (props.history != null) {
                div(classes = "titled-comp-backbutton") {
                    matFab {
                       attrs {
                           icon = MatIcons.BACKSPACE
                           mini =  true
                           clickHandler = {
                               props.history.goBack()
                           }
                       }
                    }
                }
            }

            div(classes = "titled-comp-content") {
                props.children()
            }
        }
    }
}

@ExperimentalContracts
fun RBuilder.titled(handler: RHandler<TitledCompProps>) = child(TitledComp::class, handler)