@file:JsModule("@material/textfield/helper-text")

package ch.sourcemotion.agon.ui.material.textfield

import ch.sourcemotion.agon.ui.material.MDCBase

@JsName("MDCTextFieldHelperText")
external class MDCTextFieldHelperText(element: dynamic) : MDCBase