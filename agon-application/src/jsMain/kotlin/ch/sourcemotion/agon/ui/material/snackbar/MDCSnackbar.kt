@file:JsModule("@material/snackbar")

package ch.sourcemotion.agon.ui.material.snackbar

import ch.sourcemotion.agon.ui.material.MDCBase

@JsName("MDCSnackbar")
external class MDCSnackbar(element: dynamic) : MDCBase {
    fun show(text: dynamic)
    fun listen(event: String, listener: () -> Unit)
}