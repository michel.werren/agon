package ch.sourcemotion.agon.ui.dto

import ch.sourcemotion.agon.dto.TournamentDto
import ch.sourcemotion.agon.ui.moment.*


fun TournamentDto.dateStart() : MomentTZ? {
    return scheduledDateStart?.let { getMomentTZZurich(it) }
}

fun TournamentDto.dateEnd() : MomentTZ? {
    return scheduledDateEnd?.let { getMomentTZZurich(it) }
}

fun String?.toMomentTZ(): MomentTZ? = this?.let { getMomentTZZurich(it) }

fun String?.toMoment(): Moment? = this?.let { getMoment(it) }

fun String?.toMomentUtc(): Moment? = this?.let { getMomentUtc(it) }