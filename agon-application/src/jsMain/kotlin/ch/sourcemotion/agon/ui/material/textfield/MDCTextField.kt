@file:JsModule("@material/textfield")

package ch.sourcemotion.agon.ui.material.textfield

import ch.sourcemotion.agon.ui.material.MDCBase

@JsName("MDCTextField")
external class MDCTextField(element: dynamic) : MDCBase {
    val valid: Boolean
    var value: String
    fun focus()
}