@file:JsModule("@material/textfield/icon")

package ch.sourcemotion.agon.ui.material.textfield

import ch.sourcemotion.agon.ui.material.MDCBase

@JsName("MDCTextFieldIcon")
external class MDCTextFieldIcon(element: dynamic) : MDCBase