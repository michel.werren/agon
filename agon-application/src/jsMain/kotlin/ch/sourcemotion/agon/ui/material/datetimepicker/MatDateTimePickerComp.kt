package ch.sourcemotion.agon.ui.material.datetimepicker

import ch.sourcemotion.agon.extension.isFalse
import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.extension.isNull
import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.ui.material.icon.MatIcons
import ch.sourcemotion.agon.ui.material.textfield.MatTextFieldComp
import ch.sourcemotion.agon.ui.material.textfield.matTextField
import ch.sourcemotion.agon.ui.moment.DateFormat
import ch.sourcemotion.agon.ui.moment.Moment
import ch.sourcemotion.agon.ui.moment.getMomentTZZurich
import mu.KotlinLogging
import react.*
import kotlin.contracts.ExperimentalContracts
import kotlin.js.Date

class FlickCompConf {
    var hr24: Boolean = true
    var dateOnly: Boolean = true
    var timeOnly: Boolean = false
    var minDate: Moment? = null
    var dateFormat: DateFormat = DateFormat.DATE_ONLY
}

interface MatDateTimePickerProps : RProps {
    var label: String?
    var initDate: Moment?
    var clear: Boolean?
    var required: Boolean?
    var conf: FlickCompConf?
    var changeHandler: ((Moment) -> Unit)?
}

/**
 * Component to pick a date and a time
 */
@ExperimentalContracts
class MatDateTimePickerComp : RComponent<MatDateTimePickerProps, RState>() {

    companion object {
        private val log = KotlinLogging.logger("MatDateTimePickerComp")

        init {
            globalFlatPickrConfiguration()
            log.debug { "Flatpickr globals configured" }
        }
    }

    private var flatPickr: Flatpickr? = null
    private var textField: MatTextFieldComp? = null

    override fun RBuilder.render() {
        matTextField {
            ref {
                if (it != null && textField.isNull()) {
                    textField = it
                }
            }

            attrs {
                hintText = props.label
                trailingIcon = MatIcons.CALENDAR
                outlined = true
                clear = props.clear
                required = props.required.isTrue()
                readOnly = true
                focusOnIconClick = true
                props.initDate?.let { date ->
                    initialText = date.format(DateFormat.DATE_ONLY.momentFormat)
                }
            }
        }
        if (props.clear.isTrue()) {
            clear()
        }
    }


    @ExperimentalContracts
    override fun componentDidMount() {
        if (textField.isNotNull()) {
            flatPickr = flatpickrRef(textField!!.getTextField(), options())
        } else {
            log.error { "Flatpickr input element is not available" }
        }
    }

    override fun componentWillUnmount() {
        destroyFlatPickr()
    }

    fun destroyFlatPickr() {
        flatPickr?.let {
            it.destroy()
            log.debug { "Flatpickr destroyed" }
        }
        flatPickr = null
    }

    /**
     * Configuration options for the date time picker.
     */
    @ExperimentalContracts
    private fun options(): dynamic {
        val conf = if (props.conf.isNotNull()) {
            props.conf!!
        } else {
            FlickCompConf()
        }

        val options = js("{}")
        options["enableTime"] = conf.dateOnly.isFalse()
        options["noCalendar"] = conf.timeOnly.isTrue()
        options["time_24hr"] = conf.hr24.isTrue()

        options["dateFormat"] = conf.dateFormat.faltPickrFormat
        conf.minDate?.let { minDate ->
            options["minDate"] = minDate.toDate()
        }
        props.initDate?.let { initDate ->
            options["defaultDate"] = initDate.format(DateFormat.DATE_ONLY.momentFormat)
        }
        options["onClose"] = this::applyChanges
        return options
    }

    @Suppress("UNUSED_PARAMETER")
    @ExperimentalContracts
    private fun applyChanges(date: Array<Date>, s: dynamic, i: dynamic) {
        if (date.isNotNull() && date.isNotEmpty()) {
            val mayFreshChosen = getMomentTZZurich(date[0].toISOString())
            props.changeHandler?.invoke(mayFreshChosen)
        }
    }

    private fun clear() {
        flatPickr?.clear()
        flatPickr?.close()
    }
}


@ExperimentalContracts
fun RBuilder.matDateTimePicker(handler: RHandler<MatDateTimePickerProps>) = child(MatDateTimePickerComp::class, handler)