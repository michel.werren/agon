package ch.sourcemotion.agon.ui.material.datatable.model

import ch.sourcemotion.agon.extension.isNeitherNullOrEmpty
import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.ui.material.MAT_CELL_DIMENSION
import kotlin.contracts.ExperimentalContracts

/**
 * Handler called when a row is selected ot deselected.
 * First parameter is the index of the row (header not counted). The second is the state of selection. True when selected,
 * or false when deselected.
 */
typealias SelectionHandler = (rowId: String?, rowIndex: Int, selected: Boolean) -> Unit

typealias ColumnSort = (columnIndex: Int, rows: List<List<String?>>, direction: SortDirection) -> List<List<String?>>

@ExperimentalContracts
class MatDataTableDescription {

    lateinit var columnDescription: MatDataTableColumnDescription
    /**
     * When true checkboxes will appear they are checked when the row is selected.
     */
    var selectBoxes = false

    /**
     * Select handler.
     * Important!! -> The selection is only active when this handler is present.
     */
    var selectedHandler: SelectionHandler? = null

    /**
     * Space in px between columns.
     */
    var columnSpace = MAT_CELL_DIMENSION * 2

    /**
     * Elevation grade the table should be rendered.
     */
    var elevation = 1

    var pagination = true

    companion object {
        fun description(block: MatDataTableDescription.() -> Unit): MatDataTableDescription {
            val model = MatDataTableDescription()
            block(model)
            return model
        }
    }

    fun colums(block: MatDataTableColumnDescription.() -> Unit) {
        val columnModel = MatDataTableColumnDescription()
        block(columnModel)
        this.columnDescription = columnModel
    }

    fun hasSelection() = selectedHandler.isNotNull()
    fun renderSelectionBoxes() = selectedHandler.isNotNull() && selectBoxes
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is MatDataTableDescription) return false

        if (columnDescription != other.columnDescription) return false
        if (selectBoxes != other.selectBoxes) return false
        if (selectedHandler != other.selectedHandler) return false
        if (columnSpace != other.columnSpace) return false
        if (elevation != other.elevation) return false
        if (pagination != other.pagination) return false

        return true
    }

    override fun hashCode(): Int {
        var result = columnDescription.hashCode()
        result = 31 * result + selectBoxes.hashCode()
        result = 31 * result + (selectedHandler?.hashCode() ?: 0)
        result = 31 * result + columnSpace
        result = 31 * result + elevation
        result = 31 * result + pagination.hashCode()
        return result
    }
}

class MatDataTableColumnDescription {
    val colums: MutableList<MatDataTableColumn> = ArrayList()

    fun column(block: MatDataTableColumn.() -> Unit) {
        val column = MatDataTableColumn()
        block(column)
        colums.add(column)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is MatDataTableColumnDescription) return false

        if (colums != other.colums) return false

        return true
    }

    override fun hashCode(): Int {
        return colums.hashCode()
    }
}

class MatDataTableColumn {
    /**
     * When [Int.MIN_VALUE] "auto" will be used. Value will used as px.
     */
    var width: Int? = null

    var sort: ColumnSort? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is MatDataTableColumn) return false

        if (width != other.width) return false
        if (sort != other.sort) return false

        return true
    }

    override fun hashCode(): Int {
        var result = width ?: 0
        result = 31 * result + (sort?.hashCode() ?: 0)
        return result
    }
}

enum class SortDirection {
    ASC, DESC, NONE
}

@ExperimentalContracts
val stringColumnSort = { columnIdx: Int, rows: List<List<String?>>, direction: SortDirection ->
    rows.sortedWith(Comparator { a, b ->
        val aValue = a[columnIdx]
        val bValue = b[columnIdx]
        if (aValue.isNeitherNullOrEmpty() && bValue.isNeitherNullOrEmpty()) {
            if (direction == SortDirection.ASC) {
                aValue.compareTo(bValue)
            } else {
                bValue.compareTo(aValue)
            }
        } else {
            0
        }
    })
}