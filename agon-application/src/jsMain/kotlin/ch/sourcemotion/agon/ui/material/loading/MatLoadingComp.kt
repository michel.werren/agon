package ch.sourcemotion.agon.ui.material.loading

import ch.sourcemotion.agon.extension.isTrue
import kotlinx.html.role
import react.*
import react.dom.div
import react.dom.span
import kotlin.contracts.ExperimentalContracts

interface MatLoadingCompProps : RProps {
    var text: String?
    var indeterminate: Boolean?
    var reversed: Boolean?
}

@ExperimentalContracts
class MatLoadingComp : RComponent<MatLoadingCompProps, RState>() {
    override fun RBuilder.render() {
        div(classes = "mat-loading-comp") {
            div(classes = "loading-text") {
                props.text?.let { +it }
            }
            div(classes = "${getProgressCss()} mat-loading-progress-bar-primary-color") {
                attrs.role = "progressbar"
                div(classes = "mdc-linear-progress__buffering-dots") {}
                div(classes = "mdc-linear-progress__buffer") {}
                div(classes = "mdc-linear-progress__bar mdc-linear-progress__primary-bar") {
                    span(classes = "mdc-linear-progress__bar-inner") {  }
                }
                div(classes = "mdc-linear-progress__bar mdc-linear-progress__secondary-bar") {
                    span(classes = "mdc-linear-progress__bar-inner") {  }
                }
            }
        }
    }

    private fun getProgressCss(): String {
        var css = "mdc-linear-progress"
        if (props.indeterminate.isTrue()) {
            css += " mdc-linear-progress--indeterminate"
        }
        if (props.reversed.isTrue()) {
            css += " mdc-linear-progress--reversed"
        }
        return css
    }
}

@ExperimentalContracts
fun RBuilder.matLoading(handler: RHandler<MatLoadingCompProps>) = child(MatLoadingComp::class, handler)