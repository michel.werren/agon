package ch.sourcemotion.agon.ui.i18n

import ch.sourcemotion.agon.ui.rest.RestClientFailure
import mu.KotlinLogging
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

private external val production: Boolean

@JsModule("i18next")
private external val i18next: I18Next

@JsModule("i18next-xhr-backend")
private external val backend: Any

private val namespaces = arrayOf("agon")

/**
 * Kotlin entry point to access the I18Next API
 */
class I18n {
    companion object {
        private val log = KotlinLogging.logger("I18n")
        private var init = false

        /**
         * Starts the I18n system within a coroutine
         */
        suspend fun start() {
            suspendCoroutine<Unit?> {
                if (!init) {
                    init = true
                    i18next.use(backend)
                    i18next.init(i18NextInitParams().apply {
                        fallbackLng = "de"
                        debug = !production
                        ns = namespaces
                        defaultNS = "agon"
                        backend = i18NextBackendInitParams().apply {
                            loadPath = "/i18n/{{ns}}-{{lng}}.json"
                            crossDomain = true
                        }
                    }) { _, _ ->
                        log.info { "i18Next started" }
                        it.resume(null)
                    }
                } else {
                    log.warn { "Dont start i18Next multiple times" }
                }
            }
        }

        fun default(key: String, options: Map<String, Any>? = null): String = i18next.t(key, mapAsOptions(options))

        fun failure(prefixKey: String, failure: RestClientFailure, options: Map<String, Any>? = null): String {
            val prefix = i18next.t(prefixKey, mapAsOptions(options))
            return when (failure) {
                RestClientFailure.NOT_FOUND -> I18n.default("FailureMessage.NotFoundSuffix", mapOf("prefix" to prefix))
                RestClientFailure.SERVER_ERROR -> I18n.default("FailureMessage.LoadingFailedSuffix", mapOf("prefix" to prefix))
            }
        }

        fun noValue() = default("NoValue")
    }
}

fun mapAsOptions(options: Map<String, Any>?): dynamic {
    return if (options != null) {
        val dyn = js("{}")
        options.forEach {
            dyn[it.key] = it.value
        }
        dyn
    } else {
        js("{}")
    }
}

/**
 * i18Next backend configuration
 */
external interface I18NextBackendInitParams {
    var loadPath: String
    var crossDomain: Boolean
}

fun i18NextBackendInitParams(): I18NextBackendInitParams = js("{}")

/**
 * i18Next common configuration
 */
external interface I18NextInitParams {
    var fallbackLng: String?
    var debug: Boolean?
    var ns: Array<String>?
    var defaultNS: String?
    var backend: I18NextBackendInitParams?
}

fun i18NextInitParams(): I18NextInitParams = js("{}")

external interface TranslationOptions

/**
 * Functionality of i18Next
 */
external interface I18Next {
    fun init(params: I18NextInitParams, callback: (Any, Any) -> Unit)
    fun use(module: Any): I18Next
    fun exists(key: String): Boolean
    fun loadLanguages(language: String, callback: (Any) -> Unit)
    fun loadNamespaces(namespaces: String, callback: (Any) -> Unit)
    fun changeLanguage(language: String, callback: (Any) -> Unit)
    fun setDefaultNamespace(nameSpace: String)
    fun t(key: String, options: dynamic): String
}
