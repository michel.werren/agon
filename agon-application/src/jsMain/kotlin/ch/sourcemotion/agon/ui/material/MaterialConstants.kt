package ch.sourcemotion.agon.ui.material

/**
 * Cell dimension, defined by material design
 */
const val MAT_CELL_DIMENSION = 8
/**
 * Icon cell dimension, defined by material design
 */
const val MAT_ICON_CELL_DIMENSION = 8