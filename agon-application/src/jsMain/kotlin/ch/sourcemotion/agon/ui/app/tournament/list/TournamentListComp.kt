package ch.sourcemotion.agon.ui.app.tournament.list

import ch.sourcemotion.agon.dto.DisciplineDto
import ch.sourcemotion.agon.dto.TournamentDto
import ch.sourcemotion.agon.extension.isNeitherNullOrEmpty
import ch.sourcemotion.agon.extension.isNotNull
import ch.sourcemotion.agon.extension.isTrue
import ch.sourcemotion.agon.ui.component.failurehandling.FailureHandlingComp
import ch.sourcemotion.agon.ui.component.failurehandling.FailureHandlingCompState
import ch.sourcemotion.agon.ui.component.titled.titled
import ch.sourcemotion.agon.ui.dto.dateEnd
import ch.sourcemotion.agon.ui.dto.dateStart
import ch.sourcemotion.agon.ui.extension.changeState
import ch.sourcemotion.agon.ui.extension.navigate
import ch.sourcemotion.agon.ui.extension.toStringOrNoValue
import ch.sourcemotion.agon.ui.i18n.I18n
import ch.sourcemotion.agon.ui.material.button.matFab
import ch.sourcemotion.agon.ui.material.datatable.MatDataTablePaginationL10n
import ch.sourcemotion.agon.ui.material.datatable.matDataTable
import ch.sourcemotion.agon.ui.material.datatable.model.MatDataTableData
import ch.sourcemotion.agon.ui.material.datatable.model.MatDataTableDescription
import ch.sourcemotion.agon.ui.material.datetimepicker.matDateTimePicker
import ch.sourcemotion.agon.ui.material.icon.MatIcons
import ch.sourcemotion.agon.ui.material.loading.matLoadingModal
import ch.sourcemotion.agon.ui.material.textfield.Key
import ch.sourcemotion.agon.ui.material.textfield.KeyPressed
import ch.sourcemotion.agon.ui.material.textfield.matTextField
import ch.sourcemotion.agon.ui.mediaquery.MediaClass
import ch.sourcemotion.agon.ui.mediaquery.MediaQuery
import ch.sourcemotion.agon.ui.moment.DATE_ONLY_FORMAT
import ch.sourcemotion.agon.ui.moment.MomentTZ
import ch.sourcemotion.agon.ui.moment.getMomentTZZurich
import ch.sourcemotion.agon.ui.request.DisciplinesRequest
import ch.sourcemotion.agon.ui.request.TournamentsRequest
import ch.sourcemotion.agon.ui.rest.RestClient
import ch.sourcemotion.agon.ui.rest.RestClientException
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import mu.KotlinLogging
import react.RBuilder
import react.RProps
import react.dom.div
import kotlin.contracts.ExperimentalContracts

class TournamentListDisplayModel(
    val id: String, val name: String?, val disciplineName: String?,
    val startDate: MomentTZ?, val endDate: MomentTZ?, val platform: String
)

data class TournamentListFilterModel(
    val textFilter: String? = null,
    val startDateFilter: MomentTZ? = null,
    val endDateFilter: MomentTZ? = null
)

interface TournamentListCompState : FailureHandlingCompState {
    var listModel: List<TournamentListDisplayModel>?
    var disciplines: List<DisciplineDto>?
    var tournaments: List<TournamentDto>?
    var filter: TournamentListFilterModel
    var clearFilterFields: Boolean?
    var mediaClass: MediaClass
}

@ExperimentalContracts
class TournamentListComp : FailureHandlingComp<RProps, TournamentListCompState>() {

    companion object {
        private val log = KotlinLogging.logger("TournamentListComp")
    }

    private val mediaQuery: MediaQuery

    init {
        state.filter = TournamentListFilterModel()

        mediaQuery = MediaQuery()
        mediaQuery.addListener(this::onMediaClassChange)
        state.mediaClass = mediaQuery.getCurrentMediaClass()
        log.debug { "Media class: ${state.mediaClass.name}" }
    }

    override fun componentDidMount() {
        loadTableData()
    }

    override fun RBuilder.render() {
        renderFailureDialog()
        titled {
            attrs {
                title = I18n.default("Tournaments")
            }

            div(classes = "tournament-list-comp") {
                div(classes = "tournament-list-comp-filter") {
                    div(classes = "tournament-list-comp-filter-text") {
                        matTextField {
                            attrs {
                                hintText = I18n.default("Filter")
                                outlined = true
                                clear = state.clearFilterFields
                                changeHandler = {
                                    changeState {
                                        filter = filter.copy(textFilter = it)
                                    }
                                }
                                keyPressed = KeyPressed(Key.RETURN, this@TournamentListComp::applyFilter)
                            }
                        }
                    }

                    div(classes = "tournament-list-comp-filter-from-date") {
                        matDateTimePicker {
                            attrs {
                                label = I18n.default("StartDate")
                                changeHandler = {
                                    changeState {
                                        filter = filter.copy(startDateFilter = getMomentTZZurich(it))
                                    }
                                }
                                clear = state.clearFilterFields
                            }
                        }
                    }

                    div(classes = "tournament-list-comp-filter-until-date") {
                        matDateTimePicker {
                            attrs {
                                label = I18n.default("EndDate")
                                changeHandler = {
                                    changeState {
                                        filter = filter.copy(endDateFilter = getMomentTZZurich(it))
                                    }
                                }
                                clear = state.clearFilterFields
                            }
                        }
                    }

                    matFab {
                        attrs {
                            icon = MatIcons.SEARCH
                            mini = true
                            primaryColor = true
                            classes = "tournament-list-comp-filter-btn tournament-list-comp-filter-fab"
                            clickHandler = this@TournamentListComp::applyFilter
                        }
                    }

                    matFab {
                        attrs {
                            icon = MatIcons.CLEAR
                            mini = true
                            primaryColor = true
                            classes = "tournament-list-comp-filter-clear-btn tournament-list-comp-filter-fab"
                            clickHandler = this@TournamentListComp::clearFilter
                        }
                    }
                    matFab {
                        attrs {
                            icon = MatIcons.REFRESH
                            mini = true
                            primaryColor = true
                            classes = "tournament-list-comp-filter-refresh-btn tournament-list-comp-filter-fab"
                            clickHandler = {
                                changeState {
                                    listModel = null
                                    disciplines = null
                                    tournaments = null
                                }
                                loadTableData()
                            }
                        }
                    }
                }
                div(classes = "tournament-list-comp-list") {
                    val model = state.listModel
                    if (model.isNotNull()) {
                        matDataTable {
                            attrs {
                                tableDescription = MatDataTableDescription.description {
                                    selectBoxes = false
                                    elevation = 0
                                    colums {
                                        column {
                                            width = if (state.mediaClass == MediaClass.MOBILE) {
                                                140
                                            } else {
                                                null
                                            }
                                        }
                                        column {
                                            width = if (state.mediaClass == MediaClass.MOBILE) {
                                                100
                                            } else {
                                                120
                                            }
                                        }
                                        column {
                                            width = 80
                                        }
                                        column {
                                            width = 80
                                        }
                                        if (state.mediaClass != MediaClass.MOBILE) {
                                            column {
                                                width = 120
                                            }
                                        }
                                    }

                                    selectedHandler = { rowId, _, _ ->
                                        navigate("/tournaments/$rowId")
                                    }
                                }
                                tableData = MatDataTableData.data {
                                    header {
                                        columnValue(I18n.default("TournamentName"))
                                        columnValue(I18n.default("Discipline"))
                                        columnValue(I18n.default("StartDate"))
                                        columnValue(I18n.default("EndDate"))
                                        if (state.mediaClass != MediaClass.MOBILE) {
                                            columnValue(I18n.default("Platform"))
                                        }
                                    }
                                    model.forEach { rowValue ->
                                        row {
                                            id = rowValue.id
                                            columnValue(rowValue.name)
                                            columnValue(rowValue.disciplineName)
                                            columnValue(rowValue.startDate?.format(DATE_ONLY_FORMAT))
                                            columnValue(rowValue.endDate?.format(DATE_ONLY_FORMAT))
                                            if (state.mediaClass != MediaClass.MOBILE) {
                                                columnValue(rowValue.platform)
                                            }
                                        }
                                    }
                                }
                                l10n = object : MatDataTablePaginationL10n {
                                    override fun getElementsPerSiteLabel() = I18n.default("List.ElementsPerPage")
                                    override fun getCurrentPageDescription(
                                        overallItemCount: Int,
                                        currentStartRow: Int,
                                        currentEndRow: Int
                                    ): String {
                                        return I18n.default(
                                            "List.PageDescription",
                                            mapOf(
                                                "current_start" to currentStartRow,
                                                "current_end" to currentEndRow,
                                                "overall_element_count" to overallItemCount
                                            )
                                        )
                                    }
                                }
                            }
                        }
                    } else {
                        matLoadingModal {
                            attrs {
                                indeterminate = true
                                text = I18n.default("LoadMessage.LoadTournaments")
                                show = state.failureMessage.isNullOrBlank()
                            }
                        }
                    }
                }
            }
        }
    }

    override fun componentWillUnmount() {
        mediaQuery.dismiss()
    }

    /**
     * Rerenders when field was cleared
     */
    override fun componentDidUpdate(prevProps: RProps, prevState: TournamentListCompState, snapshot: Any) {
        if (state.clearFilterFields.isTrue()) {
            changeState {
                clearFilterFields = false
            }
        }
    }

    private fun loadTableData() {
        GlobalScope.launch {
            try {
                val tournamentJob = async {
                    try {
                        RestClient.get(TournamentsRequest())
                    } catch (e: RestClientException) {
                        openFailureDialog(I18n.failure("Tournaments", e.failure))
                        null
                    }
                }
                val disciplinesJob = async {
                    try {
                        RestClient.get(DisciplinesRequest())
                    } catch (e: RestClientException) {
                        openFailureDialog(I18n.failure("Disciplines", e.failure))
                        null
                    }
                }
                val tournaments = tournamentJob.await()
                val disciplines = disciplinesJob.await()

                if (tournaments.isNotNull() && disciplines.isNotNull()) {
                    changeState {
                        listModel = mapTournaments(tournaments, disciplines)
                        this.disciplines = disciplines
                        this.tournaments = tournaments
                    }
                }
            } catch (e: Exception) {
                log.error(e) { "Unable to load tournaments or disciplines" }
            }
        }
    }

    private fun onMediaClassChange(mediaClass: MediaClass) {
        log.debug { "Actual media class: ${mediaClass.name}" }
        changeState {
            this.mediaClass = mediaClass
        }
    }

    private fun clearFilter() {
        changeState {
            filter = TournamentListFilterModel()
            listModel = mapTournaments(state.tournaments ?: emptyList(), state.disciplines ?: emptyList())
            clearFilterFields = true
        }
    }

    private fun applyFilter() {
        state.listModel?.let {
            changeState {
                listModel = getFilteredTournaments(mapTournaments(state.tournaments ?: emptyList(), state.disciplines ?: emptyList()))
            }
        }
    }

    private fun getFilteredTournaments(allTournaments: List<TournamentListDisplayModel>): List<TournamentListDisplayModel> {
        val (textFilter, startDateFilter, endDateFilter) = state.filter
        var filtered = if (textFilter.isNeitherNullOrEmpty()) {
            allTournaments.filter {
                it.name?.toLowerCase()?.contains(textFilter.toLowerCase()).isTrue()
                        || it.disciplineName?.toLowerCase()?.contains(textFilter.toLowerCase()).isTrue()
                        || I18n.default("Platforms.${it.platform}").toLowerCase().contains(textFilter.toLowerCase()).isTrue()
            }
        } else {
            allTournaments
        }

        filtered = if (startDateFilter.isNotNull()) {
            filtered.filter { it.startDate?.isSameOrAfter(startDateFilter).isTrue() }
        } else {
            filtered
        }

        filtered = if (endDateFilter.isNotNull()) {
            filtered.filter { it.endDate?.isSameOrBefore(endDateFilter).isTrue() }
        } else {
            filtered
        }

        return filtered
    }


    private fun mapTournaments(
        tournaments: List<TournamentDto>,
        disciplines: List<DisciplineDto>
    ): List<TournamentListDisplayModel> {
        val disciplineById = disciplines.associateBy { it.id }
        return tournaments.map { tournament ->
            val platforms = tournament.platforms?.filterNotNull()?.joinToString(", ") { I18n.default("Platforms.$it") }
                .toStringOrNoValue()
            val discipline = disciplineById[tournament.discipline]
            TournamentListDisplayModel(
                tournament.id,
                tournament.name,
                discipline?.name,
                tournament.dateStart(),
                tournament.dateEnd(),
                platforms
            )
        }
    }
}
