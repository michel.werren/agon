package ch.sourcemotion.agon.ui.material.textfield

import ch.sourcemotion.agon.extension.isNull
import ch.sourcemotion.agon.ui.material.MatBaseComponent
import ch.sourcemotion.agon.ui.material.icon.MatIcon
import ch.sourcemotion.agon.ui.util.UUIDJS
import ch.sourcemotion.agon.ui.util.asElementId
import kotlinx.html.id
import kotlinx.html.js.onClickFunction
import kotlinx.html.role
import kotlinx.html.tabIndex
import react.RBuilder
import react.RHandler
import react.RProps
import react.RState
import react.dom.i
import kotlin.contracts.ExperimentalContracts

interface MatTextFieldIconCompProps : RProps {
    var icon: MatIcon?
    var clickHandler: (() -> Unit)?
}

/**
 * Helper text component for [ch.sourcemotion.agon.ui.material.MatTextFieldComp]
 */
@ExperimentalContracts
class MatTextFieldIconComp : MatBaseComponent<MatTextFieldIconCompProps, RState, MDCTextFieldIcon>() {

    private val textId = UUIDJS.genV4().asElementId()

    override fun RBuilder.render() {
        i(classes = "material-icons mdc-text-field__icon") {
            ref {
                if (jsComponent.isNull() && it != null) {
                    jsComponent = MDCTextFieldIcon(it)
                }
            }
            attrs {
                id = textId
                role = "button"
                tabIndex = "0"
                props.clickHandler?.let {
                    onClickFunction = {
                        it()
                    }
                }
            }
            props.icon?.let { +it.getIconName() }
        }
    }
}

@ExperimentalContracts
fun RBuilder.matTextFieldIcon(handler: RHandler<MatTextFieldIconCompProps>) =
        child(MatTextFieldIconComp::class, handler)