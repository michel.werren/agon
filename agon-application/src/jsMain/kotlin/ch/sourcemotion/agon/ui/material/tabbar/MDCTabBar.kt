@file:JsModule("@material/tab-bar")

package ch.sourcemotion.agon.ui.material.tabbar

import ch.sourcemotion.agon.ui.material.MDCBase

@JsName("MDCTabBar")
external class MDCTabBar(element: dynamic) : MDCBase {
    fun listen(event: String, listener: (TabActivatedEvent) -> Unit)
    fun activateTab(index: Int)
    fun scrollIntoView(index: Int)
}

external interface TabActivatedEvent {
    val detail: TabActivatedEventDetail
}

external interface TabActivatedEventDetail {
    val index: Int
}