package ch.sourcemotion.agon.config

import ch.sourcemotion.agon.extension.isNotNull
import io.ktor.config.ApplicationConfig
import io.ktor.util.KtorExperimentalAPI
import kotlin.contracts.ExperimentalContracts


class AgonCacheConfig(val disciplinesEviction: Long, val commonEviction: Long, val redisUrl: String) {
    @KtorExperimentalAPI
    @ExperimentalContracts
    companion object {
        fun forApplicationConfig(config: ApplicationConfig) : AgonCacheConfig {
            val disciplinesEviction = getDisciplinesEviction(config)
            val tournamentsEviction = getCommonEviction(config)
            val redisUrl = getRedisUrl(config)
            return AgonCacheConfig(disciplinesEviction, tournamentsEviction, redisUrl)
        }

        private fun getDisciplinesEviction(config: ApplicationConfig) : Long {
            val configValue = config.propertyOrNull("disciplinesEviction")
            return if (configValue.isNotNull()) {
                configValue.getString().toLong()
            } else {
                throw IllegalArgumentException("Disciplines cache configuration is missing. Please provide value on application.conf on ktor.agon.cache.disciplinesEviction")
            }
        }

        private fun getCommonEviction(config: ApplicationConfig) : Long {
            val configValue = config.propertyOrNull("commonEviction")
            return if (configValue.isNotNull()) {
                configValue.getString().toLong()
            } else {
                throw IllegalArgumentException("Common cache eviction configuration is missing. Please provide value on application.conf on ktor.agon.cache.commonEviction")
            }
        }

        private fun getRedisUrl(config: ApplicationConfig) : String {
            val configValue = config.propertyOrNull("redisUrl")
            return if (configValue.isNotNull()) {
                configValue.getString()
            } else {
                throw IllegalArgumentException("Redis url configuration is missing. Please provide value on application.conf on ktor.agon.cache.redisUrl")
            }
        }
    }

    override fun toString(): String {
        return "AgonCacheConfig(disciplinesEviction=$disciplinesEviction, commonEviction=$commonEviction)"
    }
}