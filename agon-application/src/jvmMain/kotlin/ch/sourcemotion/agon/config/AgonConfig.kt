package ch.sourcemotion.agon.config

import ch.sourcemotion.agon.extension.isNotNull
import io.ktor.config.ApplicationConfig
import io.ktor.util.KtorExperimentalAPI
import kotlin.contracts.ExperimentalContracts

/**
 * Whole Agon configuration.
 */
class AgonConfig(val toornamentViewerBaseUrl: String,
                 val toornamentOrganizerBaseUrl: String,
                 val agonAuthConfig: AgonAuthConfig,
                 val agonCacheConfig: AgonCacheConfig) {

    @KtorExperimentalAPI
    @ExperimentalContracts
    companion object {
        fun forApplicationConfig(config: ApplicationConfig) : AgonConfig {
            val ktorAgonConfig = config.config("ktor.agon")
            val toornamentViewerBaseUrl = getToornamentViewerBaseUrl(ktorAgonConfig)
            val toornamentOrganizerBaseUrl = getToornamentOrganizerBaseUrl(ktorAgonConfig)

            val ktorAgonAuthConfig = ktorAgonConfig.config("auth")
            val authConfig = AgonAuthConfig.forApplicationConfig(ktorAgonAuthConfig)

            val ktorAgonCacheConfig = ktorAgonConfig.config("cache")
            val agonCacheConfig = AgonCacheConfig.forApplicationConfig(ktorAgonCacheConfig)

            return AgonConfig(toornamentViewerBaseUrl, toornamentOrganizerBaseUrl, authConfig, agonCacheConfig)
        }

        private fun getToornamentViewerBaseUrl(config: ApplicationConfig) : String {
            val configValue = config.propertyOrNull("toornamentViewerBaseUrl")
            return if (configValue.isNotNull()) {
                configValue.getString()
            } else {
                throw IllegalArgumentException("Toornament viewer base url missing. Please provide value on application.conf on ktor.agon.toornamentViewerBaseUrl")
            }
        }
        private fun getToornamentOrganizerBaseUrl(config: ApplicationConfig) : String {
            val configValue = config.propertyOrNull("toornamentOrganizerBaseUrl")
            return if (configValue.isNotNull()) {
                configValue.getString()
            } else {
                throw IllegalArgumentException("Toornament organizer base url missing. Please provide value on application.conf on ktor.agon.toornamentOrganizerBaseUrl")
            }
        }
    }
}