package ch.sourcemotion.agon.config

import ch.sourcemotion.agon.extension.isNotNull
import io.ktor.config.ApplicationConfig
import io.ktor.util.KtorExperimentalAPI
import kotlin.contracts.ExperimentalContracts


/**
 * Agon authentication configuration.
 */
class AgonAuthConfig(val oAuthBaseUrl: String, val clientId: String, val clientSecret: String,
                     val grantType: String, val apiKey: String) {

    @ExperimentalContracts
    @KtorExperimentalAPI
    companion object {
        fun forApplicationConfig(config: ApplicationConfig) : AgonAuthConfig {
            val oAuthBaseUrl = getOAuthBaseUrl(config)
            val clientId = getClientId(config)
            val clientSecret = getClientSecret(config)
            val grantType = getGrantType(config)
            val apiKey = getApiKey(config)

            return AgonAuthConfig(oAuthBaseUrl, clientId, clientSecret, grantType, apiKey)
        }

        private fun getOAuthBaseUrl(config: ApplicationConfig) : String {
            val configValue = config.propertyOrNull("oAuthBaseUrl")
            return if (configValue.isNotNull()) {
                configValue.getString()
            } else {
                throw IllegalArgumentException("OAuth base url is missing. Please provide value on application.conf on ktor.agon.auth.oAuthBaseUrl")
            }
        }

        private fun getClientId(config: ApplicationConfig) : String {
            val configValue = config.propertyOrNull("clientId")
            return if (configValue.isNotNull()) {
                configValue.getString()
            } else {
                throw IllegalArgumentException("Client id is missing. Please provide value on application.conf on ktor.agon.auth.clientId")
            }
        }

        private fun getClientSecret(config: ApplicationConfig) : String {
            val configValue = config.propertyOrNull("clientSecret")
            return if (configValue.isNotNull()) {
                configValue.getString()
            } else {
                throw IllegalArgumentException("Client secret is missing. Please provide value on application.conf on ktor.agon.auth.clientSecret")
            }
        }

        private fun getGrantType(config: ApplicationConfig) : String {
            val configValue = config.propertyOrNull("grantType")
            return if (configValue.isNotNull()) {
                configValue.getString()
            } else {
                throw IllegalArgumentException("Grant type is missing. Please provide value on application.conf on ktor.agon.auth.grantType")
            }
        }

        private fun getApiKey(config: ApplicationConfig) : String {
            val configValue = config.propertyOrNull("apiKey")
            return if (configValue.isNotNull()) {
                configValue.getString()
            } else {
                throw IllegalArgumentException("Grant type is missing. Please provide value on application.conf on ktor.agon.auth.apiKey")
            }
        }
    }
}