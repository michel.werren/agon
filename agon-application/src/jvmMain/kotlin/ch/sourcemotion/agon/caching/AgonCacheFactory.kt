package ch.sourcemotion.agon.caching

import ch.sourcemotion.agon.config.AgonCacheConfig
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import io.lettuce.core.RedisClient
import io.lettuce.core.api.StatefulRedisConnection
import io.lettuce.core.codec.Utf8StringCodec
import mu.KotlinLogging
import java.io.Closeable
import kotlin.contracts.ExperimentalContracts


@ExperimentalContracts
class AgonCacheFactory private constructor(@PublishedApi internal val connection: StatefulRedisConnection<String, String>,
                                           @PublishedApi internal val objectMapper: ObjectMapper) : Closeable {

    companion object {
        private val log = KotlinLogging.logger {}

        fun build(cacheConfig: AgonCacheConfig, objectMapper: ObjectMapper) : AgonCacheFactory {
            val redisClient = RedisClient.create(cacheConfig.redisUrl)
            val connection = redisClient.connect(Utf8StringCodec())

            log.info { "Redis connection established to ${cacheConfig.redisUrl}" }

            return AgonCacheFactory(connection, objectMapper)
        }
    }

    inline fun <reified K: Any, reified V : Any> getCache(expirationSeconds: Long = 30): Cache<K, V> {
        val typeRef = object : TypeReference<V>() {}
        return Cache(typeRef, objectMapper, connection.async(), expirationSeconds)
    }

    override fun close() {
        log.info { "Redis connection closed" }
        connection.close()
    }
}