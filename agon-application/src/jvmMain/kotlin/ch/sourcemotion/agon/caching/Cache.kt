package ch.sourcemotion.agon.caching

import ch.sourcemotion.agon.extension.isNeitherNullOrEmpty
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import io.lettuce.core.api.async.RedisAsyncCommands
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import java.util.concurrent.TimeUnit
import kotlin.contracts.ExperimentalContracts


@ExperimentalContracts
class Cache<K : Any, V : Any>(private val valueTypeRef: TypeReference<V>,
                              private val objectMapper: ObjectMapper,
                              private val commands: RedisAsyncCommands<String, String>,
                              private val defaultExpirationSeconds: Long) {
    suspend fun get(key: K): V? {
        val future = commands.get(key.toString())
        return suspendCancellableCoroutine { comp ->
            try {
                val valueFromCache = future.get(10, TimeUnit.SECONDS)
                if (valueFromCache.isNeitherNullOrEmpty()) {
                    comp.resumeWith(Result.success(objectMapper.readValue(valueFromCache, valueTypeRef)))
                } else {
                    comp.resumeWith(Result.success(null))
                }
            } catch (e: Exception) {
                comp.resumeWith(Result.failure(e))
            }
        }
    }

    suspend fun get(key: K, expirationSeconds: Long = this.defaultExpirationSeconds, block: suspend () -> V): V {
        val future = commands.get(key.toString())
        return suspendCancellableCoroutine { comp ->
            try {
                val valueFromCache = future.get(10, TimeUnit.SECONDS)
                if (valueFromCache.isNeitherNullOrEmpty()) {
                    comp.resumeWith(Result.success(objectMapper.readValue(valueFromCache, valueTypeRef)))
                } else {
                    GlobalScope.launch {
                        try {
                            val newValue = block()
                            put(key, newValue, expirationSeconds)
                            comp.resumeWith(Result.success(newValue))
                        } catch (e: Exception) {
                            comp.resumeWith(Result.failure(e))
                        }
                    }
                }
            } catch (e: Exception) {
                comp.resumeWith(Result.failure(e))
            }
        }
    }

    suspend fun put(key: K, value: V, expirationSeconds: Long = defaultExpirationSeconds) {
        val serializedKey = key.toString()
        suspendCancellableCoroutine<Unit> {
            val serializesValue = objectMapper.writeValueAsString(value)
            val future = commands.set(serializedKey, serializesValue)
            if (future.await(10, TimeUnit.SECONDS)) {
                if (commands.expire(serializedKey, expirationSeconds).await(10, TimeUnit.SECONDS)) {
                    it.resumeWith(Result.success(Unit))
                } else {
                    it.resumeWith(Result.failure(IllegalStateException("Unable to set expiration for key $serializedKey for cache entry in Redis because: ${future.error}")))
                }
            } else {
                it.resumeWith(Result.failure(IllegalStateException("Unable to put entry for key $serializedKey into Redis because: ${future.error}")))
            }
        }
    }
}