package ch.sourcemotion.agon.module

import ch.sourcemotion.agon.AgonHeader
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.*
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Locations
import io.ktor.util.KtorExperimentalAPI
import mu.KotlinLogging
import org.slf4j.event.Level

private val log = KotlinLogging.logger("InstallationModule")

@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
fun Application.installationModule() {
    if (isProd) {
        log.info { "Start Agon for production" }
    } else {
        log.info { "Start Agon for development" }
    }

    log.info { "Agon runs with a parallelism of ${Runtime.getRuntime().availableProcessors()}" }

    if (isDev) {
        install(CORS) {
            method(HttpMethod.Options)
            method(HttpMethod.Put)
            method(HttpMethod.Delete)
            method(HttpMethod.Patch)
            header(HttpHeaders.Authorization)
            allowCredentials = true
            // To redirect requests which not come from the application will be redirected to bas path
            headers.add(AgonHeader.AGON_REST_CLIENT_NAME)
            anyHost()
        }
        log.info { "CORS installed" }
    }

    install(CallLogging) {
        level = Level.DEBUG

    }
    install(Locations)
    install(Compression) {
        gzip {
            priority = 1.0
        }
        deflate {
            priority = 10.0
            minimumSize(1024)
        }
    }
}

@KtorExperimentalAPI
val Application.envKind get() = environment.config.property("ktor.environment").getString()
@KtorExperimentalAPI
val Application.isDev get() = envKind == "dev"
@KtorExperimentalAPI
val Application.isProd get() = envKind != "dev"
