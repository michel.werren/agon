package ch.sourcemotion.agon.module

import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.AutoHeadResponse
import io.ktor.http.content.default
import io.ktor.http.content.files
import io.ktor.http.content.static
import io.ktor.http.content.staticRootFolder
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.routing.routing
import io.ktor.util.KtorExperimentalAPI
import mu.KotlinLogging
import java.io.File
import kotlin.contracts.ExperimentalContracts

private val log = KotlinLogging.logger("StaticContentModule")

@ExperimentalContracts
@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
fun Application.staticContentModule() {
    if (isProd) {
        log.info { "Static content will be delivered" }
        install(AutoHeadResponse)
        routing {
            static {
                staticRootFolder = File("/agon-distribution/page")
                static("i18n") {
                    files("i18n")
                }
                static("image") {
                    files("image")
                }
                default("index.html")
                files(".")
            }
        }
    }
}