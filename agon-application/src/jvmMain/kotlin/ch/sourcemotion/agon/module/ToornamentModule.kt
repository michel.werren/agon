package ch.sourcemotion.agon.module

import ch.sourcemotion.agon.caching.AgonCacheFactory
import ch.sourcemotion.agon.config.AgonConfig
import ch.sourcemotion.agon.resource.v2.*
import ch.sourcemotion.agon.toornament.ToornamentClient
import ch.sourcemotion.agon.toornament.auth.BearerTokenResource
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.ktor.application.Application
import io.ktor.application.ApplicationStopping
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.routing.routing
import io.ktor.util.KtorExperimentalAPI
import mu.KotlinLogging
import kotlin.contracts.ExperimentalContracts

private val log = KotlinLogging.logger("TournamentModule")

@ExperimentalUnsignedTypes
@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
@ExperimentalContracts
fun Application.toornamentModule() {
    val agonConfig = AgonConfig.forApplicationConfig(environment.config)

    log.info { "${agonConfig.agonCacheConfig}" }

    val cacheFactory = AgonCacheFactory.build(agonConfig.agonCacheConfig, jacksonObjectMapper())


    val bearerTokenResource = BearerTokenResource.build(agonConfig.agonAuthConfig, cacheFactory.getCache())

    environment.monitor.subscribe(ApplicationStopping) {
        cacheFactory.close()
    }

    val toornamentClient = ToornamentClient.build(this, agonConfig.agonAuthConfig.apiKey,
            bearerTokenResource, agonConfig.toornamentViewerBaseUrl, agonConfig.toornamentOrganizerBaseUrl,
            cacheFactory, agonConfig.agonCacheConfig)

    routing {
        DisciplineResource(this, toornamentClient)
        TournamentResource(this, toornamentClient)
        ParticipantResource(this, toornamentClient)
        MatchResource(this, toornamentClient)
        StageResource(this, toornamentClient)
        GroupResource(this, toornamentClient)
        RoundResource(this, toornamentClient)
    }
}