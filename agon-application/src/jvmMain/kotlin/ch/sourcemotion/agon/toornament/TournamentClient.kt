package ch.sourcemotion.agon.toornament

import ch.sourcemotion.agon.caching.Cache
import ch.sourcemotion.agon.resource.v2.request.TournamentLocation
import io.ktor.application.Application
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import mu.KotlinLogging
import kotlin.contracts.ExperimentalContracts


@ExperimentalContracts
@KtorExperimentalLocationsAPI
class TournamentClient(private val application: Application,
                       private val client: HttpClient,
                       private val organizerBaseUrl: String,
                       private val tournamentsCache: Cache<TournamentLocation.GetTournamentsLocation, String>,
                       private val tournamentCache: Cache<TournamentLocation.GetTournamentLocation, String>) : PaginationClient() {

    companion object {
        private val log = KotlinLogging.logger {}
    }

    suspend fun tournamentsGet(location: TournamentLocation.GetTournamentsLocation): String {
        return tournamentsCache.get(location) {
            getPaginated("$organizerBaseUrl${application.locations.href(location)}", client, "tournaments")
        }
    }

    suspend fun tournamentGet(location: TournamentLocation.GetTournamentLocation): String {
        return tournamentCache.get(location) {
            val href = application.locations.href(location)
            log.debug { "Get tournament $href from Toornament" }
            client.get("$organizerBaseUrl$href")
        }
    }
}