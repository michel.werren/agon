package ch.sourcemotion.agon.toornament

import ch.sourcemotion.agon.caching.Cache
import ch.sourcemotion.agon.resource.v2.request.GroupLocation
import io.ktor.application.Application
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import mu.KotlinLogging
import kotlin.contracts.ExperimentalContracts

/**
 * Client to get group information from Toornament.
 */
@ExperimentalContracts
@KtorExperimentalLocationsAPI
class GroupClient(private val application: Application,
                  private val client: HttpClient,
                  private val baseUrl: String,
                  private val groupsCache: Cache<GroupLocation.GetGroupsLocation, String>,
                  private val groupCache: Cache<GroupLocation.GetGroupLocation, String>) : PaginationClient() {

    companion object {
        private val log = KotlinLogging.logger {}
    }

    suspend fun groupsGet(location: GroupLocation.GetGroupsLocation): String {
        return groupsCache.get(location) {
            getPaginated("$baseUrl${application.locations.href(location)}", client, "groups")
        }
    }

    suspend fun groupGet(location: GroupLocation.GetGroupLocation): String {
        return groupCache.get(location) {
            val href = application.locations.href(location)
            log.debug { "Get Group $href from Toornament" }
            client.get("$baseUrl$href")
        }
    }
}