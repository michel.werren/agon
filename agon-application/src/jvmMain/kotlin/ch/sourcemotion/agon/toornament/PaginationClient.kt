package ch.sourcemotion.agon.toornament

import ch.sourcemotion.agon.extension.isFalse
import ch.sourcemotion.agon.toornament.PaginationPartitioner.Companion.mergeResultArrays
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.response.HttpResponse
import io.ktor.client.response.readText
import io.ktor.locations.KtorExperimentalLocationsAPI
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import mu.KotlinLogging
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException


@KtorExperimentalLocationsAPI
abstract class PaginationClient {

    companion object {
        private val log = KotlinLogging.logger {}
    }

    /**
     * Get element recursively from Toornament.
     */
    protected suspend fun getPaginated(path: String,
                                       client: HttpClient,
                                       rangeKey: String,
                                       rangeStep: Int = 50): String {
        log.debug { "Start pagination request for $rangeKey" }
        val partitionResults = ArrayList<String>()

        val startRange = Range.firstRange(rangeStep)

        val (paginationInfo, firstRequestResult) = doFirstRequest(client, path, rangeKey, startRange)
        partitionResults.add(firstRequestResult)

        if (paginationInfo.hasPendingElements) {
            val nextRange = startRange.next(rangeStep)
            val partitionRanges = PaginationPartitioner.calculatePartitions(paginationInfo, rangeStep, nextRange)

            log.debug("Ranges to get for $rangeKey after start range $startRange -> $partitionRanges with max elements ${paginationInfo.maxElements}")

            val subsequentResults = suspendCancellableCoroutine<List<String>> { cont ->
                GlobalScope.launch {
                    val partitionJobs = partitionRanges.map { range ->

                        // We do it parallel
                        async {
                            val (from, to) = range
                            try {
                                client.get<String>(path) {
                                    header("Range", "$rangeKey=$from-$to")
                                }
                            } catch (e: Exception) {
                                cont.resumeWithException(Exception("Failed to get $range of $rangeKey from Toornament", e))
                                null
                            }
                        }
                    }
                    log.debug("${partitionJobs.size} pagination jobs pending for $rangeKey")

                    val results: List<String?> = partitionJobs.map { job ->
                        log.debug("Pagination job finished for $rangeKey")
                        job.await()
                    }
                    cont.resume(results.filterNotNull())
                }
            }
            partitionResults.addAll(subsequentResults)
        }


        return if (partitionResults.size > 1) {
            log.debug("Results for all $rangeKey ranges received")
            mergeResultArrays(partitionResults)
        } else {
            log.debug("Results for $rangeKey single range received")
            partitionResults.first()
        }
    }

    /**
     * Executes the first pagination request.
     *
     * @return [PaginationInfo] to configure subsequent requests and the response value of this request.
     */
    private suspend fun doFirstRequest(client: HttpClient, path: String, rangeKey: String, range: Range): Pair<PaginationInfo, String> {
        val response = client.get<HttpResponse>(path) {
            headers.append("Range", "$rangeKey=${range.from}-${range.to}")
        }
        val paginationInfo = PaginationInfo.buildFromResponse(response)
        return Pair(paginationInfo, response.readText(Charsets.UTF_8))
    }
}

data class PaginationInfo(val maxElements: Int, val hasPendingElements: Boolean) {
    companion object {
        fun buildFromResponse(response: HttpResponse): PaginationInfo {
            val rangeInfo = response.headers["Content-Range"]?.trim()
            if (rangeInfo.isNullOrBlank()) {
                return PaginationInfo(0, false)
            }

            val maxElements = parseMaxElements(rangeInfo)
            return if (maxElements > 0) {
                // We have to increase the range for check because it starts from 0
                PaginationInfo(maxElements, maxElements > parseRangeTo(rangeInfo).plus(1))
            } else {
                PaginationInfo(maxElements, false)
            }
        }

        private fun parseRangeTo(rangeInfo: String): Int {
            return try {
                rangeInfo.substring(rangeInfo.lastIndexOf("-") + 1, rangeInfo.lastIndexOf("/")).toInt()
            } catch (e: Exception) {
                throw Exception("Unable to parse range to from Content-Range $rangeInfo")
            }
        }

        private fun parseMaxElements(rangeInfo: String): Int {
            return try {
                rangeInfo.substring(rangeInfo.lastIndexOf("/") + 1, rangeInfo.length).toInt()
            } catch (e: Exception) {
                throw Exception("Unable to parse max elements from Content-Range $rangeInfo")
            }
        }
    }
}

/**
 * Partitioner to calculate the pending request to do.
 */
class PaginationPartitioner {
    companion object {
        /**
         * Calculates the partitions for further pending pagination requests
         */
        fun calculatePartitions(paginationInfo: PaginationInfo, rangeStep: Int, range: Range): List<Range> {
            val (maxElements, hasPendingElements) = paginationInfo

            if (hasPendingElements.isFalse()) {
                return emptyList()
            }

            val result = ArrayList<Range>()

            var nextRange = range
            while (nextRange.isValid(maxElements)) {
                result.add(nextRange)
                nextRange = nextRange.next(rangeStep)
            }
            return result
        }

        /**
         * Merges the results of which each is an json array.
         */
        fun mergeResultArrays(result: List<String>): String {
            return result.mapIndexed { index, value ->
                when (index) {
                    0 -> value.substring(0, value.length - 1)
                    result.lastIndex -> value.substring(1, value.length)
                    else -> value.substring(1, value.length - 1)
                }
            }.joinToString(",")
        }
    }
}

/**
 * Range of a pagination request.
 */
data class Range(val from: Int, val to: Int) {

    companion object {
        fun firstRange(rangeStep: Int) = Range(0, rangeStep.minus(1))
    }

    /**
     * @return  True when this range starts below max elements
     */
    fun isValid(maxElements: Int) = from < maxElements

    /**
     * @return Next range from this against the given step.
     */
    fun next(rangeStep: Int) = Range(from.plus(rangeStep), to.plus(rangeStep))
}
