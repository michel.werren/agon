package ch.sourcemotion.agon.toornament.auth

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Bearer token, received from Toornament.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
data class BearerToken(@JsonProperty("access_token") val token: String,
                       @JsonProperty("expires_in") val expiresIn: Long,
                       @JsonProperty("token_type") val tokenType: String,
                       @JsonProperty("scope") val scope: String? = null)