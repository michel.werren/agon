package ch.sourcemotion.agon.toornament

import ch.sourcemotion.agon.caching.AgonCacheFactory
import ch.sourcemotion.agon.config.AgonCacheConfig
import ch.sourcemotion.agon.toornament.auth.BearerTokenResource
import io.ktor.application.Application
import io.ktor.client.HttpClient
import io.ktor.client.features.defaultRequest
import io.ktor.client.request.header
import io.ktor.http.ContentType
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.util.KtorExperimentalAPI
import mu.KotlinLogging
import kotlin.contracts.ExperimentalContracts


/**
 * Http client, used to access Toornament REST API. Provides automated authentication with bearer token and api key
 */
@ExperimentalContracts
@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
class ToornamentClient private constructor(private val application: Application,
                                           private val httpClient: HttpClient,
                                           private val toornamentViewerBaseUrl: String,
                                           private val toornamentOrganizerBaseUrl: String,
                                           private val cacheFactory: AgonCacheFactory,
                                           private val cacheConfig: AgonCacheConfig) {

    @ExperimentalContracts
    companion object {
        private val log = KotlinLogging.logger {}

        fun build(application: Application,
                  apiKey: String,
                  tokenResource: BearerTokenResource,
                  toornamentViewerBaseUrl: String,
                  toornamentOrganizerBaseUrl: String,
                  cacheFactory: AgonCacheFactory,
                  cacheConfig: AgonCacheConfig): ToornamentClient {

            log.info { "Build Toornament client ..." }

            val client = HttpClient {
                defaultRequest {
                    log.debug { "Auth information applied" }
                    header("Accept", ContentType.Application.Json.contentType)
                    header("X-Api-Key", apiKey)
                    header("Authorization", tokenResource.getCurrentToken())
                }
            }

            log.info { "Toornament client will connect to viewer $toornamentViewerBaseUrl and organizer $toornamentOrganizerBaseUrl" }

            return ToornamentClient(application, client, toornamentViewerBaseUrl, toornamentOrganizerBaseUrl, cacheFactory, cacheConfig)
        }
    }


    fun getDisciplineClient() = DisciplineClient(application, httpClient, toornamentViewerBaseUrl,
            cacheFactory.getCache(cacheConfig.disciplinesEviction))
    fun getTournamentClient() = TournamentClient(application, httpClient, toornamentOrganizerBaseUrl,
            cacheFactory.getCache(cacheConfig.commonEviction), cacheFactory.getCache(cacheConfig.commonEviction))
    fun getParticipantClient() = ParticipantClient(application, httpClient, toornamentViewerBaseUrl,
            cacheFactory.getCache(cacheConfig.commonEviction))
    fun getMatchClient() = MatchClient(application, httpClient, toornamentViewerBaseUrl,
            cacheFactory.getCache(cacheConfig.commonEviction))
    fun getGroupClient() = GroupClient(application, httpClient, toornamentViewerBaseUrl,
            cacheFactory.getCache(cacheConfig.commonEviction), cacheFactory.getCache(cacheConfig.commonEviction))
    fun getStageClient() = StageClient(application, httpClient, toornamentViewerBaseUrl,
            cacheFactory.getCache(cacheConfig.commonEviction), cacheFactory.getCache(cacheConfig.commonEviction),
            cacheFactory.getCache(cacheConfig.commonEviction))
    fun getRoundClient() = RoundClient(application, httpClient, toornamentViewerBaseUrl,
            cacheFactory.getCache(cacheConfig.commonEviction),
            cacheFactory.getCache(cacheConfig.commonEviction))
}