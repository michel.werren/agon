package ch.sourcemotion.agon.toornament.auth

import ch.sourcemotion.agon.caching.Cache
import ch.sourcemotion.agon.config.AgonAuthConfig
import ch.sourcemotion.agon.extension.isFalse
import ch.sourcemotion.agon.extension.isNotNull
import io.ktor.client.HttpClient
import io.ktor.client.features.json.JacksonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.accept
import io.ktor.client.request.forms.FormDataContent
import io.ktor.client.request.post
import io.ktor.client.request.url
import io.ktor.http.ContentType.Application.Json
import io.ktor.http.Parameters
import io.ktor.util.KtorExperimentalAPI
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import java.util.concurrent.locks.ReentrantLock
import kotlin.contracts.ExperimentalContracts


@ExperimentalContracts
@KtorExperimentalAPI
class BearerTokenResource private constructor(private val authClient: HttpClient, private val oAuthBaseUrl: String,
                                              private val clientId: String, private val clientSecret: String,
                                              private val grantType: String,
                                              private val tokenCache: Cache<String, String>) {

    @ExperimentalContracts
    @KtorExperimentalAPI
    companion object {
        private const val CACHE_KEY = "BEARER"
        private val log = KotlinLogging.logger {}

        fun build(authConfig: AgonAuthConfig, tokenCache: Cache<String, String>): BearerTokenResource {
            val client = HttpClient {
                install(JsonFeature) {
                    serializer = JacksonSerializer()
                }
            }

            val bearerTokenResource = BearerTokenResource(client, authConfig.oAuthBaseUrl, authConfig.clientId,
                    authConfig.clientSecret, authConfig.grantType, tokenCache)
            log.info { "Bearer token resource started" }
            return bearerTokenResource
        }
    }

    /**
     * Lock to avoid multiple token leases on renew
     */
    private val renewLock = ReentrantLock(true)


    private suspend fun leaseNewToken(): BearerToken {
        return authClient.post {
            url("$oAuthBaseUrl/oauth/v2/token")
            accept(Json)
            body = FormDataContent(Parameters.build {
                append("grant_type", grantType)
                append("client_id", clientId)
                append("client_secret", clientSecret)
                append("scope", "organizer:view")
            })
        }
    }

    fun getCurrentToken(): String {
        return runBlocking {
            try {
                val existingBearerValue = tokenCache.get(CACHE_KEY)
                if (existingBearerValue.isNotNull()) {
                    existingBearerValue
                } else {
                    // When the renew process is locked from another Thread, retry to get the cached value when the lock is released
                    if (renewLock.isHeldByCurrentThread.isFalse() && renewLock.isLocked) {
                        renewLock.lock()
                        // Retry to avoid multiple leases
                        return@runBlocking getCurrentToken()
                        // When the renew process is not locked, get the lock and do renew.
                    } else if (renewLock.isHeldByCurrentThread.isFalse() && renewLock.isLocked.isFalse()) {
                        renewLock.lock()
                    }
                    log.info { "Lease new bearer" }
                    val newBearer = leaseNewToken()
                    tokenCache.put(CACHE_KEY, newBearer.token, newBearer.expiresIn)
                    newBearer.token
                }
            } finally {
                if (renewLock.isLocked && renewLock.isHeldByCurrentThread) {
                    renewLock.unlock()
                }
            }
        }
    }
}