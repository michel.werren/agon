package ch.sourcemotion.agon.toornament

import ch.sourcemotion.agon.caching.Cache
import ch.sourcemotion.agon.resource.v2.request.RoundLocation
import io.ktor.application.Application
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import mu.KotlinLogging
import kotlin.contracts.ExperimentalContracts

/**
 * Client to get group information from Toornament.
 */
@ExperimentalContracts
@KtorExperimentalLocationsAPI
class RoundClient(private val application: Application,
                  private val client: HttpClient,
                  private val baseUrl: String,
                  private val roundsCache: Cache<RoundLocation.GetRoundsLocation, String>,
                  private val roundCache: Cache<RoundLocation.GetRoundLocation, String>) : PaginationClient() {

    companion object {
        private val log = KotlinLogging.logger {}
    }

    suspend fun roundsGet(location: RoundLocation.GetRoundsLocation): String {
        return roundsCache.get(location) {
            getPaginated("$baseUrl${application.locations.href(location)}", client, "rounds")
        }
    }

    suspend fun roundGet(location: RoundLocation.GetRoundLocation): String {
        return roundCache.get(location) {
            val url = "$baseUrl${application.locations.href(location)}"
            log.debug { "Get Round $url from Toornament" }
            client.get(url)
        }
    }
}