package ch.sourcemotion.agon.toornament

import ch.sourcemotion.agon.caching.Cache
import ch.sourcemotion.agon.resource.v2.request.MatchLocation
import io.ktor.application.Application
import io.ktor.client.HttpClient
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import kotlin.contracts.ExperimentalContracts

/**
 * Client to get match information from Toornament.
 */
@ExperimentalContracts
@KtorExperimentalLocationsAPI
class MatchClient(private val application: Application,
                  private val client: HttpClient,
                  private val baseUrl: String,
                  private val cache: Cache<MatchLocation.GetMatchesLocation, String>) : PaginationClient() {

    suspend fun matchesGet(location: MatchLocation.GetMatchesLocation): String {
        return cache.get(location) {
            getPaginated("$baseUrl${application.locations.href(location)}", client, "matches", 128)
        }
    }
}