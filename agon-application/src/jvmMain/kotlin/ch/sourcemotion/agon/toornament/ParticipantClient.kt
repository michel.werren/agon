package ch.sourcemotion.agon.toornament

import ch.sourcemotion.agon.caching.Cache
import ch.sourcemotion.agon.resource.v2.request.ParticipantLocation
import io.ktor.application.Application
import io.ktor.client.HttpClient
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import kotlin.contracts.ExperimentalContracts

/**
 * Client to get participant information from Toornament.
 */
@ExperimentalContracts
@KtorExperimentalLocationsAPI
class ParticipantClient(private val application: Application,
                        private val client: HttpClient,
                        private val baseUrl: String,
                        private val participantCache: Cache<ParticipantLocation.GetParticipantsLocation, String>) : PaginationClient() {

    suspend fun participantsGet(location: ParticipantLocation.GetParticipantsLocation): String {
        return participantCache.get(location) {
            getPaginated("$baseUrl${application.locations.href(location)}", client, "participants")
        }
    }
}