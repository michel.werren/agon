package ch.sourcemotion.agon.toornament

import ch.sourcemotion.agon.caching.Cache
import ch.sourcemotion.agon.resource.v2.request.StageLocation
import io.ktor.application.Application
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import kotlin.contracts.ExperimentalContracts

/**
 * Client to get stage information from Toornament.
 */
@ExperimentalContracts
@KtorExperimentalLocationsAPI
class StageClient(private val application: Application,
                  private val client: HttpClient,
                  private val baseUrl: String,
                  private val stagesCache: Cache<StageLocation.GetStagesLocation, String>,
                  private val stageCache: Cache<StageLocation.GetStageLocation, String>,
                  private val rankingsCache: Cache<StageLocation.GetRankingsLocation, String>) : PaginationClient() {

    suspend fun stagesGet(location: StageLocation.GetStagesLocation): String {
        return stagesCache.get(location) {
            client.get("$baseUrl${application.locations.href(location)}")
        }
    }

    suspend fun stageGet(location: StageLocation.GetStageLocation): String {
        return stageCache.get(location) {
            val href = application.locations.href(location)
            client.get("$baseUrl$href")
        }
    }

    suspend fun rankingsGet(location: StageLocation.GetRankingsLocation): String {
        return rankingsCache.get(location) {
            getPaginated("$baseUrl${application.locations.href(location)}", client, "items")
        }
    }
}