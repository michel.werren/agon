package ch.sourcemotion.agon.toornament

import ch.sourcemotion.agon.caching.Cache
import ch.sourcemotion.agon.resource.v2.request.DiscliplineLocation
import io.ktor.application.Application
import io.ktor.client.HttpClient
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import mu.KotlinLogging
import kotlin.contracts.ExperimentalContracts


@ExperimentalContracts
@KtorExperimentalLocationsAPI
class DisciplineClient(private val application: Application,
                       private val client: HttpClient,
                       private val baseUrl: String,
                       private val disciplinesCache: Cache<String, String>) : PaginationClient() {

    companion object {
        private const val CACHE_KEY = "DISCIPLINES"
        private val log = KotlinLogging.logger {}
    }

    suspend fun disciplinesGet(location: DiscliplineLocation.GetDisciplinesLocation): String {
        return disciplinesCache.get(CACHE_KEY) {
            val paginated = getPaginated("$baseUrl${application.locations.href(location)}", client, "disciplines")
            log.debug { "Disciplines received" }
            paginated
        }
    }
}