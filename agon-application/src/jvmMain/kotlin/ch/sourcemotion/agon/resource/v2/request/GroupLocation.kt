package ch.sourcemotion.agon.resource.v2.request

import ch.sourcemotion.agon.TournamentPath
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location

@KtorExperimentalLocationsAPI
@Location(TournamentPath.ROOT_PATH)
class GroupLocation {
    @Location("/{tournamentId}/groups")
    data class GetGroupsLocation(val tournamentId: String, val stage_ids: String)

    @Location("/{tournamentId}/groups/{groupId}")
    data class GetGroupLocation(val tournamentId: String, val groupId: String)
}