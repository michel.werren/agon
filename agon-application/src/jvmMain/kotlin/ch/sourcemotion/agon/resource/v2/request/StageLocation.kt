package ch.sourcemotion.agon.resource.v2.request

import ch.sourcemotion.agon.TournamentPath
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location

@KtorExperimentalLocationsAPI
@Location(TournamentPath.ROOT_PATH)
class StageLocation {
    @Location("/{tournamentId}/stages")
    data class GetStagesLocation(val tournamentId: String)

    @Location("/{tournamentId}/stages/{stageId}")
    data class GetStageLocation(val tournamentId: String, val stageId: String)

    @Location("/{tournamentId}/stages/{stageId}/ranking-items")
    data class GetRankingsLocation(val tournamentId: String, val stageId: String)
}