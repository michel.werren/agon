package ch.sourcemotion.agon.resource.v2

import io.ktor.application.ApplicationCall
import io.ktor.client.features.BadResponseStatusException
import io.ktor.http.HttpStatusCode
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.response.respond
import mu.KotlinLogging


@KtorExperimentalLocationsAPI
abstract class AbstractResource {

    companion object {
        private val log = KotlinLogging.logger {}
    }

    /**
     * Convenient function to do a request against Toornament with central failure handling
     */
    protected suspend fun doCatchedToornamentRequest(call: ApplicationCall, block: suspend () -> Unit) {
        try {
            block()
        } catch (e: BadResponseStatusException) {
            log.error(e) { "Request failed to Toornament" }
            call.respond(e.statusCode, e.message ?: "")
        } catch (e: Exception) {
            log.error(e) { "Request failed to Toornament with an unexpected failure" }
            call.respond(HttpStatusCode.InternalServerError)
        }
    }
}