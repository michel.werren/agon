package ch.sourcemotion.agon.resource.v2.request

import ch.sourcemotion.agon.TournamentPath
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location

@KtorExperimentalLocationsAPI
@Location(TournamentPath.ROOT_PATH)
class ParticipantLocation {
    @Location("/{tournamentId}/participants")
    data class GetParticipantsLocation(val tournamentId: String)

    @Location("/{tournamentId}/participants/{participantId}")
    data class GetParticipantDetailLocation(val tournamentId: String, val participantId: String)
}