package ch.sourcemotion.agon.resource.v2

import ch.sourcemotion.agon.resource.v2.request.MatchLocation
import ch.sourcemotion.agon.toornament.ToornamentClient
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.get
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.util.KtorExperimentalAPI
import kotlin.contracts.ExperimentalContracts

/**
 * Rest resource for requests from UI side.
 */
@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
@ExperimentalContracts
class MatchResource(routing: Routing, toornamentClient: ToornamentClient) : AbstractResource() {

    private val client = toornamentClient.getMatchClient()

    init {
        routing.matchesGet()
    }

    private fun Routing.matchesGet() {
        get<MatchLocation.GetMatchesLocation> { location ->
            doCatchedToornamentRequest(call) {
                call.respondText(contentType = ContentType.Application.Json, provider = { client.matchesGet(location) })
            }
        }
    }
}

