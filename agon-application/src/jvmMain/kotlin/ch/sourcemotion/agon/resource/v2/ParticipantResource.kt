package ch.sourcemotion.agon.resource.v2

import ch.sourcemotion.agon.resource.v2.request.ParticipantLocation
import ch.sourcemotion.agon.toornament.ToornamentClient
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.get
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.util.KtorExperimentalAPI
import mu.KotlinLogging
import kotlin.contracts.ExperimentalContracts

/**
 * Rest resource for requests from UI side.
 */
@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
@ExperimentalContracts
class ParticipantResource(routing: Routing, toornamentClient: ToornamentClient) : AbstractResource() {

    companion object {
        private val log = KotlinLogging.logger {}
    }

    private val client = toornamentClient.getParticipantClient()

    init {
        routing.participantsGet()
    }

    private fun Routing.participantsGet() {
        get<ParticipantLocation.GetParticipantsLocation> { location ->
            doCatchedToornamentRequest(call) {
                call.respondText(contentType = ContentType.Application.Json, provider = { client.participantsGet(location) })
            }
        }
    }
}

