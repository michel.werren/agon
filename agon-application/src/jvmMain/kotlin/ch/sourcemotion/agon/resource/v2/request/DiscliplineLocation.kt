package ch.sourcemotion.agon.resource.v2.request

import ch.sourcemotion.agon.DisciplinePath
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location

@KtorExperimentalLocationsAPI
@Location(DisciplinePath.ROOT_PATH)
class DiscliplineLocation {
    @Location("/")
    class GetDisciplinesLocation
}