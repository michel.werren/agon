package ch.sourcemotion.agon.resource.v2.request

import ch.sourcemotion.agon.TournamentPath
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location

@KtorExperimentalLocationsAPI
@Location(TournamentPath.ROOT_PATH)
class TournamentLocation {
    @Location("")
    data class GetTournamentsLocation(
        val disciplines: String? = null, val statuses: String? = null, val scheduled_before: String? = null,
        val scheduled_after: String? = null, val is_online: Boolean? = null, val sort: String? = null
    )

    @Location("/{id}")
    data class GetTournamentLocation(val id: String)
}