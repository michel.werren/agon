package ch.sourcemotion.agon.resource.v2

import ch.sourcemotion.agon.AgonHeader
import ch.sourcemotion.agon.resource.v2.request.TournamentLocation
import ch.sourcemotion.agon.toornament.ToornamentClient
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.get
import io.ktor.response.respondRedirect
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.util.KtorExperimentalAPI
import mu.KotlinLogging
import kotlin.contracts.ExperimentalContracts


/**
 * Rest resource for requests from UI side.
 */
@KtorExperimentalLocationsAPI
@ExperimentalContracts
@KtorExperimentalAPI
class TournamentResource(routing: Routing, toornamentClient: ToornamentClient) : AbstractResource() {

    private val client = toornamentClient.getTournamentClient()

    companion object {
        private val log = KotlinLogging.logger {}
    }

    init {
        routing.tournamentsGet()
        routing.tournamentGet()
    }

    private fun Routing.tournamentsGet() {
        get<TournamentLocation.GetTournamentsLocation> { location ->
            // We redirect requests on this resource to root path to ensure all bookmarks on the tournament list of the old application will still work
            // Only requests they come NOT from the application itself will be redirected
            if (!call.request.headers.contains(AgonHeader.AGON_REST_CLIENT_NAME)) {
                log.debug { "Non Restful request on tournaments redirected to root path" }
                call.respondRedirect("/", true)
            } else {
                doCatchedToornamentRequest(call) {
                    call.respondText(contentType = ContentType.Application.Json, provider = { client.tournamentsGet(location) })
                }
            }
        }
    }

    private fun Routing.tournamentGet() {
        get<TournamentLocation.GetTournamentLocation> { location ->
            doCatchedToornamentRequest(call) {
                call.respondText(contentType = ContentType.Application.Json, provider = { client.tournamentGet(location) })
            }
        }
    }
}