package ch.sourcemotion.agon.resource.v2

import ch.sourcemotion.agon.resource.v2.request.StageLocation
import ch.sourcemotion.agon.toornament.ToornamentClient
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.get
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.util.KtorExperimentalAPI
import mu.KotlinLogging
import kotlin.contracts.ExperimentalContracts

/**
 * Rest resource for requests from UI side.
 */
@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
@ExperimentalContracts
class StageResource(routing: Routing, toornamentClient: ToornamentClient) : AbstractResource() {

    companion object {
        private val log = KotlinLogging.logger {}
    }

    private val client = toornamentClient.getStageClient()

    init {
        routing.stagesGet()
        routing.stageGet()
        routing.rankingsGet()
    }

    private fun Routing.stagesGet() {
        get<StageLocation.GetStagesLocation> { location ->
            doCatchedToornamentRequest(call) {
                call.respondText(contentType = ContentType.Application.Json, provider = { client.stagesGet(location) })
            }
        }
    }

    private fun Routing.stageGet() {
        get<StageLocation.GetStageLocation> { location ->
            doCatchedToornamentRequest(call) {
                call.respondText(contentType = ContentType.Application.Json, provider = { client.stageGet(location) })
            }
        }
    }

    private fun Routing.rankingsGet() {
        get<StageLocation.GetRankingsLocation> { location ->
            doCatchedToornamentRequest(call) {
                val result = client.rankingsGet(location)
                log.debug { "Got rankings for stage ${location.stageId}" }
                call.respond(result)
            }
        }
    }
}

