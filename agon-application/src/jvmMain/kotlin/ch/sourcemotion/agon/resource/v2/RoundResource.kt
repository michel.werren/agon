package ch.sourcemotion.agon.resource.v2

import ch.sourcemotion.agon.resource.v2.request.RoundLocation
import ch.sourcemotion.agon.toornament.ToornamentClient
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.get
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.util.KtorExperimentalAPI
import kotlin.contracts.ExperimentalContracts

/**
 * Rest resource for requests from UI side.
 */
@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
@ExperimentalContracts
class RoundResource(routing: Routing, toornamentClient: ToornamentClient) : AbstractResource() {

    private val client = toornamentClient.getRoundClient()

    init {
        routing.roundsGet()
        routing.roundGet()
    }

    private fun Routing.roundsGet() {
        get<RoundLocation.GetRoundsLocation> { location ->
            doCatchedToornamentRequest(call) {
                call.respondText(contentType = ContentType.Application.Json, provider = { client.roundsGet(location) })
            }
        }
    }

    private fun Routing.roundGet() {
        get<RoundLocation.GetRoundLocation> { location ->
            doCatchedToornamentRequest(call) {
                call.respondText(contentType = ContentType.Application.Json, provider = { client.roundGet(location) })
            }
        }
    }
}

