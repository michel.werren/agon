package ch.sourcemotion.agon.resource.v2.request

import ch.sourcemotion.agon.TournamentPath
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location

@KtorExperimentalLocationsAPI
@Location(TournamentPath.ROOT_PATH)
class MatchLocation {
    @Location("/{tournamentId}/matches")
    data class GetMatchesLocation(
        val tournamentId: String,
        val round_ids: String? = null,
        val stage_ids: String? = null,
        val statuses: String? = null
    )
}