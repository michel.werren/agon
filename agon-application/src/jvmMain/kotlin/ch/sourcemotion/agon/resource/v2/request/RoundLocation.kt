package ch.sourcemotion.agon.resource.v2.request

import ch.sourcemotion.agon.TournamentPath
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location

@KtorExperimentalLocationsAPI
@Location(TournamentPath.ROOT_PATH)
class RoundLocation {
    @Location("/{tournamentId}/rounds")
    data class GetRoundsLocation(val tournamentId: String, val group_ids: String? = null, val stage_ids: String? = null)

    @Location("/{tournamentId}/rounds/{roundId}")
    data class GetRoundLocation(val tournamentId: String, val roundId: String)
}