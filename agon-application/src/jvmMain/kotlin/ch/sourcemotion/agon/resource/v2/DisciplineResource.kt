package ch.sourcemotion.agon.resource.v2

import ch.sourcemotion.agon.resource.v2.request.DiscliplineLocation
import ch.sourcemotion.agon.toornament.ToornamentClient
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.get
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.util.KtorExperimentalAPI
import kotlin.contracts.ExperimentalContracts

/**
 * Rest resource for requests from UI side.
 */
@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
@ExperimentalContracts
class DisciplineResource(routing: Routing, toornamentClient: ToornamentClient) : AbstractResource() {

    private val client = toornamentClient.getDisciplineClient()

    init {
        routing.disciplinesGet()
    }

    private fun Routing.disciplinesGet() {
        get<DiscliplineLocation.GetDisciplinesLocation> { location ->
            doCatchedToornamentRequest(call) {
                call.respondText(contentType = ContentType.Application.Json, provider = { client.disciplinesGet(location) })
            }
        }
    }
}

