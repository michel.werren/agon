package ch.sourcemotion.agon.caching

import ch.sourcemotion.agon.config.AgonCacheConfig
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.kotlintest.matchers.collections.shouldHaveSize
import io.kotlintest.shouldBe
import io.kotlintest.shouldNotBe
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import me.alexpanov.net.FreePortFinder
import org.junit.After
import org.junit.Before
import org.junit.Test
import redis.embedded.RedisServer
import kotlin.contracts.ExperimentalContracts


@ExperimentalContracts
class CacheTest {

    private val freePort = FreePortFinder.findFreeLocalPort()

    private lateinit var redisServer: RedisServer

    private lateinit var cacheFactory: AgonCacheFactory

    @Before
    fun setUp() {
        redisServer = RedisServer(freePort)
        redisServer.start()

        Thread.sleep(1000)
        cacheFactory =
                AgonCacheFactory.build(AgonCacheConfig(2, 2, "redis://localhost:$freePort"), jacksonObjectMapper())
    }

    @After
    fun tearDown() {
        redisServer.stop()
    }

    @Test
    fun getNotExistingStringKey() {
        val stringKeyCache = cacheFactory.getCache<CacheKey, CacheValue>(10)
        val cacheValue = CacheValue("property")
        val cacheKey = CacheKey("cache_key")
        runBlocking {
            stringKeyCache.get(cacheKey).shouldBe(null)

            stringKeyCache.get(cacheKey) {
                cacheValue
            }.shouldBe(cacheValue)

            stringKeyCache.get(cacheKey).shouldBe(cacheValue)
        }
    }

    @Test
    fun cacheListValues() {
        val stringKeyCache = cacheFactory.getCache<CacheKey, List<CacheValue>>(10)
        val cacheValue = CacheValue("property")
        val cacheKey = CacheKey("cache_key")
        runBlocking {
            stringKeyCache.put(cacheKey, listOf(cacheValue, cacheValue, cacheValue))

            val cachedValue = stringKeyCache.get(cacheKey)
            cacheValue.shouldNotBe(null)
            cachedValue!!.shouldHaveSize(3)
            cachedValue.forEach { value -> value.shouldBe(cacheValue) }
        }
    }

    @Test
    fun cacheListSingleValue() {
        val stringKeyCache = cacheFactory.getCache<CacheKey, List<CacheValue>>(10)
        val cacheValue = CacheValue("property")
        val cacheKey = CacheKey("cache_key")
        runBlocking {
            stringKeyCache.put(cacheKey, listOf(cacheValue))

            val cachedValue = stringKeyCache.get(cacheKey)
            cacheValue.shouldNotBe(null)
            cachedValue!!.shouldHaveSize(1)
            cachedValue.forEach { value -> value.shouldBe(cacheValue) }
        }
    }

    @Test
    fun expirationStringKey() {
        val stringKeyCache = cacheFactory.getCache<CacheKey, CacheValue>(1)
        val cacheValue = CacheValue("property")
        val cacheKey = CacheKey("cache_key")
        runBlocking {
            stringKeyCache.get(cacheKey) {
                cacheValue
            }.shouldBe(cacheValue)

            stringKeyCache.get(cacheKey).shouldBe(cacheValue)

            delay(2000)

            stringKeyCache.get(cacheKey).shouldBe(null)
        }
    }
}