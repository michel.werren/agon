package ch.sourcemotion.agon.caching


data class CacheKey(val property: String)