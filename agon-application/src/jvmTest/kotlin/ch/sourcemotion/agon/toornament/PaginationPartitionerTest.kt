package ch.sourcemotion.agon.toornament

import io.kotlintest.matchers.collections.shouldHaveSize
import io.kotlintest.shouldBe
import org.junit.Test


class PaginationPartitionerTest {

    @Test
    fun calculatePartitions() {
        val paginationInfo = PaginationInfo(100, true)
        val rangeStep = 20
        val firstRange = Range.firstRange(rangeStep)
        val partitions = PaginationPartitioner.calculatePartitions(paginationInfo, rangeStep, firstRange)

        partitions.shouldHaveSize(5)

        var rangeFrom = 0
        var rangeTo = 19
        partitions.forEach {range ->
            range.from.shouldBe(rangeFrom)
            range.to.shouldBe(rangeTo)
            rangeFrom += rangeStep
            rangeTo += rangeStep
        }
    }

    @Test
    fun mergeResultArrays() {
        PaginationPartitioner.mergeResultArrays(listOf("[{}]", "[{}]")).shouldBe("[{},{}]")
        PaginationPartitioner.mergeResultArrays(listOf("[{}]", "[{}]", "[{}]")).shouldBe("[{},{},{}]")
    }
}