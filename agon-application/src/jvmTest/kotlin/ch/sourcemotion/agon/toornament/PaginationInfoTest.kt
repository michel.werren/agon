package ch.sourcemotion.agon.toornament

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.kotlintest.matchers.boolean.shouldBeFalse
import io.kotlintest.matchers.boolean.shouldBeTrue
import io.kotlintest.shouldBe
import io.ktor.client.response.HttpResponse
import io.ktor.http.Headers
import org.junit.Test


class PaginationInfoTest {

    @Test
    fun parsingHttpResponse() {
        mock<HttpResponse> {
            on { headers } doReturn Headers.build { append("Content-Range", "tournaments 0-49/114") }
        }.let { response ->
            PaginationInfo.buildFromResponse(response).let {
                it.hasPendingElements.shouldBeTrue()
                it.maxElements.shouldBe(114)
            }
        }

        mock<HttpResponse> {
            on { headers } doReturn Headers.build { append("Content-Range", "tournaments 0-9/114") }
        }.let { response ->
            PaginationInfo.buildFromResponse(response).let {
                it.hasPendingElements.shouldBeTrue()
                it.maxElements.shouldBe(114)
            }
        }

        mock<HttpResponse> {
            on { headers } doReturn Headers.build { append("Content-Range", "tournaments 0-9/9") }
        }.let { response ->
            PaginationInfo.buildFromResponse(response).let {
                it.hasPendingElements.shouldBeFalse()
                it.maxElements.shouldBe(9)
            }
        }

        mock<HttpResponse> {
            on { headers } doReturn Headers.build { append("Content-Range", "tournaments 50-74/75") }
        }.let { response ->
            PaginationInfo.buildFromResponse(response).let {
                it.hasPendingElements.shouldBeFalse()
                it.maxElements.shouldBe(75)
            }
        }

        mock<HttpResponse> {
            on { headers } doReturn Headers.build { }
        }.let { response ->
            PaginationInfo.buildFromResponse(response).let {
                it.hasPendingElements.shouldBeFalse()
                it.maxElements.shouldBe(0)
            }
        }
    }
}