package ch.sourcemotion.agon.toornament

import io.kotlintest.matchers.boolean.shouldBeFalse
import io.kotlintest.matchers.boolean.shouldBeTrue
import io.kotlintest.shouldBe
import org.junit.Test


class RangeTest {

    @Test
    fun firstRange() {
        val step = 40
        val (from, to) = Range.firstRange(step)
        from.shouldBe(0)
        to.shouldBe(39)
    }

    @Test
    fun next() {
        val step = 35
        val (from, to) = Range.firstRange(step).next(step)
        from.shouldBe(35)
        to.shouldBe(69)
    }

    @Test
    fun isValid() {
        val step = 35
        Range.firstRange(step).next(step).isValid(35).shouldBeFalse()
        Range.firstRange(step).next(step).isValid(36).shouldBeTrue()
    }
}