package ch.sourcemotion.agon


object DisciplinePath {
    const val ROOT_PATH = "/disciplines"
    const val GET_DISCIPLINES = ROOT_PATH
}