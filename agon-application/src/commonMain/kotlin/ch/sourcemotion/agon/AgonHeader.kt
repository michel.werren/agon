package ch.sourcemotion.agon


object AgonHeader {
    /**
     * Used to redirect requests on /tournaments resource they are not comming from UI
     */
    const val AGON_REST_CLIENT_NAME = "X-Accessor"
    /**
     * Used to redirect requests on /tournaments resource they are not comming from UI
     */
    const val AGON_REST_CLIENT_VALUE = "agon"
}