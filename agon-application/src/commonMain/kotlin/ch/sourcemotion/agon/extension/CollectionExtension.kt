package ch.sourcemotion.agon.extension

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

/**
 * @return True when this collection is null or empty.
 */
fun <T> Collection<T>?.isNullOrEmpty(): Boolean {
    return this == null || isEmpty()
}

/**
 * @return True when this collection is not null but empty.
 */
fun <T> Collection<T>?.isNotNullButEmpty(): Boolean {
    return this != null && isEmpty()
}

/**
 * @return True when this collection is not null and not empty
 */
@ExperimentalContracts
fun <T> Collection<T>?.isNeitherNullOrEmpty(): Boolean {
    contract {
        returns(true) implies (this@isNeitherNullOrEmpty != null)
    }
    return this != null && isNotEmpty()
}

/**
 * @return new [ArrayList] instance with the elements of this [List]
 */
fun <T> List<T>.newArrayListCopy() = ArrayList(this)