package ch.sourcemotion.agon.extension

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

/**
 * @return True when this [String] is not null and not empty.
 */
@ExperimentalContracts
fun String?.isNeitherNullOrEmpty(): Boolean {
    contract {
        returns(true) implies (this@isNeitherNullOrEmpty != null)
    }
    return this != null && this.isNotBlank()
}

fun <T> T?.or(altValue: T): T = this ?: altValue
fun <T> T?.or(block: () -> T?): T? = this ?: block()