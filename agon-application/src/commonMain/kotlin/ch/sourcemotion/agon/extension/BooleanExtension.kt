package ch.sourcemotion.agon.extension

import kotlin.contracts.ExperimentalContracts

/**
 * @return true when this [Boolean] is not null and true
 */
@ExperimentalContracts
fun Boolean?.isTrue() = this == true

/**
 * @return true when this [Boolean] is null or true
 */
fun Boolean?.isNullOrTrue() = this == null || this == true

/**
 * @return true when this [Boolean] is false
 */
fun Boolean?.isFalse() = this == null || this == false

/**
 * Executes given function when this is [Boolean.isTrue]
 */
@ExperimentalContracts
inline fun Boolean.doWhenTrue(action: () -> Unit) {
    if (isTrue()) {
        action.invoke()
    }
}

/**
 * Executes given function when this is [Boolean.isTrue]
 */
@ExperimentalContracts
inline fun Boolean.doWhenTrueAsync(action: () -> Unit) {
    if (isTrue()) {
        action.invoke()
    }
}

/**
 * Executes given function when this is [Boolean.isFalse]
 */
inline fun Boolean.doWhenFalse(action: () -> Unit) {
    if (isFalse()) {
        action.invoke()
    }
}

/**
 * Executes given function when this is [Boolean.isFalse]
 */
inline fun Boolean.doWhenFalseAsync(action: () -> Unit) {
    if (isFalse()) {
        action.invoke()
    }
}