package ch.sourcemotion.agon.extension

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

/**
 * @return true if this instance is null
 */
@ExperimentalContracts
fun Any?.isNull(): Boolean {
    contract {
        returns(true) implies (this@isNull == null)
    }
    return this == null
}

/**
 * @return true if this instance is not null
 */
@ExperimentalContracts
fun Any?.isNotNull(): Boolean {
    contract {
        returns(true) implies (this@isNotNull != null)
    }
    return this != null
}


/**
 * Execute lambda when this instance is null
 *
 * @param Lambda to execute
 */
@ExperimentalContracts
inline fun <T> T?.whenNull(delegate: () -> Unit) {
    contract {
        callsInPlace(delegate, InvocationKind.EXACTLY_ONCE)
    }
    if (this.isNull()) {
        delegate()
    }
}

/**
 * Execute lambda when this instance is not null
 *
 * @param Lambda to execute
 */
@ExperimentalContracts
inline fun <T> T?.whenNotNull(delegate: (T) -> Unit) {
    contract {
        callsInPlace(delegate, InvocationKind.EXACTLY_ONCE)
    }
    if (this.isNotNull()) {
        delegate(this)
    }
}
