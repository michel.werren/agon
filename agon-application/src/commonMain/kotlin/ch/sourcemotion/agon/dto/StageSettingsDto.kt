package ch.sourcemotion.agon.dto

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class StageSettingsDto(
        @SerialName("grand_final")
        @Optional
        val grandFinal: String? = null,
        @SerialName("size")
        @Optional
        val size: Int? = null,
        @SerialName("skip_round1")
        @Optional
        val skipRound1: Boolean? = null,
        @SerialName("threshold")
        @Optional
        val threshold: Int? = null
)