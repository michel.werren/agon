package ch.sourcemotion.agon.dto

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RankingItemDto(
    @SerialName("id")
    val id: String,
    @SerialName("group_id")
    @Optional
    val groupId: String? = null,
    @SerialName("number")
    @Optional
    val number: Int? = null,
    @SerialName("participant")
    @Optional
    val participant: ParticipantDto? = null,
    @SerialName("points")
    @Optional
    val points: Int? = null,
    @SerialName("position")
    @Optional
    val position: Int? = null,
    @SerialName("properties")
    @Optional
    val properties: RankingPropertiesDto? = null,
    @SerialName("rank")
    @Optional
    val rank: Int? = null
)