package ch.sourcemotion.agon.dto

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TournamentDto(
        @SerialName("id")
        val id: String,
        @SerialName("country")
        @Optional
        val country: String? = null,
        @SerialName("discipline")
        @Optional
        val discipline: String? = null,
        @SerialName("full_name")
        @Optional
        val fullName: String? = null,
        @SerialName("location")
        @Optional
        val location: String? = null,
        @SerialName("logo")
        @Optional
        val logo: LogoDto? = null,
        @SerialName("name")
        @Optional
        val name: String? = null,
        @SerialName("online")
        @Optional
        val online: Boolean? = null,
        @SerialName("platforms")
        @Optional
        val platforms: List<String?>? = null,
        @SerialName("public")
        @Optional
        val `public`: Boolean? = null,
        @SerialName("registration_closing_datetime")
        @Optional
        val registrationClosingDatetime: String? = null,
        @SerialName("registration_enabled")
        @Optional
        val registrationEnabled: Boolean? = null,
        @SerialName("registration_opening_datetime")
        @Optional
        val registrationOpeningDatetime: String? = null,
        @SerialName("scheduled_date_end")
        @Optional
        val scheduledDateEnd: String? = null,
        @SerialName("scheduled_date_start")
        @Optional
        val scheduledDateStart: String? = null,
        @SerialName("size")
        @Optional
        val size: Int? = null,
        @SerialName("status")
        @Optional
        val status: String? = null,
        @SerialName("timezone")
        @Optional
        val timezone: String? = null
)