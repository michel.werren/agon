package ch.sourcemotion.agon.dto

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RankingPropertiesDto(
    @SerialName("draws")
    @Optional
    val draws: Int? = null,
    @SerialName("forfeits")
    @Optional
    val forfeits: Int? = null,
    @SerialName("losses")
    @Optional
    val losses: Int? = null,
    @SerialName("played")
    @Optional
    val played: Int? = null,
    @SerialName("score_against")
    @Optional
    val scoreAgainst: Int? = null,
    @SerialName("score_difference")
    @Optional
    val scoreDifference: Int? = null,
    @SerialName("score_for")
    @Optional
    val scoreFor: Int? = null,
    @SerialName("wins")
    @Optional
    val wins: Int? = null
)