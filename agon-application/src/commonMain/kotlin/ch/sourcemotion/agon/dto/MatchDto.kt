package ch.sourcemotion.agon.dto

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MatchDto(
    @SerialName("id")
    val id: String,
    @SerialName("group_id")
    @Optional
    val groupId: String? = null,
    @SerialName("number")
    @Optional
    val number: Int? = null,
    @SerialName("opponents")
    @Optional
    val opponents: List<OpponentDto>? = null,
    @SerialName("played_at")
    @Optional
    val playedAt: String? = null,
    @SerialName("round_id")
    @Optional
    val roundId: String? = null,
    @SerialName("scheduled_datetime")
    @Optional
    val scheduledDatetime: String? = null,
    @SerialName("stage_id")
    @Optional
    val stageId: String? = null,
    @SerialName("status")
    @Optional
    val status: String? = null,
    @SerialName("type")
    @Optional
    val type: String? = null
)