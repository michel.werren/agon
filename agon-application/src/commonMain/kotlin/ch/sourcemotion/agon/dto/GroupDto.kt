package ch.sourcemotion.agon.dto

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class GroupDto(
        @SerialName("id")
        val id: String,
        @SerialName("closed")
        @Optional
        val closed: Boolean? = null,
        @SerialName("name")
        @Optional
        val name: String? = null,
        @SerialName("number")
        @Optional
        val number: Int? = null,
        @SerialName("settings")
        @Optional
        val settings: GroupSettingsDto? = null,
        @SerialName("stage_id")
        @Optional
        val stageId: String? = null
)