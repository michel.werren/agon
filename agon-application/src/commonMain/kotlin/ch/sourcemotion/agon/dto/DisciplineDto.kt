package ch.sourcemotion.agon.dto

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class DisciplineDto(
        @SerialName("id")
        val id: String,
        @SerialName("copyrights")
        @Optional
        val copyrights: String? = null,
        @SerialName("fullname")
        @Optional
        val fullname: String?  = null,
        @SerialName("name")
        @Optional
        val name: String? = null,
        @SerialName("shortname")
        @Optional
        val shortname: String? = null
)