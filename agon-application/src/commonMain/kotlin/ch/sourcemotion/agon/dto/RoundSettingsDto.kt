package ch.sourcemotion.agon.dto

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RoundSettingsDto(
        @SerialName("size")
        @Optional
        val size: Int? = null
)