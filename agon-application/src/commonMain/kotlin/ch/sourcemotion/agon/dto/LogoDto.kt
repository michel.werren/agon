package ch.sourcemotion.agon.dto

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class LogoDto(
    @SerialName("logo_large")
    @Optional
    val logoLarge: String? = null,
    @SerialName("logo_medium")
    @Optional
    val logoMedium: String? = null,
    @SerialName("logo_small")
    @Optional
    val logoSmall: String? = null,
    @SerialName("original")
    @Optional
    val original: String? = null
)