package ch.sourcemotion.agon.dto

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Currently without custom fields
 */
@Serializable
data class ParticipantDto(
    @SerialName("id")
    val id: String,
    @SerialName("lineup")
    @Optional
    val lineup: List<LineupDto?>? = emptyList(),
    @SerialName("name")
    @Optional
    val name: String?  = null
)