package ch.sourcemotion.agon.dto

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


/**
 * Currently without custom fields
 */
@Serializable
data class LineupDto(
    @SerialName("name")
    @Optional
    val name: String? = null
)