package ch.sourcemotion.agon.dto

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * We ignore some properties because we go over the organizer api. On that the tournament has more, internal properties
 * we are not interested on.
 */
@Serializable
data class TournamentDetailDto(
        @SerialName("id")
        val id: String,
        @SerialName("contact")
        @Optional
        val contact: String? = null,
        @SerialName("country")
        @Optional
        val country: String? = null,
        @SerialName("description")
        @Optional
        val description: String? = null,
        @SerialName("discipline")
        @Optional
        val discipline: String? = null,
        @SerialName("discord")
        @Optional
        val discord: String? = null,
        @SerialName("full_name")
        @Optional
        val fullName: String? = null,
        @SerialName("location")
        @Optional
        val location: String? = null,
        @SerialName("logo")
        @Optional
        val logo: LogoDto? = null,
        @SerialName("name")
        @Optional
        val name: String? = null,
        @SerialName("online")
        @Optional
        val online: Boolean? = null,
        @SerialName("organization")
        @Optional
        val organization: String? = null,
        @SerialName("participant_type")
        @Optional
        val participantType: String? = null,
        @SerialName("platforms")
        @Optional
        val platforms: List<String?>? = null,
        @SerialName("prize")
        @Optional
        val prize: String? = null,
        @SerialName("public")
        @Optional
        val `public`: Boolean? = null,
        @SerialName("registration_closing_datetime")
        @Optional
        val registrationClosingDatetime: String? = null,
        @SerialName("registration_enabled")
        @Optional
        val registrationEnabled: Boolean? = null,
        @SerialName("registration_opening_datetime")
        @Optional
        val registrationOpeningDatetime: String? = null,
        @SerialName("rules")
        @Optional
        val rules: String? = null,
        @SerialName("scheduled_date_end")
        @Optional
        val scheduledDateEnd: String? = null,
        @SerialName("scheduled_date_start")
        @Optional
        val scheduledDateStart: String? = null,
        @SerialName("size")
        @Optional
        val size: Int? = null,
        @SerialName("status")
        @Optional
        val status: String? = null,
        @SerialName("timezone")
        @Optional
        val timezone: String? = null,
        @SerialName("website")
        @Optional
        val website: String? = null
)