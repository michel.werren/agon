package ch.sourcemotion.agon.dto

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class OpponentDto(
    @SerialName("forfeit")
    @Optional
    val forfeit: Boolean? = null,
    @SerialName("number")
    @Optional
    val number: Int? = null,
    @SerialName("participant")
    @Optional
    val participant: ParticipantDto? = null,
    @SerialName("position")
    @Optional
    val position: Int? = null,
    @SerialName("rank")
    @Optional
    val rank: Int? = null,
    @SerialName("result")
    @Optional
    val result: String? = null,
    @SerialName("score")
    @Optional
    val score: Int? = null
)